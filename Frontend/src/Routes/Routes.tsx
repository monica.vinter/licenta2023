import { BrowserRouter, Route, Routes, useLocation } from 'react-router-dom';

import PrivateRoute from '../components/atoms/PrivateRoute/PrivateRoute';
import PaginaDoneaza from '../components/pages/PaginaDoneaza/PaginaDoneaza';
import FormLayout from '../components/pages/PaginaInregistrare/PaginaInregistrare';
import { RegisterForm } from '../components/molecules/RegisterForm/RegisterForm';
import { LoginForm } from '../components/molecules/LoginForm/LoginForm';
import { ForgotPasswordForm } from '../components/molecules/ForgotPasswordForm/ForgotPassword';
import { ResetPasswordForm } from '../components/molecules/ResetPasswordForm/ResetPasswordForm';
import { Navbar } from '../components/molecules/Navbar/Navbar';
import { Footer } from '../components/molecules';
import PaginaTrimiteSpreAdoptie from '../components/pages/PaginaTrimiteSpreAdoptie/PaginaTrimiteSpreAdoptie';
import PaginaAcasa from '../components/pages/PaginaAcasa/PaginaAcasa';
import PaginaAdopta from '../components/pages/PaginaAdopta/PaginaAdopta';
import PaginaAnimalAdmin from '../components/pages/PaginaAnimalAdmin/PaginaAnimalAdmin';
import PaginaAnimalUser from '../components/pages/PaginaAnimalUser/PaginaAnimalUser';
import PaginaAdminAnunturi from '../components/pages/PaginaAdminAnunturi/PaginaAdminAnunturi';
import PaginaAdminAnunturiRefuzate from '../components/pages/PaginaAdminAnunturiRefuzate/PaginaAdminAnunturiRefuzate';
import PaginaAdminAnunturiAcceptate from '../components/pages/PaginaAdminAnunturiAcceptate/PaginaAdminAnunturiAcceptate';
import PaginaAdaugaAnimalAdmin from '../components/pages/PaginaAdaugaAnimalAdmin/PaginaAdaugaAnimalAdmin';
import PaginaUserAnunturi from '../components/pages/PaginaUserAnunturi/PaginaUserAnunturi';

import { FormularIcon } from '../components/molecules/FormularAdoptie/FormularIcon/FormularIcon';
import { ButtonScrollToTop } from '../components/atoms/ButtonScrollToTop/ButtonScrollToTop';
import * as MockData from '../mockData/mockDataComponents';
import '../App.scss';
import {
  PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH,
  PAGINA_ADMIN_ANUNTURI_REFUZATE_PATH,
  PAGINA_ADMIN_ANIMAL,
  PAGINA_ADMIN_ADAUGA_ANUNT,
  PAGINA_ADMIN_ANUNTURI_PATH,
  PAGINA_USER_ANUNTURI_PATH,
  PAGINA_USER_ANIMAL,
  DONEAZA_PATH,
  FORGOT_PATH,
  LOGIN_PATH,
  PAGINA_ANIMAL,
  PAGINA_TOPIC,
  REGISTER_PATH,
  RESETPASSUSERLOGGEDIN_PATH,
  RESET_PATH,
  ACASA_PATH,
  ADOPTA_PATH,
  SFATURI_PATH,
  TRIMITESPREADOPTIE_PATH,
  USERPROFILE_PATH,
  ADMINISTRAREANIMALE_PATH,
  ADMIN_PATH,
  FORUM_PATH,
} from '../Routes/routesPath';
import { ResetPasswordUserLoggedInForm } from '../components/molecules/ResetPasswordUserLoggedIn/ResetPasswordUserLoggedInForm';
import PaginaAnimal from '../components/pages/PaginaAnimal/PaginaAnimal';
import PaginaTopic from '../components/pages/PaginaTopic/PaginaTopic';
import PageNotFound from '../components/pages/PageNotFound/PageNotFound';
import PaginaSfaturi from '../components/pages/PaginaSfaturi/PaginaSfaturi';
import PaginaPisici from '../components/pages/PaginaPisici/PaginaPisici';
import PaginaCaini from '../components/pages/PaginaCaini/PaginaCaini';
import PaginaAltele from '../components/pages/PaginaAltele/PaginaAltele';
import UserProfilePage from '../components/pages/PaginaProfilUser/PaginaProfilUser';
import AdminLayout from '../components/pages/AdminLayout/AdminLayout';
import { AdaugaAnimal } from '../components/molecules/AdaugaAnimal/AdaugaAnimal';
import { AdaugaTopic } from '../components/molecules/AdaugaTopic/AdaugaTopic';
import AdminDashboard from '../components/pages/AdminDashboard/AdminDashboard';
import PaginaForum from '../components/pages/PaginaForum/PaginaForum';

const Navigation = () => {
  const { pathname } = useLocation();
  if (
    pathname.includes(LOGIN_PATH) ||
    pathname.includes(REGISTER_PATH) ||
    pathname.includes(FORGOT_PATH) ||
    pathname.includes(RESET_PATH) ||
    pathname.includes(RESETPASSUSERLOGGEDIN_PATH)
  ) {
    return <Navbar {...MockData.NavabarIconsData} />;
  } else if (
    pathname.includes(DONEAZA_PATH) ||
    pathname.includes(SFATURI_PATH) ||
    pathname.includes(TRIMITESPREADOPTIE_PATH) 
  ) {
    return <Navbar {...MockData.NavabarLogoData} />;
  } else return <Navbar {...MockData.NavabarSimpleData} />;
};

const FormularAdoptie = () => {
  const { pathname } = useLocation();
  if (
    pathname.includes(ACASA_PATH) ||
    pathname.includes(ADOPTA_PATH) ||
    pathname.includes('/animal-page')
  ) {
    return <FormularIcon />;
  } else {
    return <></>;
  }
};

const PlatformRoutes = () => {
  return (
    <>
      <BrowserRouter>
        <FormularAdoptie />
        <Navigation />
        <ButtonScrollToTop />
        <Routes>
          <Route path={SFATURI_PATH} element={<PaginaSfaturi />} />
          <Route path={SFATURI_PATH + '/pisici'} element={<PaginaPisici />} />
          <Route path={SFATURI_PATH + '/câini'} element={<PaginaCaini />} />
          <Route path={SFATURI_PATH + '/altele'} element={<PaginaAltele />} />
          <Route path={DONEAZA_PATH} element={<PaginaDoneaza />} />
          <Route
            path={REGISTER_PATH}
            element={
              <FormLayout>
                <RegisterForm />
              </FormLayout>
            }
          />
          <Route
            path={LOGIN_PATH}
            element={
              <FormLayout>
                {' '}
                <LoginForm />{' '}
              </FormLayout>
            }
          />
          <Route
            path={FORGOT_PATH}
            element={
              <FormLayout>
                {' '}
                <ForgotPasswordForm />{' '}
              </FormLayout>
            }
          />
          <Route
            path={RESET_PATH}
            element={
              <FormLayout>
                {' '}
                <ResetPasswordForm />{' '}
              </FormLayout>
            }
          />
          <Route
            path={RESETPASSUSERLOGGEDIN_PATH}
            element={
              <PrivateRoute>
                <FormLayout>
                  <ResetPasswordUserLoggedInForm />
                </FormLayout>
              </PrivateRoute>
            }
          />
          <Route path={PAGINA_ANIMAL} element={<PaginaAnimal />} />
          <Route path={PAGINA_ADMIN_ANIMAL} element={<PaginaAnimalAdmin />} />

          <Route path={PAGINA_ADMIN_ANUNTURI_PATH} element={<PaginaAdminAnunturi />} />
          <Route path={PAGINA_ADMIN_ANUNTURI_REFUZATE_PATH} element={<PaginaAdminAnunturiRefuzate />} />
          <Route path={PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH} element={<PaginaAdminAnunturiAcceptate />} />

          <Route path={PAGINA_USER_ANUNTURI_PATH} element={<PaginaUserAnunturi />} />
          <Route path={PAGINA_USER_ANIMAL} element={<PaginaAnimalUser />} />

          
          <Route
             path={PAGINA_ADMIN_ADAUGA_ANUNT}
             element={
               <PaginaAdaugaAnimalAdmin>
                 <AdaugaAnimal />
               </PaginaAdaugaAnimalAdmin>
             }
          />
          <Route path={PAGINA_TOPIC} element={<PaginaTopic />} />
          <Route path={ADOPTA_PATH} element={<PaginaAdopta />} />
          <Route path={ADOPTA_PATH + '/:category'} element={<PaginaAdopta />} />
          <Route path={ACASA_PATH} element={<PaginaAcasa />} />
          <Route path={USERPROFILE_PATH} element={<UserProfilePage />} />
          <Route path={ADMIN_PATH} element={<AdminDashboard />} />
          <Route path='*' element={<PageNotFound />} />
          
          <Route
             path={FORUM_PATH}
             element={
               <PaginaForum>
                 <AdaugaTopic />
               </PaginaForum>
             }
          />

          <Route
             path={TRIMITESPREADOPTIE_PATH}
             element={
               <PaginaTrimiteSpreAdoptie>
                 <AdaugaAnimal />
               </PaginaTrimiteSpreAdoptie>
             }
          />
          
          <Route
            path={RESETPASSUSERLOGGEDIN_PATH}
            element={
              <PrivateRoute>
                <FormLayout>
                  <ResetPasswordUserLoggedInForm />
                </FormLayout>
              </PrivateRoute>
            }
          />
        </Routes>
        <Footer />
      </BrowserRouter>
    </>
  );
};
export default PlatformRoutes;
