export const ACASA_PATH = '/';
export const SFATURI_PATH = '/sfaturi';
export const DONEAZA_PATH = '/doneaza';
export const PAGINA_ADMIN_ANUNTURI_PATH = '/admin/anunturi';
export const PAGINA_USER_ANUNTURI_PATH = '/user/anunturi';
export const PAGINA_ADMIN_ANUNTURI_REFUZATE_PATH = '/admin/anunturi_refuzate';
export const PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH = '/admin/anunturi_acceptate';
export const PAGINA_ADMIN_ADAUGA_ANUNT = '/admin/adaugaanimal';
export const FORUM_PATH = '/Forum';
export const TRIMITESPREADOPTIE_PATH = '/trimitespreadoptie';
export const REGISTER_PATH = '/register';
export const LOGIN_PATH = '/login';
export const FORGOT_PATH = '/forgot-password';
export const RESET_PATH = '/reset-password';
export const RESETPASSUSERLOGGEDIN_PATH = '/reset-password-user-logged-in';
export const PAGINA_ANIMAL = '/pagina-animal/:id';
export const PAGINA_ADMIN_ANIMAL = '/admin/pagina-animal/:id';
export const PAGINA_USER_ANIMAL = '/user/pagina-animal/:id';
export const PAGINA_TOPIC = '/pagina-topic/:id';

export const ANIMAL_PAGE_2 = '/animal-page';
export const ADOPTA_PATH = '/adopta';
export const CHECKOUT_PATH = '/checkout';

export const USERPROFILE_PATH = '/user-profile';
export const ADMIN_PATH = '/admin';

export const ADMINISTRAREANIMALE_PATH = '/administrare-animale';
export const ORDERMANAGEMENT_PATH = '/admin/order-management';
export const NEWSMANAGEMENT_PATH = '/admin/news-management';

