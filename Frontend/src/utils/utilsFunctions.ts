import Cookies from 'universal-cookie';
import { DateFormular } from '../Interfaces/formularInterface';
import { TOKEN } from './Constants';

const cookies = new Cookies();


export const addCookie = (cookieKey: string, cookieData: string) => {
  const date = new Date();
  date.setTime(date.getTime() + 36000000);
  cookies.set(cookieKey, cookieData, {
    path: '/',
    expires: date,
  });
};

export const getCookie = (cookieKey: string) => {
  return cookies.get(cookieKey);
};

export const existToken = () => {
  const token: string = getCookie(TOKEN);
  if (token) return true;
  else return false;
};

export const deleteCookie = (cookieKey: string) => {
  cookies.remove(cookieKey, { path: '/' });
};

export const getTotalItems = (cart: DateFormular[]) => {
  let totalQuantity = 0;
  cart.forEach((item) => {
    totalQuantity++;
  });
  return { totalQuantity};
};
