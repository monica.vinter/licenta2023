export const animaleUrl = '/animale';
export const topicuriUrl = '/topicuri';
export const mesajeUrl = '/mesaje';
export const animaleAction = 'animale/getAnimale';
export const topicuriAction = 'animale/getTopicuri';
export const mesajeAction = 'user/getMesaje';

export const registerUserAction = 'user/register';
export const loginUserAction = 'user/login';
export const getUserAction = 'user/getUsers';


export const userUrl = '/users';

export const forgotPasswordKeyAction = 'user/forgotPasswordKey';
export const forgotPasswordAction = 'user/forgotPassword';

export const resetPasswordKeyAction = 'user/resetPasswordKey';
export const resetPasswordAction = 'user/changePassword';

export const newsUrl = '/news';
export const newsAction = 'news/getNews';

export const newsLetter = '/newsLetter';

export const formularUrl = '/formulare';
export const formulareAction = 'formulare/getFormulare';