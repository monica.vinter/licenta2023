// Accordion
export enum AccordionShapes {
  BASIC = 'basic',
  ROUNDED = 'rounded',
}
export enum AccordionTypes {
  BASIC = 'basic',
  MARGIN = 'margin',
  BORDER = 'border',
  BORDER_MARGIN = 'border-margin',
}

export enum AccordionBackground {
  DEFAULT = 'default',
  DARK_BLUE = 'dark-blue',
  BLACK = 'black',
}

// Button
export enum ButtonTypes {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
}
export enum BackgroundThreeColor {
  DARK = 'dark',
  MEDIUM = 'medium',
  LIGHT = 'light',
}

// Dropdown
export enum PageColor {
  DARK = 'dark',
  VIOLET = 'violet',
  GREEN = 'green',
}

// Input

export enum InputTypes {
  NUMBER = 'number',
  PASSWORD = 'password',
  TEXT = 'text',
  EMAIL = 'email',
  MULTILINE = 'MULTILINE'
}

export enum InputStyleTypes {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TEXTAREA = 'textarea',
}

// ButtonGroup
export enum ButtonGroupTypes {
  TEE = 'tee',
  HEADER = 'header',
}

// Breadcrumbs Nav
export enum BreadcrumbsNavColors {
  BROWN = '',
}

// IndexCard

export const RESET_LABEL = 'Reset';

// ShopCard

export const VIEW_SHOP = 'Mergi la adopții';

// CategoriiAnimale

export const CATEGORII_ANIMALE = 'Mergi la adopții';
export const CATEGORII_ANIMALE_SFATURI = 'Vezi sfaturi';

// Photo With Text Card

export enum PhotoCardBorders {
  BIG = 'big',
  SMALL = 'small',
  NOBORDER = 'no-border',
}

export enum ContentCardTypes {
  HEADER = 'header',
  CONTENT = 'content',
  FORM = 'form',
  NEWS = 'news',
}

// Carousel

export enum CarouselTypes{
  SIMPLE = 'photo',
  TITLE = 'photo_title',
  TEXT = 'photo_title_text'
}

// Tooltip

export enum Placement {
  TOP = 'top',
  BOTTOM = 'bottom',
  LEFT = 'left',
  RIGHT = 'right',
}

export enum BackgroundStyles{
  LIGHT_BLUE = 'light-blue',
  GRAY = 'gray',
}

// Slider

export enum SliderStyleTypes{
  INDEX = 'index',
  PRICE = 'price',
}
