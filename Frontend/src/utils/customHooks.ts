import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ACASA_PATH } from '../Routes/routesPath';
import { RootState } from '../redux/store';

export function usePopup() {
  const [isOpen, setisOpen] = useState(false);

  const toggle = () => {
    setisOpen(!isOpen);
    setTimeout(() => {
      setisOpen(false);
    }, 7000);
  };

  return {
    isOpen,
    toggle,
  };
}

export function useCart() {
  const [isOpen, setisOpen] = useState(false);

  const toggle = () => {
    setisOpen(!isOpen);
  };

  return {
    isOpen,
    toggle
  };
}

export function useRestrict() {
  const { isConnected } = useSelector((state: RootState) => state.userState);
  const navigate = useNavigate();

  useEffect(() => {
    if (isConnected) {
      navigate(ACASA_PATH);
    }
  }, [isConnected]);
  return isConnected;
}
