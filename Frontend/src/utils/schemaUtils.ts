
export const onlyLettersRegEx = new RegExp(/^([^0-9]*)$/);
export const onlyNumbersRegEx = new RegExp(/^[0-9\s]*$/);
export const passwordRegEx = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()]).{8,20}\S$/);

export const REGEX_PASSWORD = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*£-]).{8,}$/);
export const REGEX_EMAIL = new RegExp(/^[\w-\\.]+@([\w-]+\.)+[\w-]{2,4}$/);
export const REGEX_NAME = new RegExp(/^([a-zA-Zăâîțș-]{2,})$/);
export const REGEX_PHONE = new RegExp(/^[\\+]?[(]?[0-9]{3}[)]?[-\s\\.]?[0-9]{3}[-\s\\.]?[0-9]{4,6}$/im);
export const REGEX_POSTCODE = new RegExp(/^([a-zA-Z0-9]{6})$/);
// valid formats
// (123) 456-7890
// (123)456-7890
// 123-456-7890
// 123.456.7890
// 1234567890
// +31636363634
// 075-63546725 

export const REGEX_BIRTHDAY = new RegExp( /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/);

export const REGEX_VERIFICATION_CODE = new RegExp(/^([a-z0-9]{5})$/);