export const latitude = 56.338909;
export const longitude = -2.79893;

export const TOKEN = 'TOKEN';
export const EMAIL = 'EMAIL';

export const STATUS_ADOPTAT = 'ADOPTAT';
export const DETALII = 'Detalii';

export const Culori = [
  'black',
  'gray',
  'white',
  'orange',
  'yellow',
  'brown'
];
export const Judete = ['Alba', 'Arad', 'Argeș', 'Bacău', 'Bihor', 'Bistrița-Năsăud', 'Botoșani', 'Brașov', 'Brăila', 'București', 'Buzău', 'Caraș-Severin', 'Călărași', 'Cluj', 'Constanța', 'Covasna', 'Dâmbovița', 'Dolj', 'Galați', 'Giurgiu', 'Gorj', 'Harghita', 'Hunedoara', 'Ialomița', 'Iași', 'Ilfov', 'Maramureș', 'Mehedinți', 'Mureș', 'Neamț', 'Olt', 'Prahova', 'Satu Mare', 'Sălaj', 'Sibiu', 'Suceava', 'Teleorman', 'Timiș', 'Tulcea', 'Vaslui', 'Vâlcea', 'Vrancea',];
export const Gen = ['Feminin', 'Masculin'];


