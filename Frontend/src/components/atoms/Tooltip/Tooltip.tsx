import React, { ReactNode, useState } from 'react';
import clsx from 'clsx';
import './Tooltip.scss';
import { Placement, BackgroundStyles } from '../../../utils/customTypes';

export interface TooltipProps {
  message: string;
  placement?: Placement;
  backgroundStyle?: BackgroundStyles;
  children: ReactNode | ReactNode[];
}

export const Tooltip: React.FC<TooltipProps> = ({ message, placement = Placement.RIGHT, backgroundStyle = BackgroundStyles.GRAY, children, }) => {

  const [isActive, setIsActive] = useState(false);
  const tooltipEvents = {
    onMouseEnter: () => setIsActive(true),
    onMouseLeave: () => setIsActive(false),
  };

  const styles = {
    tooltip: clsx('tooltip', `tooltip--${backgroundStyle}`),
    message: clsx('tooltip__message', isActive && `tooltip__message--${placement} active`),
    arrow: clsx('tooltip__arrow')
  };

  return (
    <div className={styles.tooltip} {...tooltipEvents} data-testid='tooltip'>
      {children}
      <div className={styles.message}>
        {message}
        <span className={styles.arrow} />
      </div>
    </div>
  );

};