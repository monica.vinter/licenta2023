import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Tooltip, TooltipProps } from './Tooltip';
import { Button } from '../Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
// import { InfoIcon } from '../../../assets/imagePaths';
import InfoIcon from '../../../assets/InfoIcon.png'

const meta: Meta = {
  title: 'Tooltip',
  component: Tooltip,
  argTypes: {
    children: {
      defaultValue: 'Hover me',
    },
  },
};

export default meta;

const Template: Story<TooltipProps> = args => (
  <div style={{ margin: '10rem', width: 'max-content' }}>
    <Tooltip {...args} />
  </div>
);

export const Basic = Template.bind({});
Basic.args = {
  message: 'Tooltip message',
};

export const TooltipWithBtn = Template.bind({});
TooltipWithBtn.args = {
  message: 'Tooltip with button',
  children: <Button
    type={CustomTypes.ButtonTypes.PRIMARY}
    label={'Book a Tee Time'}
    backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
  />,
};

export const TooltipWithIcon = Template.bind({});
TooltipWithIcon.args = {
  message: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis',
  children: <img src={`${InfoIcon}`} alt='InfoIcon' width={'16px'} />,
};