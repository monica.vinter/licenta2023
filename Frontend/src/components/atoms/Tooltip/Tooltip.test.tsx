import { render, screen, fireEvent } from '@testing-library/react';
import { Tooltip } from './Tooltip';
import * as CustomTypes from '../../../utils/customTypes';


describe('Tooltip', () => {

    it('should render a tooltip with the given message', () => {
        render(<Tooltip message='Some information'> <p> Hover me </p> </Tooltip>)

        const tooltip = screen.getByTestId('tooltip');
        const tootlipMessage = screen.getByText('Some information')
        expect(tooltip).toBeInTheDocument();
        expect(tootlipMessage).toBeInTheDocument();
    });

    it('should have the given background style', () => {
        render(<Tooltip message='Some information' backgroundStyle={CustomTypes.BackgroundStyles.LIGHT_BLUE}> <p> Hover me </p> </Tooltip>)

        const tooltip = screen.getByTestId('tooltip');
        expect(tooltip).toBeInTheDocument();
        expect(tooltip).toHaveClass('tooltip--light-blue');
    });

    it('should be displayed with the given placement when hovered and closed when exited', () => {
        render(<Tooltip message='Some information' placement={CustomTypes.Placement.TOP}> <p> Hover me </p> </Tooltip>)

        const tooltip = screen.getByTestId('tooltip');
        const tooltipMessage = screen.getByText('Some information');

        fireEvent.mouseEnter(tooltip);
        expect(tooltipMessage).toBeVisible();
        expect(tooltipMessage).toHaveClass('tooltip__message--top active');

        fireEvent.mouseLeave(tooltip);;
        expect(tooltipMessage).toHaveClass('tooltip__message');
    });


});