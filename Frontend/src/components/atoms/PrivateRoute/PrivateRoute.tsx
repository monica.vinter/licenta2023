import React from 'react';
import { Navigate } from 'react-router';
import { TOKEN } from '../../../utils/Constants';

import { getCookie } from '../../../utils/utilsFunctions';

const PrivateRoute: React.FC<any> = ({ children }) => {
  return getCookie(TOKEN) ? children : <Navigate to='/login' />;
};

export default PrivateRoute;
