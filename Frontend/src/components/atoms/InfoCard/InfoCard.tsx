import './InfoCard.scss';
import { IoIosPeople } from 'react-icons/io';
import { ImNewspaper } from 'react-icons/im';
import { AiOutlineForm } from 'react-icons/ai';

export interface InfoCardProps {
  icon: 'utilizatori' | 'anunturi' | 'formulare';
  title: string;
  subTitle: number;
}
export const InfoCard: React.FC<InfoCardProps> = ({ icon, title, subTitle }) => {
  const styles = {
    infoCardContainer: 'info-card__container',
    infoCardIcon: 'info-card__container--icon',
    infoCardData: 'info-card__container__data',

    infoCardTitle: 'info-card__container__data--title',
    infoCardSubTitle: 'info-card__container__data--subtitle',
  };
  return (
    <div className={styles.infoCardContainer}>
      <div className={styles.infoCardIcon}>
        <div>
          {icon === 'utilizatori' ? (
            <IoIosPeople />
          ) : icon === 'anunturi' ? (
            <ImNewspaper />
          ) : icon === 'formulare' ? (
            <AiOutlineForm />
          ) : (
            ''
          )}
        </div>
      </div>
      <div className={styles.infoCardData}>
        <p className={styles.infoCardTitle}>{title}</p>
        <p className={styles.infoCardSubTitle}>{subTitle}</p>
      </div>
    </div>
  );
};
