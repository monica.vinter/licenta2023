import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ButtonGroup } from './ButtonGroup';
import * as CustomTypes from '../../../utils/customTypes';

export default {
  title: 'ButtonGroup',
  component: ButtonGroup,
} as ComponentMeta<typeof ButtonGroup>;

const Template: ComponentStory<typeof ButtonGroup> = (args) => <ButtonGroup {...args} />;

export const ButtonGroup1 = Template.bind({});

const printButtonLabel = (label: string) => {
  console.log(label);
};

ButtonGroup1.args = {
  type: CustomTypes.ButtonGroupTypes.TEE,
  backgroundColor: CustomTypes.PageColor.DARK,
  buttons: [
    { label: 'One', index: 1 },
    { label: 'Two', index: 2 },
    { label: 'Three', index: 3 }],
  afterClick: (printButtonLabel)
};