import clsx from 'clsx';
import './ButtonGroup.scss';
import * as CustomTypes from '../../../utils/customTypes';
import React, { useState } from 'react';

interface ButtonGroupProps {
  type: CustomTypes.ButtonGroupTypes;
  backgroundColor?: CustomTypes.PageColor;
  afterClick: (label: string) => void;
  buttons: { label: string, index: number, icon?: React.ReactNode }[];
}

export const ButtonGroup = ({ type, backgroundColor, buttons, afterClick }: ButtonGroupProps) => {
  const styles = {
    buttonGroup: clsx('button-group', `button-group--${type}`, `button-group--${type}-${backgroundColor}`),
    buttonGroupContainer: 'button-group__container',
    buttonGroupActive: clsx('button-group active', `button-group--${type}`, `button-group--${type}-${backgroundColor}`)
  };
  const [clickedId, setClickedId] = useState(1);
  const handleClick = (label: string, id: number) => {
    setClickedId(id);
    afterClick(label);
  };
  return (
    <div className={styles.buttonGroupContainer}>
      {buttons.map((i) => (
        <button
          key={i.index}
          className={i.index === clickedId ? styles.buttonGroupActive : styles.buttonGroup}
          onClick={() => handleClick(i.label, i.index)}>
          {i.icon && <span className="button-group__icon">{i.icon}</span>}
          {i.label}
        </button>
      ))}
    </div>
  );
};

