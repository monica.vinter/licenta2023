import { render, screen, fireEvent } from '@testing-library/react';
import { ButtonGroup } from './ButtonGroup';
import * as CustomTypes from '../../../utils/customTypes';


describe('ButtonGroup', () => {

    const mockButtons = [
        { label: 'One', index: 1 },
        { label: 'Two', index: 2 },
        { label: 'Three', index: 3 }
    ];

    it('should render a group button with the given type and background color', () => {
        const mockClick = jest.fn();
        render(<ButtonGroup
            type={CustomTypes.ButtonGroupTypes.HEADER}
            backgroundColor={CustomTypes.PageColor.DARK}
            afterClick={mockClick}
            buttons={mockButtons} />);

        mockButtons.forEach(({ label, index }) => {
            const button = screen.queryByText(label);
            expect(button).toBeInTheDocument();
            expect(button).toHaveClass('button-group--header');
            expect(button).toHaveClass('button-group--header-dark');
        });
    });

    it('one button should be active when clicked', () => {
        const mockClick = jest.fn();
        render(<ButtonGroup
            type={CustomTypes.ButtonGroupTypes.HEADER}
            afterClick={mockClick}
            buttons={mockButtons} />);

        mockButtons.forEach(({ label, index }) => {
            const button = screen.queryByText(label) as HTMLButtonElement;

            fireEvent.click(button);

            expect(button).toHaveClass('button-group active');
            expect(mockClick).toHaveBeenCalled();
        });
    });

});