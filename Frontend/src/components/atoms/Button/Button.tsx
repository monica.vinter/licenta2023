import React, { ReactNode } from 'react';
import clsx from 'clsx';
import './Button.scss';
import { ButtonTypes, BackgroundThreeColor } from '../../../utils/customTypes';

interface ButtonProps {
  type: ButtonTypes;
  backgroundColor?: BackgroundThreeColor;
  label?: string;
  disabled?: boolean;
  icon?: ReactNode;
  onClick?: () => void;
}

export const Button = ({ type, backgroundColor, label, disabled, icon, ...props }: ButtonProps) => {
  const styles = { button: clsx('button', `button--${type}`, `button--color-${backgroundColor}`) };
  return (
    <button
      type='button'
      className={styles.button}
      disabled={disabled}
      {...props}>
      {icon && <p data-testid='button-icon'> {icon} </p>}
      <label> {label && label} </label>
    </button>
  );
};

