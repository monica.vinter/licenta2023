import { render, screen, fireEvent } from '@testing-library/react';
import { Button } from './Button';
import * as CustomTypes from '../../../utils/customTypes';
import { AiOutlineShoppingCart } from 'react-icons/ai';


describe('Button', () => {

    it('should render a button with the type primary', () => {
        render(<Button type={CustomTypes.ButtonTypes.PRIMARY} label='Button' />);
        const primaryButton = screen.getByRole('button', { name: /button/i });
        expect(primaryButton).toBeInTheDocument();
        expect(primaryButton).toHaveClass('button--primary');
    });

    it('should render a button with the type secondary', () => {
        render(<Button type={CustomTypes.ButtonTypes.SECONDARY} label='Button' />)
        const secondaryButton = screen.getByRole('button', { name: /button/i })
        expect(secondaryButton).toBeInTheDocument();
        expect(secondaryButton).toHaveClass('button--secondary')
    });

    it('should call onClick when enabled', () => {
        const mockClick = jest.fn();
        render(<Button type={CustomTypes.ButtonTypes.PRIMARY} onClick={mockClick} disabled={false} />);
        const button = screen.getByRole('button')
        fireEvent.click(button);
        expect(mockClick).toHaveBeenCalledTimes(1);
    });

    it('should not call onClick when disabled', () => {
        const mockClick = jest.fn();
        render(<Button type={CustomTypes.ButtonTypes.PRIMARY} onClick={mockClick} disabled />);
        const button = screen.getByRole('button')
        fireEvent.click(button);
        expect(mockClick).not.toHaveBeenCalled();
    });

    it('should display the icon given as prop', () => {
        const mockClick = jest.fn();
        render(<Button type={CustomTypes.ButtonTypes.PRIMARY} onClick={mockClick} icon={<AiOutlineShoppingCart />} />)
        const buttonWithIcon = screen.getByRole('button');
        const icon = screen.getByTestId('button-icon');

        expect(buttonWithIcon).toBeInTheDocument();
        expect(icon).toBeInTheDocument();
    });
});
