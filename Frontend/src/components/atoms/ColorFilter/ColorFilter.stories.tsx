import { Meta, ComponentStory } from '@storybook/react';
import { ColorFilter } from './ColorFilter';
import '../ColorFilter/ColorFilter.scss';
import { Culori } from '../../../utils/Constants';

const meta: Meta = {
    title: 'ColorFilter',
    component: ColorFilter,
};

export default meta;

export const SingleColor: ComponentStory<typeof ColorFilter> = (args) => <ColorFilter {...args} />;

SingleColor.args = {
    id: '1',
    value: 'red',
    color: 'red'
};

export const ColorGroup: ComponentStory<typeof ColorFilter> = (args) => {
    return (
        <div className='color-filter-group__container'>
            {Culori.map((color, index) => {
                return (
                    <ColorFilter
                        {...args}
                        value={color}
                        color={color}
                        key={index}
                    />
                );
            })};
        </div>
    );
};

ColorGroup.args = {
}