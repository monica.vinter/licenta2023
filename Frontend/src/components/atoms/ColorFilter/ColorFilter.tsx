import './ColorFilter.scss'
import React, { useState } from 'react';
import clsx from 'clsx';

export interface ColorFilterProps {
  id?: string;
  value?: string;
  color?: string;
  checked?: boolean;
  onChange: (value: boolean) => void;
}

export const ColorFilter = ({ id, value, color, checked = false, onChange }: ColorFilterProps) => {
  const [checkedColor, setCheckedColor] = useState(checked);
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.checked);
    setCheckedColor(!checkedColor);
  };

  const styles = {
    container: 'color-filter__container',
    input: clsx(
      'color-filter__input',
      `color-filter__input--${color}`,
      color !== 'white' && checkedColor && 'color-filter__input--checked',
      color === 'white' && checkedColor && 'color-filter__input--checked-white',
    ),
  };

  return (
    <div className={styles.container}>
      <input
        type='checkbox'
        id={id}
        value={value}
        checked={checkedColor}
        onChange={handleChange}
        className={styles.input}
      ></input>
    </div>
  );
};