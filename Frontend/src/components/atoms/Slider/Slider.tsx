import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import './Slider.scss';
import { BackgroundThreeColor, SliderStyleTypes } from '../../../utils/customTypes';

interface SliderProps {
  text?: string;
  backgroundColor?: BackgroundThreeColor;
  styleType?: SliderStyleTypes;
  index?: number;
  maxPrice?: string;
  onChange: (value: number) => void;
}

export const Slider = ({
  text,
  backgroundColor = BackgroundThreeColor.MEDIUM,
  styleType = SliderStyleTypes.INDEX,
  index = 0,
  maxPrice = '0',
  onChange,
}: SliderProps) => {
  const styles = {
    container: clsx('slider', `slider--${styleType}`),
    text: 'slider__text',
    inputWrapper: 'slider__input-wrapper',
    input: clsx('slider__input', `slider__input--color-${backgroundColor}`),
    value: 'slider__value',
  };
  const [value, setValue] = useState(index);

  const getBackgroundSize = () => {
    return {
      backgroundSize: `${value}% 100%`,
    };
  };

  const getBackgroundSizePrice = () => {
    const valPrice = (value * 100) / parseInt(maxPrice);
    return {
      backgroundSize: `${valPrice}% 100%`,
    };
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const index = parseInt(e.target.value);
    onChange(index);
    setValue(index);
  };

  useEffect(() => {
    setValue(index);
  }, [index]);

  return (
    <div className={styles.container}>
      {styleType === SliderStyleTypes.PRICE ?
        <>
          <div className={styles.inputWrapper}>
            <input
              type='range'
              min='0'
              max={maxPrice}
              value={value}
              onChange={handleChange}
              style={getBackgroundSizePrice()}
              className={styles.input}
            ></input>
            <div className={styles.value}>£{value}</div>
          </div>
        </>
        :
        <>
          <p className={styles.text}>{text}</p>
          <div className={styles.inputWrapper}>
            <input
              type='range'
              min='0'
              max='100'
              value={value}
              onChange={handleChange}
              style={getBackgroundSize()}
              className={styles.input}
            ></input>
            <div className={styles.value}>{value}</div>
          </div>
        </>
      }
    </div>
  );
};