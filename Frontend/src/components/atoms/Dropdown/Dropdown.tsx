import clsx from 'clsx';
import './Dropdown.scss';
import { PageColor } from '../../../utils/customTypes';
import React, { useState } from 'react';
import ReactFlagsSelect from 'react-flags-select';


interface DropdownProps {
  label?: string;
  name?: string;
  backgroundColor?: string;
  options?: { text?: string; value?: string }[];
  onChange?: (value: string) => void;
  placeholder?: string;
  forCountry?: boolean;
  error?: string;
  required?: boolean;
}

export const Dropdown = ({
  name,
  label,
  backgroundColor = PageColor.DARK,
  options = [{ text: 'test', value: 'test' }],
  onChange,
  placeholder,
  forCountry,
  error,
  required,
}: DropdownProps) => {

  const [selectedCountry, setSelectedCountry] = useState('');

  const handleSelect = (code: string) => {
    setSelectedCountry(code);
    onChange?.(code);
  };

  const styles = {
    container: clsx('dropdown__container', error && 'dropdown__container--error'),
    wrapper: 'dropdown__wrapper',
    dropdown: clsx('dropdown', `dropdown--${backgroundColor}`),
    label: clsx('dropdown__label', required && 'dropdown__label--required'),
    errorText: 'dropdown__error-text'
  };

  return (
    <div className={styles.container}>
      <label htmlFor={name} className={styles.label}>
        {label}
      </label>
      <div className={styles.wrapper}>
        {forCountry ?
          <>
            <ReactFlagsSelect
              selected={selectedCountry}
              onSelect={(code) => handleSelect(code)}
              placeholder={placeholder}
              searchPlaceholder='Search countries'
              searchable
            />
          </>
          :
          <>
            <select
              onChange={(e) => onChange?.(e.target.value)}
              name={name}
              id='courses'
              className={styles.dropdown}
              defaultValue=''
            >
              <option value='' disabled hidden>
                {placeholder}
              </option>
              {options.map((item) => (
                <option key={item.value} value={item.value}>
                  {item.text}
                </option>
              ))}
            </select>
          </>
        }
      </div>
      <div className={styles.errorText}>
        <p> {error} </p>
      </div>
    </div>
  );
};


