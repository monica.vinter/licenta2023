import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Dropdown } from './Dropdown';

export default {
  title: 'Dropdown',
  component: Dropdown,
} as ComponentMeta<typeof Dropdown>;

const Template: ComponentStory<typeof Dropdown> = (args) => (
  <div>
    {' '}
    <Dropdown {...args} />
  </div>
);

export const Dropdown1 = Template.bind({});

Dropdown1.args = {
  label: 'Course',
  options: [
    { text: 'Course 1', value: 'Course 1' },
    { text: 'Course 2', value: 'Course 2' },
    { text: 'Course 3', value: 'Course 3' },
    { text: 'Course 4', value: 'Course 4' },
  ],
  name: 'courses',
  onChange: (value) => {
    console.log(value);
  },
};
