import { ComponentStory, ComponentMeta } from '@storybook/react';
import { BreadcrumbsNav } from './BreadcrumbsNav';
import * as CustomTypes from '../../../utils/customTypes';

export default {
  title: 'BreadcrumbsNav',
  component: BreadcrumbsNav,
} as ComponentMeta<typeof BreadcrumbsNav>;

const Template: ComponentStory<typeof BreadcrumbsNav> = (args) => <BreadcrumbsNav {...args} />;

export const BreadcrumbsNav1 = Template.bind({});

BreadcrumbsNav1.args = {
  color: CustomTypes.BreadcrumbsNavColors.BROWN,
  links: [
    {
      url: '',
      label: 'Breadcrumb 1',
    },
    {
      url: '',
      label: 'Breadcrumb 2',
    },
    {
      url: '',
      label: 'Breadcrumb 3',
    },
    {
      url: '',
      label: 'Breadcrumb 4',
    },
    {
      url: '',
      label: 'Breadcrumb 5',
    },
    {
      url: '',
      label: 'Breadcrumb 6',
      active: true,
    },
  ],
};
