import clsx from 'clsx';
import './BreadcrumbsNav.scss';
import { BreadcrumbsNavColors } from '../../../utils/customTypes';
import { Link } from 'react-router-dom';

interface BreadcrumbsNavProps {
  color: BreadcrumbsNavColors;
  links: { url: string; label: string; active?: boolean }[];
}

export const BreadcrumbsNav = ({ color, links }: BreadcrumbsNavProps) => {
  const styles = {
    breadcrumbsNav: clsx('breadcrumbs-nav', `breadcrumbs-nav--${color}`),
    activeLink: clsx(`breadcrumbs-nav--${color}--active`),
  };
  return (
    (links.length <= 3)
      ?
      <ul className={styles.breadcrumbsNav}>
        {links.map((link, index) => (
          <li className={link.active ? styles.activeLink : styles.breadcrumbsNav} key={link.label + index}>
            <Link to={link.url}>{link.label}</Link>
          </li>
        ))}
      </ul>
      :
      <ul className={styles.breadcrumbsNav}>
        {links.slice(0, 1).map((link, index) => (
          <li className={link.active ? styles.activeLink : styles.breadcrumbsNav} key={link.label + index}>
            <Link to={link.url}>{link.label}</Link>
          </li>
        ))}
        <li>...</li>
        {links.slice(-2).map((link, index) => (
          <li className={link.active ? styles.activeLink : styles.breadcrumbsNav} key={link.label + index}>
            <Link to={link.url}>{link.label}</Link>
          </li>
        ))}
      </ul>
  );
};
