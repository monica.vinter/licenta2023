import { ComponentStory, ComponentMeta } from '@storybook/react';
import DatePicker from './DatePicker'
import { AiOutlineCalendar } from 'react-icons/ai';
import { MdError } from 'react-icons/md';
import { GrStatusWarningSmall } from 'react-icons/gr';
import { IoIosArrowDown } from 'react-icons/io';
export default {
  title: 'DatePicker',
  component: DatePicker,
} as ComponentMeta<typeof DatePicker>;

const Template: ComponentStory<typeof DatePicker> = (args) => <DatePicker {...args} />;

export const Default = Template.bind({});
Default.args = {
  icon: <AiOutlineCalendar fontSize={'2rem'} />,
  onChange: (value: Date) => console.log(value)
};

export const Disabled = Template.bind({});
Disabled.args = {
  icon: <AiOutlineCalendar fontSize={'2rem'} />,
  disabled: true
};

export const Error = Template.bind({});
Error.args = {
  icon: <MdError fontSize={'2rem'} />,
  error: 'Error message here'
};

export const Warning = Template.bind({});
Warning.args = {
  icon: <GrStatusWarningSmall fontSize={'2rem'} />,
  warning: 'Warning message here'
};

export const DefaultRange = Template.bind({});
DefaultRange.args = {
  isRangeDate: true,
  icon: <AiOutlineCalendar fontSize={'2rem'} />
};

export const DisabledRange = Template.bind({});
DisabledRange.args = {
  isRangeDate: true,
  icon: <AiOutlineCalendar fontSize={'2rem'} />,
  disabled: true
};

export const ErrorRange = Template.bind({});
ErrorRange.args = {
  isRangeDate: true,
  icon: <MdError fontSize={'2rem'} />,
  error: 'Error message here'
};

export const WarningRange = Template.bind({});
WarningRange.args = {
  isRangeDate: true,
  icon: <GrStatusWarningSmall fontSize={'2rem'} />,
  warning: 'Warning message here'
};

export const FormDatePicker = Template.bind({});
FormDatePicker.args = {
  icon: <IoIosArrowDown fontSize={'2rem'} color={'white'} />,
  isForm: true,
};
