import { useEffect, useRef, useState, ReactNode } from 'react'
import clsx from 'clsx';
import { Calendar, DateRange } from 'react-date-range'
import format from 'date-fns/format'
import './DatePicker.scss'
import 'react-date-range/dist/styles.css'
import 'react-date-range/dist/theme/default.css'

interface DatePickerProps {
  id?: string
  name?: string
  label?: string
  value?: Date
  icon?: ReactNode
  isRangeDate?: boolean
  isForm?: boolean
  required?: boolean
  disabled?: boolean
  error?: string
  warning?: string
  onChange?: (value: Date) => void
  onDateRangeChange?: (value: Date) => void
}

const DatePicker: React.FC<DatePickerProps> = ({ id, name, value, label, icon, isRangeDate = false, isForm, required = false, disabled, error, warning, onChange, onDateRangeChange, ...props }: DatePickerProps) => {
  const styles = {
    datepicker: clsx('datepicker',
      isRangeDate && 'datepicker__range',
      isForm && 'datepicker__form',
      error && 'datepicker__error',
      warning && 'datepicker__warning',
      disabled && 'datepicker__disabled'),
    datepickerContainer: 'datepicker__container',
    datepickerWrapper: 'datepicker__wrapper',
    datepickerLabel: clsx('datepicker__label', required && 'datepicker__label--required'),
    datepickerInput: clsx('datepicker__input', isRangeDate && 'datepicker__input_range', isForm && 'datepicker__input_form'),
    datepickerText: clsx(error && 'datepicker__error_text', warning && 'datepicker__warning_text'),
    calendarForm: clsx('calendar', isForm && 'calendar__form')
  };
  const [calendar, setCalendar] = useState(format(new Date(), 'MM/dd/yyyy'))
  const [open, setOpen] = useState(false)
  const refOne = useRef<HTMLInputElement>(null);

  const toggleCalendar = () => {
    setOpen(open => !open)
  }

  useEffect(() => {
    document.addEventListener('keydown', hideOnEscape, true)
    document.addEventListener('click', hideOnClickOutside, true)
  }, [])

  const hideOnEscape = (e: { key: string }) => {
    if (e.key === 'Escape') {
      setOpen(false)
    }
  }

  const hideOnClickOutside = (e: { target: any }) => {
    if (refOne.current && !refOne.current.contains(e.target)) {
      setOpen(false)
    }
  }

  const handleSelect = (date: any) => {
    setCalendar(format(date, 'MM/dd/yyyy'))
    onChange && onChange(date)
    setOpen(false)
  }

  const handleOnChange = (item: any) => {
    setRange([item.selection])
    onDateRangeChange && onDateRangeChange(item)
    if (item.selection.startDate !== item.selection.endDate)
      setOpen(false)
  }

  // date state
  const [range, setRange] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection'
    }
  ])

  return (
    <div>
      <div className={styles.datepickerContainer}>
        {label && <label className={styles.datepickerLabel}> {label} </label>}
        {isRangeDate ?
          <div className={styles.datepickerWrapper}>
            <div className={styles.datepicker}>
              <input
                id={id}
                name={name}
                placeholder="mm/dd/yyyy"
                value={`${format(range[0].startDate, 'MM/dd/yyyy')}`}
                className={styles.datepickerInput}
                onClick={toggleCalendar}
                {...props}
              />
              {icon}
            </div>

            <div className={styles.datepicker}>
              <input
                id={id}
                name={name}
                placeholder="mm/dd/yyyy"
                value={`${format(range[0].endDate, 'MM/dd/yyyy')}`}
                className={styles.datepickerInput}
                onClick={toggleCalendar}
                {...props}
              />
              {icon}
            </div>
          </div>
          :
          <div className={styles.datepicker}>
            <input
              id={id}
              name={name}
              placeholder="mm/dd/yyyy"
              value={calendar}
              className={styles.datepickerInput}
              onClick={toggleCalendar}
              onChange={handleSelect}
              {...props}
            />
            <div onClick={toggleCalendar} style={{ cursor: 'pointer' }}>
              {icon}
            </div>
          </div>}
        {error && <span className={styles.datepickerText}> {error}</span>}
        {warning && <span className={styles.datepickerText}> {warning}</span>}
      </div>

      {isRangeDate ?
        <div ref={refOne}>
          {open &&
            <DateRange
              onChange={handleOnChange}
              editableDateInputs={true}
              moveRangeOnFirstSelection={false}
              showDateDisplay={false}
              showMonthAndYearPickers={false}
              showPreview={false}
              ranges={range}
              months={1}
              direction="horizontal"
              className="calendarElement"
              weekdayDisplayFormat='EEEEE'
            />
          }
        </div>
        :
        <div ref={refOne} className={styles.calendarForm}>
          {open && (
            isForm ?
              <Calendar
                date={new Date()}
                minDate={new Date()}
                onChange={handleSelect}
                showMonthAndYearPickers={true}
                weekdayDisplayFormat='EEEEE'
              />
              :
              <Calendar
                date={new Date()}
                onChange={handleSelect}
                showMonthAndYearPickers={true}
                weekdayDisplayFormat='EEEEE'
              />
          )
          }
        </div>}
    </div>
  )
}
export default DatePicker