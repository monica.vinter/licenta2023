import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Input } from './Input';
import * as CustomTypes from '../../../utils/customTypes';
import { AiOutlineUser } from 'react-icons/ai';


describe('Input', () => {

    it('should render an input with the type primary', () => {
        render(<Input />)
        const primaryInput = screen.getByTestId('input');
        expect(primaryInput).toBeInTheDocument();
        expect(primaryInput).toHaveClass('input__wrapper--primary');
    });

    it('should render an input with the type secondary', () => {
        render(<Input styleType={CustomTypes.InputStyleTypes.SECONDARY} />)
        const secondaryInput = screen.getByTestId('input');
        expect(secondaryInput).toBeInTheDocument();
        expect(secondaryInput).toHaveClass('input__wrapper--secondary');
    });

    it('should be able to type into input', () => {
        const mockChange = jest.fn();

        render(<Input type={CustomTypes.InputTypes.TEXT} onChange={mockChange} />);
        const inputElement = screen.getByRole('textbox') as HTMLInputElement;
        fireEvent.click(inputElement);
        fireEvent.change(inputElement, { target: { value: 'Text' } });
        expect(mockChange).toHaveBeenCalledTimes(1);
        expect(inputElement.value).toBe('Text');
    });

    it('should not be able to type into disabled input', () => {
        const mockChange = jest.fn();

        render(<Input type={CustomTypes.InputTypes.TEXT} onChange={mockChange} disabled />);
        const disabledInput = screen.getByRole('textbox') as HTMLInputElement;
        fireEvent.click(disabledInput);
        expect(disabledInput.disabled).toBeTruthy();
    });

    it('should display the icon given as prop', () => {
        render(<Input type={CustomTypes.InputTypes.TEXT} rightIcon={<AiOutlineUser />} />)
        const inputWithIcon = screen.getByRole('textbox');
        const iconInput = screen.getByTestId('icon');

        expect(inputWithIcon).toBeInTheDocument();
        expect(iconInput).toBeInTheDocument();
        expect(iconInput).toHaveClass('input__icon');
    });

    it('should display the required indicator when needed', () => {
        render(<Input type={CustomTypes.InputTypes.TEXT} label={'Required input'} required />)
        const inputRequired = screen.getByRole('textbox');
        const labelInput = screen.getByText('Required input');

        expect(inputRequired).toBeInTheDocument();
        expect(labelInput).toBeInTheDocument();
        expect(labelInput).toHaveClass('input__label--required');
    });

    it('should display a tooltip with format information when needed', () => {
        render(<Input type={CustomTypes.InputTypes.TEXT} formatInfo={'Information about format'} />)
        const inputElement = screen.getByRole('textbox');
        const tooltip = screen.getByText('Information about format');

        expect(inputElement).toBeInTheDocument();
        expect(tooltip).toBeInTheDocument();
    });

    it('should display error message when transmitted', () => {
        render(<Input error={'Error message'} />)

        const inputWithError = screen.getByTestId('input');
        const errorText = screen.getByText('Error message');

        expect(errorText).toBeInTheDocument();
        expect(inputWithError).toBeInTheDocument();
        expect(inputWithError).toHaveClass('input__wrapper--error');
    });

    it('should have type password when given prop and change it to text when eye icon is clicked', () => {
        render(<Input type={CustomTypes.InputTypes.PASSWORD} label={'Password'} id={'1'} />)
        const inputPassword = screen.getByLabelText('Password');
        const eyeIcon = screen.getByTestId('eye-icon');

        expect(inputPassword).toBeInTheDocument();
        expect(eyeIcon).toBeInTheDocument();
        expect(inputPassword).toHaveAttribute('type', 'password');

        fireEvent.click(eyeIcon);
        expect(inputPassword).toHaveAttribute('type', 'text');
    });

});