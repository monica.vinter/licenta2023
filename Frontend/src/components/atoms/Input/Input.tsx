import React, { ReactNode, useState } from 'react';
import clsx from 'clsx';
import './Input.scss';
import { Tooltip } from '../Tooltip/Tooltip';
import { InputStyleTypes } from '../../../utils/customTypes';

import * as CustomTypes from '../../../utils/customTypes';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import InfoIcon from '../../../assets/InfoIcon.png';

export interface InputProps extends React.HTMLProps<HTMLInputElement | HTMLTextAreaElement> {
  label?: string;
  type?: string;
  styleType?: InputStyleTypes;
  rightIcon?: ReactNode;
  error?: string;
  disabled?: boolean;
  required?: boolean;
  formatInfo?: string;
}

export const Input: React.FC<InputProps> = ({ id, name, value, label, type, styleType = InputStyleTypes.PRIMARY, rightIcon, error = false, disabled = false, required = false, placeholder, formatInfo, onChange, onBlur }) => {
  const styles = {
    inputWrapper: clsx('input__wrapper', `input__wrapper--${styleType}`, error && 'input__wrapper--error', disabled && 'input__wrapper--disabled'),
    labelContainer: 'input__label-container',
    inputLabel: clsx('input__label', required && 'input__label--required'),
    inputIcon: 'input__icon',
    input: 'input__field',
    errorText: 'input__error-text',
  };

  const [visible, setVisible] = useState<boolean>(false);

  return (
    <div className={styles.inputWrapper} data-testid='input'>
      <div className={styles.labelContainer}>
        {label && <label htmlFor={id} className={styles.inputLabel}> {label} </label>}
        {formatInfo &&
          <Tooltip message={formatInfo}>
            <img src={`${InfoIcon}`} alt='InfoIcon' />
          </Tooltip>}
      </div>
      <div className={styles.input}>
        {type === CustomTypes.InputTypes.PASSWORD ? (
          <>
            <input id={id} type={visible ? CustomTypes.InputTypes.TEXT : CustomTypes.InputTypes.PASSWORD} name={name} value={value} placeholder={placeholder} onChange={onChange} onBlur={onBlur} disabled={disabled} />
            <div className={styles.inputIcon} style={{ cursor: 'pointer' }} onClick={() => setVisible(!visible)} data-testid='eye-icon'>
              {visible ? <AiFillEyeInvisible /> : <AiFillEye />}
            </div>
          </>
         ) : type === CustomTypes.InputTypes.MULTILINE ? (
          <textarea id={id} name={name} value={value} placeholder={placeholder} onChange={onChange} onBlur={onBlur} disabled={disabled} />
         ) : (
          <>
            <input id={id} type={type} name={name} value={value} placeholder={placeholder} onChange={onChange} onBlur={onBlur} disabled={disabled} />
            <div className={styles.inputIcon} data-testid='icon'>
              {rightIcon && rightIcon}
            </div>
          </>
         )
        }
      </div>
      <div className={styles.errorText}>
        <p> {error} </p>
      </div>
    </div>
  );
};
