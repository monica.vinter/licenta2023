import { Story, Meta } from '@storybook/react';
import { Input, InputProps } from './Input';
import { AiOutlineEye, AiOutlineLock } from 'react-icons/ai';
import { InputTypes } from '../../../utils/customTypes';

const meta: Meta = {
  title: 'Input',
  component: Input,
};
export default meta;

const Template: Story<InputProps> = (args) => <Input {...args} />;

export const Basic = Template.bind({});
Basic.args = {
  label: 'Input',
  placeholder: 'Type here',
  type: InputTypes.TEXT,
};


export const WithRightIcon = Template.bind({});
WithRightIcon.args = {
  label: 'Input',
  placeholder: 'Password',
  type: InputTypes.PASSWORD,
  rightIcon: <AiOutlineEye />,
};

export const Password = Template.bind({});
Password.args = {
  label: 'Password',
  placeholder: 'Password',
  type: InputTypes.PASSWORD,
  rightIcon: <AiOutlineLock />,
};

export const Error = Template.bind({});
Error.args = {
  label: 'Input',
  placeholder: 'Type here',
  type: InputTypes.TEXT,
  error: 'Error message here!',
};

export const Disabled = Template.bind({});
Disabled.args = {
  label: 'Input',
  placeholder: 'Type here',
  type: InputTypes.TEXT,
  disabled: true,
};
