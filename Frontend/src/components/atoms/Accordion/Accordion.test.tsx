import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Accordion } from './Accordion';
import { AccordionItem } from './AccordionItem/AccordionItem';


describe('Accordion', () => {

    const items = [{
        title: 'Title1',
        content: 'Content1'
    },
    {
        title: 'Title2',
        content: 'Content2'
    },
    {
        title: 'Title3',
        content: 'Content3'
    }
    ];

    it('should render all items', () => {
        render(<Accordion>
            {items.map((item, index) => {
                return (
                    <AccordionItem title={item.title} key={index}>
                        {item.content}
                    </AccordionItem>
                );
            })}
        </Accordion>
        );

        items.forEach(({ title, content }) => {
            const titleEl = screen.queryByText(title);
            const contentEl = screen.queryByText(content);

            expect(titleEl).toBeInTheDocument();
            expect(contentEl).toBeInTheDocument();
        });
    });

    it('should open and close items content when clicked', () => {
        render(<Accordion>
            {items.map((item, index) => {
                return (
                    <AccordionItem title={item.title} key={index}>
                        {item.content}
                    </AccordionItem>
                );
            })}
        </Accordion>
        );

        items.forEach(({ title, content }) => {
            const element = screen.getByText(title) as HTMLButtonElement;
            const contentEl = screen.queryByText(content);
            fireEvent.click(element);
            expect(contentEl).toHaveClass('accordion__content active');
            fireEvent.click(element);
            expect(contentEl).not.toHaveClass('accordion__content active');
        });

    });

});
