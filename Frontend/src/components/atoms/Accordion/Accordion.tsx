import React, { Children, cloneElement, ReactNode, ReactElement } from 'react';
import './Accordion.scss';
import { AccordionTypes, AccordionShapes, AccordionBackground } from '../../../utils/customTypes';

export interface AccordionProps {
  type?: AccordionTypes;
  shape?: AccordionShapes;
  backgroundColor?: AccordionBackground;
  children: ReactNode | ReactNode[] | ReactElement[];
}

export const Accordion: React.FC<AccordionProps> = ({
  type = AccordionShapes.BASIC,
  shape = AccordionTypes.BASIC,
  backgroundColor = AccordionBackground.DEFAULT,
  children
}) => {

  const styles = { accordionContainer: 'accordion__container' };

  const childrenArray = Children.toArray(children).map(child =>
    cloneElement(child as ReactElement, { shape, type, backgroundColor })
  );
  return (
    <div className={styles.accordionContainer}>
      {childrenArray}
    </div>
  );
};
