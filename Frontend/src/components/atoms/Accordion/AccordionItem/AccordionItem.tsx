import React, { ReactNode, useState } from 'react';
import clsx from 'clsx';
import { AiOutlinePlus, AiOutlineMinus } from 'react-icons/ai';
import { AccordionTypes, AccordionShapes, AccordionBackground } from '../../../../utils/customTypes';
import './AccordionItem.scss';

export interface AccordionItemProps {
  title: string;
  shape?: AccordionShapes;
  type?: AccordionTypes;
  firstOpen?: boolean;
  children: ReactNode | ReactNode[];
  backgroundColor?: AccordionBackground;
}

export const AccordionItem: React.FC<AccordionItemProps> = ({
  title,
  children,
  shape,
  type,
  firstOpen = false,
  backgroundColor,
}) => {

  const [isOpen, setIsOpen] = useState(firstOpen);
  const toggleAccordion = () => {
    setIsOpen(!isOpen);
  };

  const styles = {
    accordion: clsx(`accordion--${type}`, `accordion--${shape}`, `accordion--color-${backgroundColor}`),
    accordionIcon: clsx('accordion__icon', isOpen && 'accordion__icon active'),
    accordionTitle: clsx('accordion__title', isOpen && 'accordion__title active'),
    contentOpen: clsx('accordion__content', isOpen && 'accordion__content active')
  }

  return (
    <div className={styles.accordion}>
      <div className={styles.accordionTitle} onClick={toggleAccordion}>
        <div className={styles.accordionIcon}>
          {isOpen ? <AiOutlineMinus /> : <AiOutlinePlus />}
        </div>
        <h2>{title}</h2>
      </div>
      <div className={styles.contentOpen}>{children}</div>
    </div>
  );
};