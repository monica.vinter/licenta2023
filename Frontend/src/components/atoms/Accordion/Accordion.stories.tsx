import React from 'react';
import { Story, Meta } from '@storybook/react';
import { Accordion, AccordionProps } from './Accordion';
import { AccordionItem } from './AccordionItem/AccordionItem';
import { AccordionTypes, AccordionShapes } from '../../../utils/customTypes';

const meta: Meta = {
  title: 'Accordion',
  component: Accordion,
};
export default meta;

const Template: Story<AccordionProps> = (args) => (
  <Accordion {...args}>
    <AccordionItem title='Accordion Item 1'>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis numquam, ut laborum
      excepturi atque reiciendis cumque nostrum adipisci perferendis? Unde voluptates nulla
      assumenda, nihil esse hic numquam veniam asperiores recusandae.
    </AccordionItem>
    <AccordionItem title='Accordion Item 2'>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis numquam, ut laborum
      excepturi atque reiciendis cumque nostrum adipisci perferendis? Unde voluptates nulla
      assumenda, nihil esse hic numquam veniam asperiores recusandae.
    </AccordionItem>
    <AccordionItem title='Accordion Item 3'>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis numquam, ut laborum
      excepturi atque reiciendis cumque nostrum adipisci perferendis? Unde voluptates nulla
      assumenda, nihil esse hic numquam veniam asperiores recusandae.
    </AccordionItem>
  </Accordion>
);

export const Basic = Template.bind({});

export const Border = Template.bind({});
Border.args = {
  type: AccordionTypes.BORDER,
};

export const Margin = Template.bind({});
Margin.args = {
  type: AccordionTypes.MARGIN,
};

export const Rounded = Template.bind({});
Rounded.args = {
  shape: AccordionShapes.ROUNDED,
};

export const RoundedWithBorder = Template.bind({});
RoundedWithBorder.args = {
  shape: AccordionShapes.ROUNDED,
  type: AccordionTypes.BORDER,
};
export const RoundedWithMargin = Template.bind({});
RoundedWithBorder.args = {
  shape: AccordionShapes.ROUNDED,
  type: AccordionTypes.MARGIN,
};
export const BorderWithMargin = Template.bind({});
BorderWithMargin.args = {
  type: AccordionTypes.BORDER_MARGIN,
};
