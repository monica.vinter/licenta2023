import './SettingsMenu.scss';
import { MdDelete, MdOutlineModeEditOutline } from 'react-icons/md';
import { Link } from 'react-router-dom';
import { ACASA_PATH } from '../../../Routes/routesPath';
import { HiDotsHorizontal } from 'react-icons/hi';
import { useState } from 'react';

const SettingsMenu = () => {
  const [open, setOpen] = useState(false);
  const styles = {
    settingsContainer: 'settings-container',
    settingsButtonContainer: 'settings-container__button',
    settingsLinks: 'settings-container__links',
    settingsLinksEdit: 'settings-container__links--edit',
  };
  return (
    <div
      className={styles.settingsContainer}
      onMouseEnter={() => setOpen(!open)}
      onMouseLeave={() => setOpen(!open)}
    >
      <div className={styles.settingsButtonContainer}>
        <button>
          <HiDotsHorizontal />
        </button>
      </div>
      {open && (
        <div className={styles.settingsLinks}>
          <Link to={ACASA_PATH}>
            <MdOutlineModeEditOutline />
            Edit
          </Link>
          <Link to={ACASA_PATH}>
            <MdDelete />
            Delete
          </Link>
        </div>
      )}
    </div>
  );
};

export default SettingsMenu;
