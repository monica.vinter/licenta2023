import { useEffect, useState } from 'react';
import { IoIosArrowUp } from 'react-icons/io';
import './ButtonScrollToTop.scss';

export const ButtonScrollToTop = () => {
    const [isVisible, setIsVisible] = useState(false);
    const toggleVisibility = () => {
        if (window.pageYOffset > 300)
            setIsVisible(true);
        else
            setIsVisible(false);
    }
    const scrollToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        })
    }
    useEffect(() => {
        window.addEventListener('scroll', toggleVisibility);
        return () => {
            window.removeEventListener('scroll', toggleVisibility);
        }
    }, []);
    const styles = {
        buttonScrollToTop: 'button-scroll-to-top'
    }
    return (
        <>
            {isVisible && <button
                className={styles.buttonScrollToTop}
                onClick={scrollToTop}>
                <IoIosArrowUp style={{ fontSize: '2.5rem', color: 'black' }} />
            </button>}
        </>
    );
};

