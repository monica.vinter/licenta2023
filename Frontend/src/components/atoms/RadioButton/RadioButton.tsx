import './RadioButton.scss'
import React from 'react';
import clsx from 'clsx';

export interface RadioButtonProps {
  id?: string;
  name?: string;
  value?: string;
  label?: string;
  checked?: boolean;
  disabled?: boolean;
  onChange: (value: string) => void;
}

export const RadioButton = ({ id, name, value = 'Radio', checked = false, disabled = false, onChange, label }: RadioButtonProps) => {

  const styles = {
    container: 'radio-button__container',
    input: clsx('radio-button__input', checked && 'radio-button__input--checked', disabled && 'radio-button__input--disabled'),
    label: 'radio-button__label',
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };

  return (
    <div className={styles.container}>
      <input
        type='radio'
        id={id}
        name={name}
        value={value}
        disabled={disabled}
        checked={checked}
        onChange={handleChange}
        className={styles.input}
      >
      </input>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>
    </div>

  );
};