import React, { useState } from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { RadioButton } from './RadioButton';

const MockRadioGroup = () => {
  const [value, setValue] = useState('first');
  return (
    <div>
      <RadioButton
        name="group"
        value="first"
        checked={value === 'first'}
        onChange={(v) => setValue(v)}
        label="Radio"
      />
      <RadioButton
        name="group"
        value="second"
        checked={value === 'second'}
        onChange={(v) => setValue(v)}
        label="Radio"
      />
      <RadioButton
        name="group"
        value="third"
        checked={value === 'third'}
        onChange={(v) => setValue(v)}
        label="Radio"
      />
    </div>
  );
}

describe('RadioButton', () => {

  it('should be in the document and unchecked by default', () => {
    const mockChange = jest.fn();
    render(<RadioButton onChange={mockChange} />);

    const radioElement = screen.getByRole('radio');
    expect(radioElement).toBeInTheDocument();
    expect(radioElement).not.toBeChecked();
  });

  it('user should check one radio element at a time', () => {
    render(<MockRadioGroup />);

    const firstRadio = screen.getByDisplayValue('first');
    const secondRadio = screen.getByDisplayValue('second');
    const thirdRadio = screen.getByDisplayValue('third');

    expect(firstRadio).toBeChecked;

    fireEvent.click(secondRadio);
    expect(firstRadio).not.toBeChecked;
    expect(secondRadio).toBeChecked;

    fireEvent.click(thirdRadio);
    expect(secondRadio).not.toBeChecked;
    expect(thirdRadio).toBeChecked;

  });

  it('should be disabled when prop is true', () => {
    const mockChange = jest.fn();
    render(<RadioButton onChange={mockChange} disabled />);
    const radioElement = screen.getByRole('radio') as HTMLInputElement;
    expect(radioElement).toBeInTheDocument();
    expect(radioElement.disabled).toBeTruthy();
    expect(radioElement).toHaveClass('radio-button__input--disabled');
  });

});
