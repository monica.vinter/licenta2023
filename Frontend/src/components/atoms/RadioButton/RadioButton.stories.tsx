import React, { useState } from 'react';
import { Meta, ComponentStory } from '@storybook/react';
import { RadioButton } from './RadioButton';
import '../RadioButton/RadioButton.scss';

const meta: Meta = {
  title: 'RadioButton',
  component: RadioButton,
};

export default meta;

export const SingleRadio: ComponentStory<typeof RadioButton> = (args) => <RadioButton {...args} />;

SingleRadio.args = {
  id: '1',
  name: 'radio',
  value: 'radio',
  label: 'Radio'
};

export const RadioGroup: ComponentStory<typeof RadioButton> = (args) => {
  const [value, setValue] = useState('first');
  return (
    <div className='radio-group__container'>
      <label className='radio-group__label'>
        Label
      </label>
      <div className='radio-group__radio-buttons--inline'>
        <RadioButton
          {...args}
          name="group"
          value="first"
          checked={value === 'first'}
          onChange={(v) => setValue(v)}
          label="Radio"
        />
        <RadioButton
          {...args}
          name="group"
          value="second"
          checked={value === 'second'}
          onChange={(v) => setValue(v)}
          label="Radio"
        />
        <RadioButton
          {...args}
          name="group"
          value="third"
          checked={value === 'third'}
          onChange={(v) => setValue(v)}
          label="Radio"
        />
      </div>
    </div>
  );
};

RadioGroup.args = {
}



