import './Checkbox.scss'
import React, { useState } from 'react';
import clsx from 'clsx';

export interface CheckboxProps {
  id?: string;
  value?: string;
  label?: string;
  checked?: boolean;
  disabled?: boolean;
  onChange: (value: boolean) => void;
}

export const Checkbox = ({
  id,
  value = 'Checkbox',
  checked = false,
  disabled = false,
  onChange,
  label,
}: CheckboxProps) => {
  const [checkedInput, setCheckedInput] = useState(checked);
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.checked);
    setCheckedInput(!checkedInput);
  };

  const styles = {
    container: 'checkbox__container',
    input: clsx(
      'checkbox__input',
      checkedInput && 'checkbox__input--checked',
      disabled && 'checkbox__input--disabled',
    ),
    label: 'checkbox__label',
  };

  return (
    <div className={styles.container}>
      <input
        type='checkbox'
        id={id}
        value={value}
        disabled={disabled}
        checked={checkedInput}
        onChange={handleChange}
        className={styles.input}
      ></input>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>
    </div>
  );
};