import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Checkbox } from './Checkbox';


describe('Checkbox', () => {

  it('should be in the document and unchecked by default', () => {
    const mockChange = jest.fn();
    render(<Checkbox onChange={mockChange} />);

    const checkboxElement = screen.getByRole('checkbox');
    expect(checkboxElement).toBeInTheDocument();
    expect(checkboxElement).not.toBeChecked();
  });

  it('should be checked on click event', () => {
    const mockChange = jest.fn();
    render(<Checkbox onChange={mockChange} />);

    const checkboxElement = screen.getByRole('checkbox');

    fireEvent.click(checkboxElement);
    expect(checkboxElement).toHaveClass('checkbox__input--checked');
    expect(checkboxElement).toBeChecked();
    expect(mockChange).toHaveBeenCalledTimes(1);
  });

  it('should be disabled when prop is true', () => {
    const mockChange = jest.fn();
    render(<Checkbox onChange={mockChange} disabled />);
    const checkboxElement = screen.getByRole('checkbox') as HTMLInputElement;
    expect(checkboxElement).toBeInTheDocument();
    expect(checkboxElement.disabled).toBeTruthy();
    expect(checkboxElement).toHaveClass('checkbox__input--disabled');
  });

});