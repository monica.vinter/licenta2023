import React, { useState } from 'react';
import { Meta, ComponentStory } from '@storybook/react';
import { Checkbox } from './Checkbox';
import '../Checkbox/Checkbox.scss';

const meta: Meta = {
  title: 'Checkbox',
  component: Checkbox,
};

export default meta;

export const SingleCheckbox: ComponentStory<typeof Checkbox> = (args) => <Checkbox {...args} />;

SingleCheckbox.args = {
  id: '1',
  value: 'checkbox',
  label: 'Checkbox'
};

export const CheckboxGroup: ComponentStory<typeof Checkbox> = (args) => {

  return (
    <div className='checkbox-group__container'>
      <label className='checkbox-group__label'>
        Label
      </label>
      <div className='checkbox-group__checkboxes--block'>
        <Checkbox
          {...args}
          value="first"
          label="Checkbox"
        />
        <Checkbox
          {...args}
          value="second"
          label="Checkbox"
        />
        <Checkbox
          {...args}
          value="third"
          label="Checkbox"
        />
      </div>
    </div>
  );
};

CheckboxGroup.args = {
}