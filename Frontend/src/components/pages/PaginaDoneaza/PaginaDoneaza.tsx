import './PaginaDoneaza.scss';
import {
} from '../../molecules/index';
import { BreadcrumbsNav } from '../../atoms/index';
import * as MockData from '../../../mockData/mockDataComponents';
import { InformatiiAsociatie } from '../../molecules';

const PaginaDoneaza: React.FC = () => {
  const styles = {
    paginaDoneaza: 'pagina_doneaza',
    content: 'pagina_doneaza__content',
    asociatie: 'asociatie',
    breadCrumbs: 'asociatie__breadCrumbs',
    header: 'asociatie__header',
    title: 'asociatie__header__title',
    text: 'asociatie__header__text',
    mariuta: 'asociatie__mariuta',
    hope: 'asociatie__hope',
  };
  return (
    <>
      <div className={styles.paginaDoneaza}>
        <div className={styles.breadCrumbs}>
          <BreadcrumbsNav {...MockData.PaginaDoneazaDate.breadCrumbs} />
        </div>
        <div className={styles.asociatie}>
          <div className={styles.header}>

          </div>
          <div className={styles.mariuta}>
            <InformatiiAsociatie {...MockData.PaginaDoneazaDate.content.Mariuta} />
          </div>
          <div className={styles.hope}>
            <InformatiiAsociatie {...MockData.PaginaDoneazaDate.content.Hope} />
          </div>
        </div>
      </div>
    </>
  );
};

export default PaginaDoneaza;
