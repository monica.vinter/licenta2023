import './PaginaAnimal.scss';
import { useState, useMemo, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as CustomTypes from '../../../utils/customTypes';
import { } from '../../atoms/index';
import { ButtonGroup } from '../../atoms/ButtonGroup/ButtonGroup';
import * as MockData from '../../../mockData/mockDataComponents';
import { CardAnimal } from '../../molecules/CardAnimal/CardAnimal';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getAnimale } from '../../../redux/features/animalSlice';
import { addToFormulare } from '../../../redux/features/formulareSlice';
import FormularAdoptie from '../../molecules/FormularAdoptie/FormularAdoptie/FormularAdoptie';
import { useCart } from '../../../utils/customHooks';

import {
  ImaginiAnimale,
  PaginaAdoptaBackground,
} from '../../../assets/imagePaths';
import { BsBook, BsFillPencilFill, BsFillTelephoneOutboundFill } from 'react-icons/bs';
import clsx from 'clsx';
import { IoMdContact, IoMdMail } from 'react-icons/io';
import { MdDescription } from 'react-icons/md';
import { RiContactsFill } from 'react-icons/ri';
import { LOGIN_PATH } from '../../../Routes/routesPath';


const PaginaAnimal: React.FC = () => {
  const dispatch = useDispatch<AppThunkDispatch>();

  useEffect(() => {
    dispatch(getAnimale());
  }, []);

  const [animalPhoto, setAnimalPhoto] = useState<string[]>([ImaginiAnimale, ImaginiAnimale, ImaginiAnimale]);

  const animale = useSelector((state: RootState) => state.animalState.animale);
  const animalId = useParams();

  const foundAnimal = animale.find((element) => {
    return element._id === animalId.id;
  });

  const ButtonGroupAnimale = [
    {
      title: 'Descriere',
      descriere: foundAnimal?.descriere,
    },
    {
      title: 'Date de contact stăpân',
      numeStapan: foundAnimal?.numeStapan,
      numarTelefonStapan: foundAnimal?.numarTelefonStapan,
      emailStapan: foundAnimal?.emailStapan,
    }
  ];
  const [label, setLabel] = useState('Descriere');
  const found = useMemo(
    () =>
      ButtonGroupAnimale.find((element) => {
        return element.title === label;
      }),
    [label],
  );

  const { isOpen, toggle } = useCart();
  const [clicked, setClicked] = useState(false);
  const navigate = useNavigate();
  const { isConnected, userData } = useSelector((state: RootState) => state.userState);

  const handleClick = () => {
    if (isConnected) {
      dispatch(addToFormulare({ foundAnimal }));
      toggle();
      {
        toggle && (
          <FormularAdoptie
            isOpen={isOpen}
            toggle={toggle}
            title={MockData.FormularAdoptie.title}
            subTitle={MockData.FormularAdoptie.subTitle}
            buttonLabel={MockData.FormularAdoptie.buttonLabel}
          ></FormularAdoptie>
        )
      }
      setClicked(true);
      setTimeout(() => {
        setClicked(false);
      }, 5000);
    }
    else {
      navigate(LOGIN_PATH);
    }
  };

  const styles = {
    paginaAnimal: 'pagina_animal',
    image: 'pagina_animal__image',
    mainContainer: 'pagina_animal__main-container',
    upperSection: 'pagina_animal__main-container__upper-section',
    leftPartContainer: 'pagina_animal__main-container__upper-section__left',
    bigPhoto: 'pagina_animal__main-container__upper-section__left__big-photo',
    rightPart: 'pagina_animal__main-container__upper-section__right-part',
    rightPartFirstP: 'pagina_animal__main-container__upper-section__right-part__first',
    rightPartSecondP: 'pagina_animal__main-container__upper-section__right-part__second',
    rightPartLine: 'pagina_animal__main-container__upper-section__right-part__line',
    buttonWithFormular: clsx(
      'pagina_animal__main-container__upper-section__right-part__button',
      clicked && 'pagina_animal__main-container__upper-section__right-part__button--clicked',
    ),
    labelAddToFormular: 'pagina_animal__main-container__upper-section__right-part__button__add-to-formular',
    labelAnimalAdded:
      'pagina_animal__main-container__upper-section__right-part__button__animal-added',
    formularIcon: 'pagina_animal__main-container__upper-section__right-part__button__formular-icon',
    boxIcon: 'pagina_animal__main-container__upper-section__right-part__button__box-icon',
    buttonGroup: 'pagina_animal__main-container__upper-section__right-part__button-group',
    formButtonGroup: 'pagina_animal__main-container__upper-section__right-part__button-group--form',
    informationButtonGroup: 'pagina_animal__main-container__upper-section__right-part__button-group--form--information',
    descriereButtonGroup: 'pagina_animal__main-container__upper-section__right-part__button-group--form--descriere',
    fieldsButtonGroupMain: 'pagina_animal__main-container__upper-section__right-part__button-group--form--fields',
    animalPageRecomanded: 'pagina_animal__main-container__recomanded',
    animalPageRecomandedTitle: 'pagina_animal__main-container__recomanded--title',
    animalPageRecomandedAnimals: 'pagina_animal__main-container__recomanded--animals',
  };


  return (
    <div className={styles.paginaAnimal}>
      <img src={PaginaAdoptaBackground} className={styles.image} alt='Animale' />

      <div className={styles.mainContainer}>
        <div className={styles.upperSection}>
          <div className={styles.leftPartContainer}>
            <div className={styles.bigPhoto}>
              <img
                src={`https://res.cloudinary.com/dssizmzir/image/upload/${foundAnimal?.imagini[0]}`}
                key={animalPhoto[0]}
              />
            </div>
          </div>
          <div className={styles.rightPart}>
            <p className={styles.rightPartSecondP}>{foundAnimal?.titluAnunt}, <span className={styles.informationButtonGroup}> {foundAnimal?.varsta} </span> </p>
            <hr className={styles.rightPartLine} />
            <button className={styles.buttonWithFormular} onClick={handleClick}>
              <span className={styles.labelAddToFormular}> FORMULAR ADOPȚIE </span>
              <span className={styles.labelAnimalAdded}> SPRE FORMULAR </span>
              <span className={styles.formularIcon}>
                <BsBook />
              </span>
              <span className={styles.boxIcon}>
                <BsFillPencilFill />
              </span>
            </button>
            <div className={styles.buttonGroup}>
              <ButtonGroup
                type={CustomTypes.ButtonGroupTypes.HEADER}
                buttons={[
                  { label: 'Descriere', index: 1, icon: <MdDescription /> },
                  { label: 'Date de contact stăpân', index: 2, icon: <RiContactsFill /> },
                ]}
                backgroundColor={CustomTypes.PageColor.DARK}
                afterClick={(label) => {
                  setLabel(label);
                }
                }
              />
              <div className={styles.formButtonGroup}>
                {label === 'Date de contact stăpân' ? (
                  <>
                    <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${foundAnimal?.imagineProfilStapan}`} />
                    <div className={styles.fieldsButtonGroupMain}>
                      <h1>  <span className='icon'><IoMdContact /></span> &nbsp; Nume: <span className={styles.informationButtonGroup}> {found && found.numeStapan} </span> </h1>
                      <h1>  <span className='icon'><BsFillTelephoneOutboundFill /></span> &nbsp; Număr de telefon: <span className={styles.informationButtonGroup}> {found && found.numarTelefonStapan} </span></h1>
                      <h1>  <span className='icon'><IoMdMail /></span> &nbsp; Email: <span className={styles.informationButtonGroup}> {found && found.emailStapan} </span> </h1>
                    </div>
                  </>
                ) : (
                  <div className={styles.descriereButtonGroup}>
                    <h1> &nbsp;&nbsp;&nbsp;{found && found.descriere} </h1>
                  </div>)}
              </div>

            </div>
          </div>
        </div>
        <div className={styles.animalPageRecomanded}>
          <h2 className={styles.animalPageRecomandedTitle}>Alte animale care caută stăpân</h2>
          <div className={styles.animalPageRecomandedAnimals}>
            {MockData.UltimeleAnunturi.map((animal, index) => {
              return (
                <CardAnimal
                  buttonLabel={animal.buttonLabel}
                  key={animal.name + index}
                  id={animal.id}
                  image={animal.image}
                  onClick={() => {
                    console.log('');
                  }}
                />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaginaAnimal;
