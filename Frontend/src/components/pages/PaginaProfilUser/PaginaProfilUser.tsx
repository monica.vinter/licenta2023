import './PaginaProfilUser.scss';
import { Button } from '../../atoms';
import * as CustomTypes from '../../../utils/customTypes';
import { FiMail, FiPhone, FiCalendar, FiCamera } from 'react-icons/fi';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { MdCallMade, MdCallReceived } from 'react-icons/md';
import { getFormulare } from '../../../redux/features/profileSlice';
import { getUser } from '../../../redux/features/userSlice';
import { FormulareTrimise, FormularePrimite } from '../../molecules';
import { useNavigate } from 'react-router-dom';
import { RESETPASSUSERLOGGEDIN_PATH, PAGINA_USER_ANUNTURI_PATH } from '../../../Routes/routesPath';
import Axios from 'axios';
import { UpdateImagineUser, StergeUser } from '../../../API/userAPI';
import {
  FormButtonsLabel,
} from '../../../mockData/mockDataForms';
import { deleteCookie } from '../../../utils/utilsFunctions';
import { TOKEN } from '../../../utils/Constants';
import { setToast } from '../../../redux/features/toastSlice';
import { LOGGED_OUT } from '../../../utils/toastMessages';


const PaginaProfilUser: React.FC = () => {
  const styles = {
    paginaProfilUser: 'pagina-profil-user',
    profile: 'pagina-profil-user__profile',
    camera: 'pagina-profil-user__profile__camera',
    image: 'pagina-profil-user__profile__image',
    name: 'pagina-profil-user__profile__name',
    info: 'pagina-profil-user__profile__info',
    email: 'pagina-profil-user__profile__info__email',
    phone: 'pagina-profil-user__profile__info__phone',
    bday: 'pagina-profil-user__profile__info__bday',
    right: 'pagina-profil-user__right',
    formulare: 'pagina-profil-user__right__formulare',
    orderTitle: 'pagina-profil-user__right__formulare__title',
    containerFormular: 'pagina-profil-user__right__formulare__containter',
  };
  useEffect(() => {
    dispatch(getFormulare());
  }, []);

  const navigate = useNavigate();
  const dispatch = useDispatch<AppThunkDispatch>();
  const { userData } = useSelector((state: RootState) => state.userState);
  const { formulare } = useSelector((state: RootState) => state.formulareState);
  const [img, setImg] = useState('');
  useEffect(() => {
    if (img !== '') {
      handleSubmit();
    }
  }, [img]);

  const handleImage = async (files: FileList) => {
    console.log(files[0]);
    const formData: any = new FormData();
    formData.append('upload_preset', 'dcnhkgdy');
    formData.append('file', files[0]);

    Axios.post(
      'https://api.cloudinary.com/v1_1/dssizmzir/image/upload',
      formData
    ).then((response) => {
      setImg(response.data.public_id);
      handleSubmit(); 
    });
  };

  const handleSubmit = async () => {
    const data = await UpdateImagineUser(`/users/${userData?._id}`, img);
    dispatch(getUser());
  };

  const logoutHandler = () => {
    deleteCookie(TOKEN);
    const toastData = { message: LOGGED_OUT, success: true };
    dispatch(setToast(toastData));
    const timeout = setTimeout(() => {
      window.location.reload();
    }, 3000);
    return () => {
      clearTimeout(timeout);
    };
  };

  const handleSubmitStergere = async () => {
    const data = await StergeUser(`/users/${userData?._id}`);
    console.log(userData?._id);
    logoutHandler();
  };

  return (
    <div className={styles.paginaProfilUser}>
      <div className={styles.profile}>
        <img className={styles.image} src={`https://res.cloudinary.com/dssizmzir/image/upload/${userData?.imagine}`} />
        <div className={styles.camera}>
          <label className='upload-icon'>
            <input
              type='file'
              accept='image/*'
              onChange={(event) => {
                handleImage(event.currentTarget.files!)
              }}
            />
            <FiCamera />
          </label>
        </div>
        <p className={styles.name}>
          {userData?.firstname.toUpperCase() + ' ' + userData?.lastname.toUpperCase()}
        </p>
        <div className={styles.info}>
          <div className={styles.email}>
            <FiMail /> <span>{userData?.email}</span>
          </div>
          <div className={styles.phone}>
            <FiPhone />
            <span>{userData?.phone}</span>
          </div>
          <div className={styles.bday}>
            <FiCalendar />
            <span> {userData?.birthday.substring(0, 10)}</span>
          </div>
        </div>
        <Button
          type={CustomTypes.ButtonTypes.PRIMARY}
          label={'Vizualizează-ți anunțurile'}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          onClick={() => navigate(PAGINA_USER_ANUNTURI_PATH)}
        />
        <Button
          type={CustomTypes.ButtonTypes.PRIMARY}
          label={'Resetează parola'}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          onClick={() => navigate(RESETPASSUSERLOGGEDIN_PATH)}
        />
        <Button
          type={CustomTypes.ButtonTypes.PRIMARY}
          label={'Șterge acest cont'}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          onClick={() => handleSubmitStergere()}
        />

      </div>
      <div className={styles.right}>
        <div className={styles.formulare}>
          <div className={styles.orderTitle}>
            <MdCallMade /> <p> FORMULARE TRIMISE </p>
          </div>
          {formulare
            .slice(0)
            .reverse()
            .map((formular, index) => {
              return (
                formular.user?.email === userData?.email && (
                  <FormulareTrimise {...formular} id={formular?._id} key={index} />
                )
              );
            })}
        </div>

        <div className={styles.formulare}>
          <div className={styles.orderTitle}>
            <MdCallReceived /> <p> FORMULARE PRIMITE </p>
          </div>
          {formulare
            .slice(0)
            .reverse()
            .map((formular, index) => {
              return (
                formular.emailStapanAnimal === userData?.email && (
                  <FormularePrimite {...formular} id={formular?._id} key={index} />
                )
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default PaginaProfilUser;
