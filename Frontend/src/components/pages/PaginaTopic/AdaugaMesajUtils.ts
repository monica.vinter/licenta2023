import {FormRequiredInputs, FormFormatInputs} from '../../../mockData/mockDataForms';

export const DateMesaj = {
    mesaj: {
        required: FormRequiredInputs.mesaj,
        format: FormFormatInputs.mesaj,
    },
};
