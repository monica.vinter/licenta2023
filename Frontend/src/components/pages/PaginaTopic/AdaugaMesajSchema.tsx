import { REGEX_NAME } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface adaugaMesajSchemaProps {
  mesaj: {
    required: string;
    format: string;
  };
}

const adaugaMesajSchema = (validationMsgs: adaugaMesajSchemaProps) => {

  const { mesaj } = validationMsgs;

  return yup.object({
    mesaj: yup
      .string()
      .trim()
      .required(mesaj.required)
  })
};

export default adaugaMesajSchema;