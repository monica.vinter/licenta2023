import './PaginaTopic.scss';
import { useEffect } from 'react';
import { Input } from '../../atoms/Input/Input';
import { Button, BreadcrumbsNav } from '../../atoms/index';
import * as MockData from '../../../mockData/mockDataComponents';
import { MesajCardProfile } from '../../molecules';

import {
  FormButtonsLabel,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import * as CustomTypes from '../../../utils/customTypes';
import { useSelector, useDispatch } from 'react-redux';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { useFormik } from 'formik';
import { setToast } from '../../../redux/features/toastSlice';
import { adaugaMesaj } from '../../../API/mesajeAPI';
import { ADAUGAREMESAJ_SUCCESS, SERVER_ERROR } from '../../../utils/toastMessages';
import adaugaMesajSchema from './AdaugaMesajSchema';
import { DateMesaj } from './AdaugaMesajUtils';
import { getMesaje } from '../../../redux/features/mesajSlice';
import { BiCommentAdd } from 'react-icons/bi';
import { updateNumarComentariiTopic } from '../../../API/topicuriAPI';
import { useNavigate } from 'react-router-dom';
import { LOGIN_PATH } from '../../../Routes/routesPath';

const PaginaTopic = () => {

  const styles = {
    paginaForum: 'pagina_topic',
    pMessage: 'pagina_topic__p-message',
    navLinks: 'pagina_topic__nav_links',
    container: 'pagina_topic__container',
    adaugaTopic: 'pagina_topic__container__adauga-topic',
    singleInputRow: 'pagina_topic__container__single-input-row',
    submitButton: 'pagina_topic__container__submit-button',

  };

  useEffect(() => {
    dispatch(getMesaje());
  }, []);

  const { mesaje } = useSelector((state: RootState) => state.mesajeState);

  let numMesaje = mesaje.filter(mesaj => mesaj.topic === window.location.pathname.substring(14)).length;
  const dispatch = useDispatch<AppThunkDispatch>();
  const { isConnected, userData } = useSelector((state: RootState) => state.userState);
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      mesaj: '',

    },

    onSubmit: async function (values) {

      if (isConnected) {
        const data = await updateNumarComentariiTopic(`/topicuri/${window.location.pathname.substring(14)}`, ++numMesaje);

        console.log(values)
        const toastErrorData = { message: SERVER_ERROR, success: false };
        try {
          const DateMesaj = {
            mesaj: values.mesaj,
            user: userData,
            topic: window.location.pathname.substring(14),
            numeUtilizator: userData?.firstname + ' ' + userData?.lastname,
            imagineProfilUtilizator: userData?.imagine,
          };
          console.log(window.location.pathname.substring(14));
          const data = await adaugaMesaj(DateMesaj);
          dispatch(getMesaje());
          if (data) {
            const toastData = { message: ADAUGAREMESAJ_SUCCESS, success: true };
            dispatch(setToast(toastData));

          } else {
            dispatch(setToast(toastErrorData));
          }
        } catch (error) {
          dispatch(setToast(toastErrorData));
        }
      }
      else {
        navigate(LOGIN_PATH);
      }
    },

    validationSchema: adaugaMesajSchema(DateMesaj),

  });


  return (
    <>
      <div className={styles.paginaForum}>
        <div className={styles.navLinks}>
          <BreadcrumbsNav {...MockData.PaginaTopicDate.breadCrumbs} />
        </div>
        <div className={styles.container}>
          <div className={styles.adaugaTopic}>
            <form>
              <h1> {FormInformation.adaugaMesajHere} </h1>
              <div className={styles.singleInputRow}>
                <Input
                  id={FormInputNames.mesaj}
                  name={FormInputNames.mesaj}
                  type={CustomTypes.InputTypes.MULTILINE}
                  styleType={CustomTypes.InputStyleTypes.TEXTAREA}
                  placeholder={FormInputPlaceholders.mesaj}
                  required
                  error={
                    formik.touched.mesaj && formik.errors.mesaj
                      ? formik.errors.mesaj
                      : undefined
                  }
                  value={formik.values.mesaj}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />

              </div>

              <div className={styles.submitButton}>
                <Button
                  icon={<BiCommentAdd></BiCommentAdd>}
                  type={CustomTypes.ButtonTypes.PRIMARY}
                  label={FormButtonsLabel.adaugaMesaj}
                  backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
                  onClick={() => formik.handleSubmit()}
                />
              </div>
            </form>
          </div>
          {mesaje
            .slice(0)
            .reverse()
            .map((mesaj, index) => {
              return (
                mesaj.topic === window.location.pathname.substring(14) && (
                  <MesajCardProfile {...mesaj} id={mesaj?._id} key={index} />
                )
              );

            })}
        </div>
      </div>
    </>
  );
};

export default PaginaTopic;

