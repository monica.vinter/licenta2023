import './PaginaAcasa.scss';
import * as MockData from '../../../mockData/mockDataComponents';
import * as MockDataCarousels from '../../../mockData/mockDataCarousels';
import { MostPopularModelsCarousel } from '../../molecules/CarouselComponents/MostPopularModelsCarousel';
import { PortalAdoptii } from '../../molecules/PortalAdoptii/PortalAdoptii';
import { Carousel } from '../../molecules/StageCarousel/StageCarousel';
import { CategoriiAnimale } from '../../molecules/CategoriiAnimale/CategoriiAnimale';
import { getAnimale } from '../../../redux/features/animalSlice';
import { useDispatch } from 'react-redux';
import { AppThunkDispatch } from '../../../redux/store';
import { useEffect } from 'react';

const PaginaAcasa: React.FC = () => {
  const dispatch = useDispatch<AppThunkDispatch>();

  useEffect(() => {
    dispatch(getAnimale());
  });
  const styles = {
    PaginaAcasaContainer: 'pagina-acasa-page',
    PaginaAcasaMargins: 'pagina-acasa-margin',
    PaginaAcasaBlackBackground: 'pagina-acasa__black-background',
    PaginaAcasaCarouselContainer: 'pagina-acasa__carousel-containter',
  };

  return (
    <div className={styles.PaginaAcasaContainer}>
      <Carousel
        title={MockDataCarousels.StageCarouselShopPage.title}
        buttonLabel={MockDataCarousels.StageCarouselShopPage.buttonLabel}
        textAreaTitle={MockDataCarousels.StageCarouselShopPage.textAreaTitle}
        textAreaContent={MockDataCarousels.StageCarouselShopPage.textAreaContent}
        disableTime={MockDataCarousels.StageCarouselShopPage.disableTime}
        sliderImage={MockDataCarousels.SliderCarouselImages}
      />
      <div className={styles.PaginaAcasaBlackBackground} />
      <CategoriiAnimale />
      <div className={styles.PaginaAcasaMargins}>
      </div>
      <div className={styles.PaginaAcasaCarouselContainer}>
        <MostPopularModelsCarousel />
      </div>
      <PortalAdoptii
        title={MockData.PortalAdoptiiDate.title}
        text={MockData.PortalAdoptiiDate.text}
        label={MockData.PortalAdoptiiDate.label}
        type={MockData.PortalAdoptiiDate.type}
      />
    </div>
  );
};
export default PaginaAcasa;
