import './PaginaAnimalAdmin.scss';
import { useState, useMemo, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import * as CustomTypes from '../../../utils/customTypes';
import { } from '../../atoms/index';
import { ButtonGroup } from '../../atoms/ButtonGroup/ButtonGroup';
import * as MockData from '../../../mockData/mockDataComponents';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { getAnimale } from '../../../redux/features/animalSlice';
import { addToFormulare } from '../../../redux/features/formulareSlice';
import FormularAdoptie from '../../molecules/FormularAdoptie/FormularAdoptie/FormularAdoptie';
import { useCart } from '../../../utils/customHooks';
import { Button } from '../../atoms/Button/Button';
import { updateStatusAnuntAcceptat, updateStatusAnuntRefuzat } from '../../../API/animaleAPI';
import { ANUNT_ACCEPTAT_SUCCES, ANUNT_REFUZAT_SUCCES } from '../../../utils/toastMessages';

import {
  ImaginiAnimale,
  PaginaAnimalAdminBackground,
} from '../../../assets/imagePaths';
import { BsBoxSeam, BsFillCartCheckFill, BsFillTelephoneOutboundFill } from 'react-icons/bs';
import clsx from 'clsx';
import { setToast } from '../../../redux/features/toastSlice';
import { AiFillCheckCircle, AiFillCloseCircle } from 'react-icons/ai';
import { IoMdContact, IoMdMail } from 'react-icons/io';
import { MdDescription } from 'react-icons/md';
import { RiContactsFill } from 'react-icons/ri';
import { FaCheckCircle, FaClock, FaTimesCircle } from 'react-icons/fa';

const PaginaAnimalAdmin: React.FC = () => {
  const dispatch = useDispatch<AppThunkDispatch>();

  useEffect(() => {
    dispatch(getAnimale());
  }, []);

  const [animalPhoto, setAnimalPhoto] = useState<string[]>([ImaginiAnimale, ImaginiAnimale, ImaginiAnimale]);

  const animale = useSelector((state: RootState) => state.animalState.animale);
  const animalId = useParams();

  const foundAnimal = animale.find((element) => {
    return element._id === animalId.id;
  });

  const ButtonGroupAnimale = [
    {
      title: 'Descriere',
      descriere: foundAnimal?.descriere,
    },
    {
      title: 'Date de contact stăpân',
      numeStapan: foundAnimal?.numeStapan,
      numarTelefonStapan: foundAnimal?.numarTelefonStapan,
      emailStapan: foundAnimal?.emailStapan,
    }
  ];
  const [label, setLabel] = useState('Descriere');
  const found = useMemo(
    () =>
      ButtonGroupAnimale.find((element) => {
        return element.title === label;
      }),
    [label],
  );

  const { isOpen, toggle } = useCart();
  const [clicked, setClicked] = useState(false);

  const AcceptaAnunt = () => {
    updateStatusAnuntAcceptat(`/animale/${foundAnimal?._id}`);
    console.log(foundAnimal?._id);
    const toastData = { message: ANUNT_ACCEPTAT_SUCCES, success: true };
    dispatch(setToast(toastData));
  };
  const RefuzaAnunt = () => {
    updateStatusAnuntRefuzat(`/animale/${foundAnimal?._id}`);
    const toastData = { message: ANUNT_REFUZAT_SUCCES, success: true };
    dispatch(setToast(toastData));
  };

  const styles = {
    paginaAnimal: 'pagina_animal_admin',
    buttons: 'pagina_animal_admin__main-container__button',
    image: 'pagina_animal_admin__image',
    status: 'pagina_animal_admin__main-container__upper-section__right-part__status',
    shoppingSquare: 'pagina_animal_admin__square',
    mainContainer: 'pagina_animal_admin__main-container',
    upperSection: 'pagina_animal_admin__main-container__upper-section',
    leftPartContainer: 'pagina_animal_admin__main-container__upper-section__left',
    photosContainer: 'pagina_animal_admin__main-container__upper-section__left__photos',
    bigPhoto: 'pagina_animal_admin__main-container__upper-section__left__big-photo',
    leftPartSecondPhotos: 'pagina_animal_admin__main-container__upper-section__left__left-part--photos',
    rightPart: 'pagina_animal_admin__main-container__upper-section__right-part',
    rightPartSecondP: 'pagina_animal_admin__main-container__upper-section__right-part__second',
    rightPartLine: 'pagina_animal_admin__main-container__upper-section__right-part__line',
    buttonWithFormular: clsx(
      'pagina_animal_admin__main-container__upper-section__right-part__button',
      clicked && 'pagina_animal_admin__main-container__upper-section__right-part__button--clicked',
    ),
    labelAddToFormular: 'pagina_animal_admin__main-container__upper-section__right-part__button__add-to-formular',
    labelAnimalAdded:
      'pagina_animal_admin__main-container__upper-section__right-part__button__animal-added',
    formularIcon: 'pagina_animal_admin__main-container__upper-section__right-part__button__formular-icon',
    boxIcon: 'pagina_animal_admin__main-container__upper-section__right-part__button__box-icon',
    buttonGroup: 'pagina_animal_admin__main-container__upper-section__right-part__button-group',
    formButtonGroup: 'pagina_animal_admin__main-container__upper-section__right-part__button-group--form',
    informationButtonGroup: 'pagina_animal_admin__main-container__upper-section__right-part__button-group--form--information',
    descriereButtonGroup: 'pagina_animal_admin__main-container__upper-section__right-part__button-group--form--descriere',
    fieldsButtonGroupMain: 'pagina_animal_admin__main-container__upper-section__right-part__button-group--form--fields',
    animalPageRecomanded: 'pagina_animal_admin__main-container__recomanded',
    animalPageRecomandedTitle: 'pagina_animal_admin__main-container__recomanded--title',
    animalPageRecomandedShop: 'pagina_animal_admin__main-container__recomanded--shop',
  };


  return (
    <div className={styles.paginaAnimal}>
      <img src={PaginaAnimalAdminBackground} className={styles.image} alt='Animale' />

      <div className={styles.mainContainer}>
        <div className={styles.upperSection}>
          <div className={styles.leftPartContainer}>
            <div className={styles.bigPhoto}>
              <img
                src={`https://res.cloudinary.com/dssizmzir/image/upload/${foundAnimal?.imagini[0]}`}
                key={animalPhoto[0]}
              />
            </div>
          </div>
          <div className={styles.rightPart}>
            <p className={styles.rightPartSecondP}>{foundAnimal?.titluAnunt}</p>
            <hr className={styles.rightPartLine} />
            <div className={styles.status}>
              <p>Status: {foundAnimal?.statusAnunt}</p>
              {foundAnimal?.statusAnunt === 'Anunt acceptat' && <FaCheckCircle color='green' />}
              {foundAnimal?.statusAnunt === 'Anunt refuzat' && <FaTimesCircle color='red' />}
              {foundAnimal?.statusAnunt === 'In asteptare' && <FaClock color='gray' />}
            </div>
            <div className={styles.buttonGroup}>
              <ButtonGroup
                type={CustomTypes.ButtonGroupTypes.HEADER}
                buttons={[
                  { label: 'Descriere', index: 1, icon: <MdDescription /> },
                  { label: 'Date de contact stăpân', index: 2, icon: <RiContactsFill /> },
                ]}
                backgroundColor={CustomTypes.PageColor.DARK}
                afterClick={(label) => {
                  setLabel(label);
                }
                }
              />
              <div className={styles.formButtonGroup}>
                {label === 'Date de contact stăpân' ? (
                  <>
                    <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${foundAnimal?.imagineProfilStapan}`} />
                    <div className={styles.fieldsButtonGroupMain}>
                      <h1>  <span className='icon'><IoMdContact /></span> &nbsp; Nume: <span className={styles.informationButtonGroup}> {found && found.numeStapan} </span> </h1>
                      <h1>  <span className='icon'><BsFillTelephoneOutboundFill /></span> &nbsp; Număr de telefon: <span className={styles.informationButtonGroup}> {found && found.numarTelefonStapan} </span></h1>
                      <h1>  <span className='icon'><IoMdMail /></span> &nbsp; Email: <span className={styles.informationButtonGroup}> {found && found.emailStapan} </span> </h1>
                    </div>
                  </>
                ) : (
                  <div className={styles.descriereButtonGroup}>
                    <h1> &nbsp;&nbsp;&nbsp;{found && found.descriere} </h1>
                  </div>)}
              </div>
            </div>
          </div>
        </div>
        <div className={styles.buttons}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            icon={<AiFillCheckCircle color={'green'} size={'3rem'} />}
            label={'Acceptă anunț'}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={AcceptaAnunt}
          />
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            icon={<AiFillCloseCircle color={'red'} size={'3rem'} />}
            label={'Refuză anunț'}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={RefuzaAnunt}
          />
        </div>
      </div>
    </div>
  );
};

export default PaginaAnimalAdmin;
