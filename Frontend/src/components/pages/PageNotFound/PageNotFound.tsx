import { Button } from '../../atoms';
import * as CustomTypes from '../../../utils/customTypes';
import './PageNotFound.scss';
import { Link } from 'react-router-dom';

const PageNotFound: React.FC = () => {
  const styles = {
    pageNotFound: 'page-not-found',
    title: 'page-not-found__title',
    oops: 'page-not-found__title__oops',
    whiteText: 'page-not-found__title__oops--white',
    brownText: 'page-not-found__title__oops--brown',
    number: 'page-not-found__title__number',
    content: 'page-not-found__content',
    text: 'page-not-found__content__text',
  };
  return (
    <div className={styles.pageNotFound}>
      <div className={styles.title}>
        <div className={styles.oops}>
          <h1 className={styles.whiteText}>Oops, looks like you missed the putt!</h1>
          <h1 className={styles.brownText}>This page doesn`t exist!</h1>
        </div>
        <h1 className={styles.number}>
          4<span>4</span>
        </h1>
      </div>
      <div className={styles.content}>
        <div className={styles.text}>
          We are currently in the process of updating our site, so some pages are still in work. We
          are sorry for the inconvenience.
        </div>
        <Link to='/' onClick={() => window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })}>
          <Button type={CustomTypes.ButtonTypes.PRIMARY} label='Take me home &#x21E8;' />
        </Link>
      </div>
    </div>
  );
};

export default PageNotFound;
