import './PaginaTrimiteSpreAdoptie.scss';
import { ReactNode } from 'react';
import * as MockData from '../../../mockData/mockDataComponents';
import { BreadcrumbsNav } from '../../atoms';
import { PaginaFormSpreAdoptie } from '../../../assets/imagePaths';

export interface PaginaTrimiteSpreAdoptieProps {
  children: ReactNode;
}

export default function PaginaTrimiteSpreAdoptie({ children }: PaginaTrimiteSpreAdoptieProps): JSX.Element {
  const styles = {
    paginaTrimiteSpreAdoptie: 'trimite-adoptie',
    background: 'trimite-adoptie__background',
    form: 'trimite-adoptie__background__form',
    breadcrumbs: 'trimite-adoptie__breadcrumbs',
  };
  return (
    <div className={styles.paginaTrimiteSpreAdoptie}>
      <div className={styles.breadcrumbs}>
        <BreadcrumbsNav {...MockData.PaginaTrimiteSpreAdoptieDate.breadcrumbs} />
      </div>
      <div className={styles.background}>
        <div className={styles.form}>
          {children}
        </div>
      </div>

    </div>

  );
};
