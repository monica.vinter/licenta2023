import Sidebar from '../../molecules/Sidebar/Sidebar';
import { InfoCard } from '../../atoms/InfoCard/InfoCard';
import './AdminDashboard.scss';

const AdminDashboard = () => {
  const styles = {
    admin: 'admin-dashboard',
    text: 'admin-dashboard__text',
    up: 'admin-dashboard__up',
  };
  return (
    <div className={styles.admin}>
      <div>
        <Sidebar />
      </div>
      <div>
        <div className={styles.text}>
          <h1>Bine ai venit,</h1>
          <p>Ce se întamplă pe aplicația ta?</p>
        </div>

        <div className={styles.up}>
          <InfoCard icon={'utilizatori'} title={'Utilizatori'} subTitle={4} />
          <InfoCard icon={'anunturi'} title={'Anunțuri'} subTitle={6} />
          <InfoCard icon={'formulare'} title={'formulare'} subTitle={2} />
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
