import './PaginaSfaturi.scss';
import { BreadcrumbsNav } from '../../atoms';
import * as MockData from '../../../mockData/mockDataComponents';
import { CategoriiAnimaleSfaturi } from '../../molecules/CategoriiAnimaleSfaturi/CategoriiAnimaleSfaturi';

const PaginaSfaturi = () => {
  const styles = {
    paginaSfaturi: 'pagina-sfaturi',
    breadCrumbs: 'pagina-sfaturi__breadCrumbs',
    BlackBackground: 'pagina-sfaturi__black-background',
  };
  return (
    <div className={styles.paginaSfaturi}>
      <div className={styles.breadCrumbs}>
        <BreadcrumbsNav {...MockData.PaginaSfaturiDate.breadcrumbs} />
      </div>
      <div className={styles.BlackBackground} />
      <CategoriiAnimaleSfaturi />
    </div>
  );
};

export default PaginaSfaturi;