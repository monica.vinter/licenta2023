import { useMemo, useState } from 'react';
import './PaginaPisici.scss';
import { BreadcrumbsNav, ButtonGroup } from '../../atoms/index';
import {
  SfaturiPisici,
  SfaturiActivitatePisici,
  SfaturiHranaPisici,
  SfaturiMediuPisici,
} from '../../../assets/imagePaths';
import { BsActivity } from 'react-icons/bs';
import { FaPaw, FaUtensils } from 'react-icons/fa';
import * as MockData from '../../../mockData/mockDataComponents';
import * as CustomTypes from '../../../utils/customTypes';

const PaginaPisici: React.FC = () => {

  const ButtonGroupAnimale = [
    {
      title: 'Activitatea fizică',
      imagine: SfaturiActivitatePisici,
      text: 'Pisicile sunt animale active care preferă activități intense pe perioade scurte pentru a-și menține corpul și mintea în formă și a interacționa cu lumea din jur. Pisicile de exterior iubesc să hoinărească ore întregi prin împrejurimi, vânând. Pisicile de interior au nevoie de interacțiune cu stăpânii lor și de jucării care să le țină ocupate pentru a rămâne sănătoase și fericite. Este important să planifici perioade de joacă și interacțiune pe tot parcursul zilei, pentru a asigura mișcarea adecvată și pentru a preveni plictiseala și excesul de greutate.'
    },
    {
      title: 'Mediul înconjurător',
      imagine: SfaturiMediuPisici,
      text: 'Pentru o pisică sănătoasă și fericită, mediul în care trăiește este crucial. Acesta trebuie să fie sigur și să permită pisicii să devină sociabilă, să se obișnuiască cu oamenii și cu obiectele din jurul ei. Chiar și de mică, pisica își va organiza teritoriul unde va dormi, se va juca, va vâna, se va hrăni, se va ascunde, se va cățăra și se va cuibări. În locuința sa, pisica își organizează viața în jurul a patru zone principale: zona de hrănire, zona de odihnă, zona de curățare și zona de joacă. Este esențial să respecți aceste zone pentru a preveni problemele de comportament, în special pentru pisicile de interior.'
    },
    {
      title: 'Hrănirea',
      imagine: SfaturiHranaPisici,
      text: 'Hrănirea unei pisici este un aspect crucial al îngrijirii sale. În general, pisicile au nevoie de hrană de înaltă calitate, bogată în proteine și nutrienți esențiali pentru a-și menține o sănătate bună. Există două opțiuni principale de hrană pentru pisici: hrana uscată și hrana umedă. Este important să acorzi atenție dimensiunii porțiilor și să eviți supra-alimentarea pisicii, deoarece acest lucru poate duce la probleme de greutate și de sănătate. În plus, asigură-te că pisica are întotdeauna acces la apă proaspătă și curată. Consultă un medic veterinar pentru recomandări privind dieta și cantitatea de hrană potrivite pentru pisica ta în funcție de vârsta, greutatea și nivelul de activitate.'
    }
  ];
  const [label, setLabel] = useState('Activitatea fizică');
  const found = useMemo(
    () =>
      ButtonGroupAnimale.find((element) => {
        return element.title === label;
      }),
    [label],
  );

  const styles = {
    paginaAnimal: 'pagina_pisici',
    breadCrumbs: 'pagina_pisici__breadCrumbs',
    mainContainer: 'pagina_pisici__main-container',
    header: 'pagina_pisici__main-container__header',
    HeaderTitle: 'pagina_pisici__main-container__header__title',
    HeaderLine: 'pagina_pisici__main-container__header__line',
    photo: 'pagina_pisici__main-container__photo',
    continut: 'pagina_pisici__main-container__continut',
    buttonGroup: 'pagina_pisici__main-container__continut__button-group',
    formButtonGroup: 'pagina_pisici__main-container__continut__button-group__form',
  };


  return (
    <div className={styles.paginaAnimal}>
      <div className={styles.breadCrumbs}>
        <BreadcrumbsNav {...MockData.PaginaSfaturiDatePisici.breadcrumbs} />
      </div>
      <div className={styles.mainContainer}>

        <div className={styles.header}>
          <hr className={styles.HeaderLine} />
          <h4 className={styles.HeaderTitle}>Pisici</h4>
          <hr className={styles.HeaderLine} />
          <h3>Dacă te-ai decis că vrei să adopți o pisică, trebuie să știi că va trebui să acorzi timp și energie pentru a înțelege cât mai bine nevoile noului tău prieten, pentru ca el să crească într-un mediu plăcut și să ducă o viață cât mai fericită. Nu-ți fă griji, iubirea pe care o vei primi în schimb, merită orice sacrificiu.</h3>
        </div>

        <div className={styles.photo}>
          <img src={SfaturiPisici} ></img>
        </div>

        <div className={styles.continut}>
          <div className={styles.buttonGroup}>
            <ButtonGroup
              type={CustomTypes.ButtonGroupTypes.HEADER}
              buttons={[
                { label: 'Activitatea fizică', index: 1, icon: <BsActivity /> },
                { label: 'Mediul înconjurător', index: 2, icon: <FaPaw /> },
                { label: 'Hrănirea', index: 3, icon: <FaUtensils /> },
              ]}
              backgroundColor={CustomTypes.PageColor.DARK}
              afterClick={(label) => {
                setLabel(label);
              }
              }
            />
            <div className={styles.formButtonGroup}>
              {label === 'Activitatea fizică' ? (
                <>
                  <img src={found?.imagine} ></img>
                  <p>{found?.text}</p>
                </>
              ) : label === 'Mediul înconjurător' ? (
                <>
                  <p>{found?.text}</p>
                  <img src={found?.imagine} ></img>
                </>
              ) : label === 'Hrănirea' ? (
                <>
                  <img src={found?.imagine} ></img>
                  <p>{found?.text}</p>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaginaPisici;
