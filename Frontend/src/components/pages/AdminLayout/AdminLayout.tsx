import { ReactNode } from 'react';
import Sidebar from '../../molecules/Sidebar/Sidebar';
import './AdminLayout.scss';
import { BackgroundAdmin } from '../../../assets/imagePaths';


const AdminLayout: React.FC = () => {
    const styles = {
        container: 'admin-layout__container',
    };
    return (
        <div className={styles.container}>
            <Sidebar />
        </div>

    );
};

export default AdminLayout;
