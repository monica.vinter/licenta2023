import './PaginaAltele.scss';
import { useState, useMemo, useEffect } from 'react';
import * as CustomTypes from '../../../utils/customTypes';
import { } from '../../atoms/index';
import { ButtonGroup } from '../../atoms/ButtonGroup/ButtonGroup';
import CategorieAltele from '../../../assets/CategorieAltele.png';

const PaginaAltele: React.FC = () => {

  const ButtonGroupAnimale = [
    {
      title: 'Descriere',
      text: 'hello'
    },
    {
      title: 'Date de contact stăpân',
      text: 'yellow'
    }
  ];
  const [label, setLabel] = useState('Description');

  const styles = {
    paginaAnimal: 'pagina_altele',
    imageAnimalPage: 'pagina_altele__image',
    mainContainer: 'pagina_altele__main-container',
    upperSection: 'pagina_altele__main-container__upper-section',
    leftPartContainer: 'pagina_altele__main-container__upper-section__left',
    bigPhoto: 'pagina_altele__main-container__upper-section__left__big-photo',
    leftPartSecondPhotos: 'pagina_altele__main-container__upper-section__left__left-part--photos',
    rightPart: 'pagina_altele__main-container__upper-section__right-part',
    rightPartSecondP: 'pagina_altele__main-container__upper-section__right-part__second',
    rightPartLine: 'pagina_altele__main-container__upper-section__right-part__line',
    buttonGroup: 'pagina_altele__main-container__upper-section__right-part__button-group',
  };


  return (
    <div className={styles.paginaAnimal}>
      <div className={styles.mainContainer}>
        <div className={styles.upperSection}>
          <div className={styles.leftPartContainer}>
            <div className={styles.bigPhoto}>
              <img src={CategorieAltele} ></img>
            </div>
          </div>
          <div className={styles.rightPart}>
            <p className={styles.rightPartSecondP}>Altele</p>
            <hr className={styles.rightPartLine} />

            <div className={styles.buttonGroup}>
              <ButtonGroup
                type={CustomTypes.ButtonGroupTypes.HEADER}
                buttons={[
                  { label: 'Descriere', index: 1 },
                  { label: 'Date de contact stăpân', index: 2 },
                ]}
                backgroundColor={CustomTypes.PageColor.DARK}
                afterClick={(label) => {
                  setLabel(label);
                }
                }
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaginaAltele;
