import './PaginaAdminAnunturiAcceptate.scss';
import { Dropdown, Input } from '../../atoms/index';
import { CardAnimalAdoptie } from '../../molecules/index';
import * as MockData from '../../../mockData/mockDataComponents';
import { useDispatch, useSelector } from 'react-redux';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { getAnimale } from '../../../redux/features/animalSlice';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Pagination from '../../molecules/Pagination/Pagination';
import { PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH } from '../../../Routes/routesPath';

import { useEffect, useState, useCallback, useMemo } from 'react';
import Sidebar from '../../molecules/Sidebar/Sidebar';

function scrollWin() {
  window.scrollTo(200, 0);
}
const PaginaAdminAnunturiAcceptate: React.FC = () => {
  const dispatch = useDispatch<AppThunkDispatch>();
  const { category } = useParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(3);

  useEffect(() => {
    dispatch(getAnimale());
  }, []);

  const { animale, filter } = useSelector((state: RootState) => state.animalState);

  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);
  const [searchInput, setSearchInput] = useState('');
  const [sortDropdown, setSortDropdown] = useState('');
  const filteredAnimale = useMemo(
    () =>
      animale
        .filter((animale) =>
          filter.judet ? filter.judet.includes(animale.judet) : true,
        )
        .filter((animal) =>
          filter.gen ? filter.gen.includes(animal.gen) : true,
        )
        .filter((animal) => {
          if (searchInput === '') return animal;
          if (animal.titluAnunt.toLowerCase().includes(searchInput.toLowerCase())) return animal;
        }),
    [
      animale,
      filter.judet,
      filter.gen,
      searchInput,
      sortDropdown,
    ],
  );

  useEffect(() => {
    setCurrentPage(1);
  }, [category]);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const SortedAnimaleAnunt = filteredAnimale.filter((animal) => animal.statusAnunt === 'Anunt acceptat');
  const currentPost = SortedAnimaleAnunt.slice(indexOfFirstPost, indexOfLastPost);

  const sortAnimale = useCallback(() => {
    const sortedAnimale = [...currentPost];
    if (sortDropdown === MockData.OptiuniSortareAnimale.ascAlphabetical) {
      return sortedAnimale.sort((a, b) => (a.titluAnunt < b.titluAnunt ? -1 : 1));
    }
    if (sortDropdown === MockData.OptiuniSortareAnimale.descAlphabetical) {
      return sortedAnimale.sort((a, b) => (a.titluAnunt < b.titluAnunt ? 1 : -1));
    }
    if (sortDropdown === MockData.OptiuniSortareAnimale.ascJudet) {
      return sortedAnimale.sort((a, b) => (a.judet < b.judet ? -1 : 1));
    }
    if (sortDropdown === MockData.OptiuniSortareAnimale.descJudet) {
      return sortedAnimale.sort((a, b) => (a.judet < b.judet ? 1 : -1));
    }
    return sortedAnimale;
  }, [currentPost]);

  const onChangeDropdown = useCallback((value: string) => {
    setSortDropdown(value);
  }, []);

  const styles = {
    paginaAnunturiAcceptate: 'pagina-anunturi-acceptate',
    container: 'pagina-anunturi-acceptate__container',
    body: 'pagina-anunturi-acceptate__body',
    leftColumn: 'pagina-anunturi-acceptate__body__left-column',
    rightColumn: 'pagina-anunturi-acceptate__body__right-column',
    header: 'pagina-anunturi-acceptate__body__right-column__header',
    search: 'pagina-anunturi-acceptate__body__right-column__header__search',
    sort: 'pagina-anunturi-acceptate__body__right-column__header__sort',
    animaleContainer: 'pagina-anunturi-acceptate__body__right-column__animale-container',
    animal: 'pagina-anunturi-acceptate__body__right-column__animale-container__animal',
  };
  return (

    <div className={styles.paginaAnunturiAcceptate}>
      <div className={styles.body}>
        <div className={styles.leftColumn}>
          <div className={styles.container}>
            <Sidebar />
          </div>
        </div>
        <div className={styles.rightColumn}>
          <div className={styles.header}>
            <div className={styles.sort}>
              <Dropdown
                name={MockData.CategoriiPagina.dropdownData.name}
                placeholder={MockData.CategoriiPagina.dropdownData.placeholder}
                options={MockData.CategoriiPagina.dropdownData.options}
                onChange={(value) => onChangeDropdown(value)}
                backgroundColor={MockData.CategoriiPagina.dropdownData.backgroundColor}
              />
            </div>
            <div className={styles.search}>
              <Input
                type={MockData.CategoriiPagina.inputData.inputType}
                styleType={MockData.CategoriiPagina.inputData.styleType}
                placeholder={MockData.CategoriiPagina.inputData.inputPlaceholder}
                rightIcon={MockData.CategoriiPagina.inputData.icon}
                onChange={(event) => setSearchInput((event.target as HTMLInputElement).value)}
              />
            </div>
          </div>
          <div className={styles.animaleContainer}>
            {sortAnimale().map((animal, index) => {
              return (
                <div className={styles.animal} key={index}>
                  <Link to={`/admin/pagina-animal/${animal._id}`} onClick={scrollWin}>
                    <CardAnimalAdoptie
                      id={animal._id}
                      titluAnunt={animal.titluAnunt}
                      judet={animal.judet}
                      imagine={animal.imagini[0]}
                      adoptat={!animal.adoptat}
                      categorie={animal.categorie}
                      gen={animal.gen}
                      onClick={(value) => {
                        console.log(value);
                      }}
                    />
                  </Link>
                </div>
              );
            })}
          </div>
          <Pagination
            path={PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH}
            categorie={category}
            postsPerPage={postsPerPage}
            totalPosts={SortedAnimaleAnunt.length}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            paginate={paginate}
          />
        </div>
      </div>
    </div>
  );
};

export default PaginaAdminAnunturiAcceptate;
