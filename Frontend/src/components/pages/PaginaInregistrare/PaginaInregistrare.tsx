
import { ReactNode } from 'react';
import './PaginaInregistrare.scss';
import { PaginaFormRegister } from '../../../assets/imagePaths';
import { BreadcrumbsNav } from '../../atoms';
import * as MockData from '../../../mockData/mockDataComponents';

export interface PaginaInregistrareProps {
  children: ReactNode;
}

export default function PaginaInregistrare({ children }: PaginaInregistrareProps): JSX.Element {
  const styles = {
    container: 'pagina-inregistrare',
    background: 'pagina-inregistrare__background',
    breadcrumbs: 'breadcrumbs',
    form: 'pagina-inregistrare__background__form',
  };
  return (
    <>
      <div className={styles.breadcrumbs}>
        <BreadcrumbsNav {...MockData.PaginaInregistrare.breadcrumbs} />
      </div>
      <div className={styles.container}>
        <div className={styles.background}>
          <div className={styles.form}>
            {children}
          </div>
        </div>
      </div>
    </>
  );
}



