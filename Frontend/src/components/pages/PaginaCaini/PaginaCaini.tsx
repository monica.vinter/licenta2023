import { useMemo, useState } from 'react';
import './PaginaCaini.scss';
import { BreadcrumbsNav, ButtonGroup } from '../../atoms/index';
import {
  SfaturiCaini,
  SfaturiActivitateCaini,
  SfaturiHranaCaini,
  SfaturiMediuCaini,
} from '../../../assets/imagePaths';
import { BsActivity } from 'react-icons/bs';
import { FaPaw, FaUtensils } from 'react-icons/fa';
import * as MockData from '../../../mockData/mockDataComponents';
import * as CustomTypes from '../../../utils/customTypes';

const PaginaCaini: React.FC = () => {

  const ButtonGroupAnimale = [
    {
      title: 'Activitatea fizică',
      imagine: SfaturiActivitateCaini,
      text: 'Orice câine are nevoie să aibă activitate fizică, însă ceea ce influențează volumul acesteia depinde de rasă, talie și vârstă. Foarte important de reținut, dacă ai un câine de talie înaltă sau medie, și este în creștere, este indicat să nu facă efort îndelungat, pentru că oasele și articulațiile acestuia se dezvoltă. Cel mai important însă, este să îți faci timp pentru joacă, astfel prietenul tău va crește sănatos și fericit.'
    },
    {
      title: 'Mediul înconjurător',
      imagine: SfaturiMediuCaini,
      text: 'Cățeii sunt sensibili la mediul lor și experiențele negative pot avea efecte de durată asupra comportamentului lor. Socializarea este importantă pentru dezvoltarea lor și începe de la patru săptămâni și se termină la 14 săptămâni. În această perioadă de socializare, cățelul trece prin perioada de frică, așa că este important să-i oferi un mediu sigur și stimulativ. Acesta trebuie să aibă propriul pat, jucării de cauciuc și să se obișnuiască treptat cu noul mediu. De asemenea, trebuie să fie obișnuit să rămână singur și să fie prezentat altor câini și animale pentru a facilita socializarea. Deplasarea în jurul orașului și plimbările sunt, de asemenea, importante pentru cățelul tău să exploreze împrejurimile.'
    },
    {
      title: 'Hrănirea',
      imagine: SfaturiHranaCaini,
      text: 'La fel ca și activitatea fizică, necesitățile nutriționale ale câinilor se schimbă odată cu creșterea lor. În timp ce cățeii vor avea nevoie inițial de mai multe mese mici pe zi, acestea vor fi reduse treptat la una sau două. Talia și rasa câinelui vor influența, de asemenea, nevoile sale nutriționale, de aceea este important să consulți medicul veterinar pentru a afla cantitatea corectă de hrană necesară câinelui tău. Hrana trebuie să furnizeze energie și să mențină sănătatea organismului, ajutând la prevenirea problemelor digestive, ale pielii, ale dinților, ale articulațiilor și a celor legate de vârstă. Nu schimba hrana brusc, ci înlocuiește-o treptat, timp de o săptămână. Alege hrana potrivită vârstei câinelui și stabilește o rutină de hranire, hrănindu-l în același loc, la aceeași oră și după ce tu și familia ta ați mâncat.'
    }
  ];
  const [label, setLabel] = useState('Activitatea fizică');
  const found = useMemo(
    () =>
      ButtonGroupAnimale.find((element) => {
        return element.title === label;
      }),
    [label],
  );

  const styles = {
    paginaAnimal: 'pagina_caini',
    breadCrumbs: 'pagina_caini__breadCrumbs',
    mainContainer: 'pagina_caini__main-container',
    header: 'pagina_caini__main-container__header',
    HeaderTitle: 'pagina_caini__main-container__header__title',
    HeaderLine: 'pagina_caini__main-container__header__line',
    photo: 'pagina_caini__main-container__photo',
    continut: 'pagina_caini__main-container__continut',
    buttonGroup: 'pagina_caini__main-container__continut__button-group',
    formButtonGroup: 'pagina_caini__main-container__continut__button-group__form',
  };


  return (
    <div className={styles.paginaAnimal}>
      <div className={styles.breadCrumbs}>
        <BreadcrumbsNav {...MockData.PaginaSfaturiDateCaini.breadcrumbs} />
      </div>
      <div className={styles.mainContainer}>

        <div className={styles.header}>
          <hr className={styles.HeaderLine} />
          <h4 className={styles.HeaderTitle}>Câini</h4>
          <hr className={styles.HeaderLine} />
          <h3>Dacă te-ai decis că vrei să adopți un câine, trebuie să știi că va trebui să acorzi timp și energie pentru a înțelege cât mai bine nevoile noului tău prieten, pentru ca el să crească într-un mediu plăcut și să ducă o viață cât mai fericită. Nu-ți fă griji, iubirea pe care o vei primi în schimb, merită orice sacrificiu.</h3>
        </div>

        <div className={styles.photo}>
          <img src={SfaturiCaini} ></img>
        </div>

        <div className={styles.continut}>
          <div className={styles.buttonGroup}>
            <ButtonGroup
              type={CustomTypes.ButtonGroupTypes.HEADER}
              buttons={[
                { label: 'Activitatea fizică', index: 1, icon: <BsActivity /> },
                { label: 'Mediul înconjurător', index: 2, icon: <FaPaw /> },
                { label: 'Hrănirea', index: 3, icon: <FaUtensils /> },
              ]}
              backgroundColor={CustomTypes.PageColor.DARK}
              afterClick={(label) => {
                setLabel(label);
              }
              }
            />
            <div className={styles.formButtonGroup}>
              {label === 'Activitatea fizică' ? (
                <>
                  <img src={found?.imagine} ></img>
                  <p>{found?.text}</p>
                </>
              ) : label === 'Mediul înconjurător' ? (
                <>
                  <p>{found?.text}</p>
                  <img src={found?.imagine} ></img>
                </>
              ) : label === 'Hrănirea' ? (
                <>
                  <img src={found?.imagine} ></img>
                  <p>{found?.text}</p>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaginaCaini;
