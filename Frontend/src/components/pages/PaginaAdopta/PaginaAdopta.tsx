import './PaginaAdopta.scss';
import { Dropdown, Input, BreadcrumbsNav } from '../../atoms/index';
import { FiltreAnimale, CardAnimalAdoptie, CategoriiAnimaleLista } from '../../molecules/index';
import * as MockData from '../../../mockData/mockDataComponents';
import { useDispatch, useSelector } from 'react-redux';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { getAnimale } from '../../../redux/features/animalSlice';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Pagination from '../../molecules/Pagination/Pagination';
import { ADOPTA_PATH } from '../../../Routes/routesPath';
import { PaginaAdoptaBackground } from '../../../assets/imagePaths';
import { useEffect, useState, useCallback, useMemo } from 'react';

function scrollWin() {
  window.scrollTo(200, 0);
}
const PaginaAdopta: React.FC = () => {
  const dispatch = useDispatch<AppThunkDispatch>();
  const navigate = useNavigate();
  const { category } = useParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(6);

  useEffect(() => {
    dispatch(getAnimale());
  }, []);

  
  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);

  const [searchInput, setSearchInput] = useState('');
  const [sortDropdown, setSortDropdown] = useState('');

  const { animale, filter } = useSelector((state: RootState) => state.animalState);

  const filteredAnimale = useMemo(
    () =>
      animale
        .filter((animale) =>
          filter.judet ? filter.judet.includes(animale.judet) : true,
        )
        .filter((animal) =>
          filter.gen ? filter.gen.includes(animal.gen) : true,
        )
        .filter((animal) => (filter.color ? filter.color.includes(animal.culoare) : true))
        .filter((animal) => {
          if (searchInput === '') return animal;
          if (animal.titluAnunt.toLowerCase().includes(searchInput.toLowerCase())) return animal;
        }),
    [
      animale,
      filter.judet,
      filter.gen,
      filter.color,
      searchInput,
      sortDropdown,
    ],
  );

  useEffect(() => {
    setCurrentPage(1);
  }, [category]);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const categorySortedAnimale = category
    ? filteredAnimale.filter((animal) => animal.categorie === category)
    : filteredAnimale;

  const currentPost = categorySortedAnimale.slice(indexOfFirstPost, indexOfLastPost);

  const sortAnimale = useCallback(() => {
    const sortedAnimale = [...currentPost];
    if (sortDropdown === MockData.OptiuniSortareAnimale.ascAlphabetical) {
      return sortedAnimale.sort((a, b) => (a.titluAnunt < b.titluAnunt ? -1 : 1));
    }
    if (sortDropdown === MockData.OptiuniSortareAnimale.descAlphabetical) {
      return sortedAnimale.sort((a, b) => (a.titluAnunt < b.titluAnunt ? 1 : -1));
    }
    if (sortDropdown === MockData.OptiuniSortareAnimale.ascJudet) {
      return sortedAnimale.sort((a, b) => (a.judet < b.judet ? -1 : 1));
    }
    if (sortDropdown === MockData.OptiuniSortareAnimale.descJudet) {
      return sortedAnimale.sort((a, b) => (a.judet < b.judet ? 1 : 1));
    }
    return sortedAnimale;
  }, [currentPost]);

  const onChangeDropdown = useCallback((value: string) => {
    setSortDropdown(value);
  }, []);

  const styles = {
    paginaAdopta: 'pagina-adopta',
    breadCrumbs: 'pagina-adopta__breadCrumbs',
    image: 'pagina-adopta__image',
    body: 'pagina-adopta__body',
    leftColumn: 'pagina-adopta__body__left-column',
    categories: 'pagina-adopta__body__left-column__categories',
    filterSection: 'pagina-adopta__body__left-column__filter-section',
    rightColumn: 'pagina-adopta__body__right-column',
    header: 'pagina-adopta__body__right-column__header',
    search: 'pagina-adopta__body__right-column__header__search',
    sort: 'pagina-adopta__body__right-column__header__sort',
    animaleContainer: 'pagina-adopta__body__right-column__animale-container',
    animal: 'pagina-adopta__body__right-column__animale-container__animal',
  };
  return (
    <div className={styles.paginaAdopta}>
      <div className={styles.breadCrumbs}>
        <BreadcrumbsNav {...MockData.PaginaAdoptaDate.breadcrumbs} />
      </div>
      <img src={PaginaAdoptaBackground} className={styles.image} alt='Animale' />
      <div className={styles.body}>
        <div className={styles.leftColumn}>
          <div className={styles.categories}>
            <CategoriiAnimaleLista
              categories={MockData.CategoriiPagina.categoriesTypes}
              onClick={(categorie) => navigate(ADOPTA_PATH + `/${categorie}`)}
            />
          </div>
          <div className={styles.filterSection}>
            <FiltreAnimale />
          </div>
        </div>
        <div className={styles.rightColumn}>
          <div className={styles.header}>
            <div className={styles.search}>
              <Input
                type={MockData.CategoriiPagina.inputData.inputType}
                styleType={MockData.CategoriiPagina.inputData.styleType}
                placeholder={MockData.CategoriiPagina.inputData.inputPlaceholder}
                rightIcon={MockData.CategoriiPagina.inputData.icon}
                onChange={(event) => setSearchInput((event.target as HTMLInputElement).value)}
              />
            </div>
            <div className={styles.sort}>
              <Dropdown
                name={MockData.CategoriiPagina.dropdownData.name}
                placeholder={MockData.CategoriiPagina.dropdownData.placeholder}
                options={MockData.CategoriiPagina.dropdownData.options}
                onChange={(value) => onChangeDropdown(value)}
                backgroundColor={MockData.CategoriiPagina.dropdownData.backgroundColor}
              />
            </div>
          </div>
          <div className={styles.animaleContainer}>
            {sortAnimale().map((animal, index) => {
              return (
                animal.statusAnunt === 'Anunt acceptat' && (
                  <div className={styles.animal} key={index}>
                    <Link to={`/pagina-animal/${animal._id}`} onClick={scrollWin}>
                      <CardAnimalAdoptie
                        id={animal._id}
                        titluAnunt={animal.titluAnunt}
                        judet={animal.judet}
                        imagine={animal.imagini[0]}
                        adoptat={!animal.adoptat}
                        categorie={animal.categorie}
                        gen={animal.gen}
                        onClick={(value) => {
                          console.log(value);
                        }}
                      />
                    </Link>
                  </div>)
              );
            })}
          </div>

          <Pagination
            path={ADOPTA_PATH}
            categorie={category}
            postsPerPage={postsPerPage}
            totalPosts={categorySortedAnimale.length}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            paginate={paginate}
          />
        </div>
      </div>
    </div>
  );
};

export default PaginaAdopta;
