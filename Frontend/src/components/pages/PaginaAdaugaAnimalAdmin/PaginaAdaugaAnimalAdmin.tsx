import './PaginaAdaugaAnimalAdmin.scss';
import { ReactNode } from 'react';
import Sidebar from '../../molecules/Sidebar/Sidebar';

export interface PaginaAdaugaAnimalAdminProps {
  children: ReactNode;
}

export default function PaginaAdaugaAnimalAdmin({ children }: PaginaAdaugaAnimalAdminProps): JSX.Element {
  const styles = {
    paginaTrimiteSpreAdoptie: 'adauga-admin',
    form: 'adauga-admin__form',
    sidebar: 'adauga-admin__sidebar',
  };
  return (
    <div className={styles.paginaTrimiteSpreAdoptie}>
      <div className={styles.sidebar}>
        <Sidebar />
      </div>
      <div className={styles.form}>
        {children}
      </div>
    </div>

  );
};
