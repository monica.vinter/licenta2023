import './PaginaForum.scss';
import { useState, ReactNode, useEffect, useMemo } from 'react';
import {
} from '../../molecules/index';
import { Input } from '../../atoms/Input/Input';
import { BreadcrumbsNav } from '../../atoms/index';
import * as MockData from '../../../mockData/mockDataComponents';
import { getTopicuri } from '../../../redux/features/topicSlice';
import { TopicCardProfile } from '../../molecules';
import { Link } from 'react-router-dom';
import moment, { isMoment } from 'moment';
import { useSelector, useDispatch } from 'react-redux';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { ImagineForum } from '../../../assets/imagePaths';

import {
  Paper,
  Stack,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

const tableContainerSx: SxProps = {
  border: '1px solid rgba(128,128,128,0.4)',
  width: 1400,
  marginLeft: 'auto',
  marginRight: 'auto',
  marginTop: 4,
  borderRadius: 2,
  maxHeight: 600,
  overflow: 'scroll',
  '&::-webkit-scrollbar': {
    width: '10px',
    height: '10px',
  },
  '&::-webkit-scrollbar-track': {
    backgroundColor: '#f1f1f1',
  },
  '&::-webkit-scrollbar-thumb': {
    backgroundColor: '#e8cfa0',
    borderRadius: '5px',
  },
  '&::-webkit-scrollbar-thumb:hover': {
    backgroundColor: '#c2ad88',
  },
};

export interface PaginaForumProps {
  children: ReactNode;
}

export default function PaginaForum({ children }: PaginaForumProps): JSX.Element {
  const styles = {
    paginaForum: 'pagina_forum',
    top: 'pagina_forum__top',
    right: 'pagina_forum__top__right',
    imagine: 'pagina_forum__top__right__imagine',
    search: 'pagina_forum__top__right__search',
    breadCrumbs: 'pagina_forum__breadCrumbs',
    adaugaTopic: 'pagina_forum__top__adauga-topic',
  };
  useEffect(() => {
    dispatch(getTopicuri());
  }, []);

  const dispatch = useDispatch<AppThunkDispatch>();

  const { topicuri } = useSelector((state: RootState) => state.topicuriState);

  const [searchInput, setSearchInput] = useState('');

  return (
    <>
      <div className={styles.paginaForum}>
        <div className={styles.breadCrumbs}>
          <BreadcrumbsNav {...MockData.PaginaForumDate.breadCrumbs} />
        </div>
        <div className={styles.top}>
          <div className={styles.adaugaTopic}>
            {children}
          </div>
          <div className={styles.right}>
            <img src={ImagineForum} className={styles.imagine} />
            <div className={styles.search}>
              <Input
                type={MockData.SearchForum.inputData.inputType}
                styleType={MockData.SearchForum.inputData.styleType}
                placeholder={MockData.SearchForum.inputData.inputPlaceholder}
                rightIcon={MockData.SearchForum.inputData.icon}
                onChange={(event) => setSearchInput((event.target as HTMLInputElement).value)}
              />
            </div>
          </div>
        </div>

        <TableContainer
          component={Paper}
          sx={tableContainerSx}
        >
          <Table stickyHeader={true}>
            <TableHead sx={{ '& .MuiTableCell-stickyHeader': { backgroundColor: 'rgb(190,151,114)' } }}>
              <TableRow>
                <TableCell sx={{ fontSize: 20, color: 'rgb(56,29,29)' }} scope='header'>Titlu topic</TableCell>
                <TableCell sx={{ fontSize: 20, color: 'rgb(56,29,29)' }} scope='header'>Autor</TableCell>
                <TableCell sx={{ fontSize: 20, color: 'rgb(56,29,29)' }} scope='header'>Numar comentarii</TableCell>
                <TableCell sx={{ fontSize: 20, color: 'rgb(56,29,29)' }} scope='header'>Data</TableCell>
              </TableRow>
            </TableHead>
            <TableBody
              sx={{
                '& tr:nth-of-type(2n+1)': {
                  backgroundColor: 'rgb(253,247,236)',
                },
              }}
            >
              {topicuri
                .slice(0)
                .reverse()
                .map((topic) => {
                  return (
                    (searchInput === '' || topic.titlu.toLowerCase().includes(searchInput.toLowerCase())) && (
                      <TableRow key={topic.titlu}>
                        <TableCell scope="row" sx={{ fontSize: 16, color: 'rgb(56,29,29)' }} >
                          <Stack direction="column">
                            <Link to={`/pagina-topic/${topic._id}`}>{topic.titlu}</Link>
                          </Stack>
                        </TableCell>

                        <TableCell scope="row" sx={{ fontSize: 16, color: 'rgb(56,29,29)' }} >
                          <Stack direction="column">
                            <div>{topic.user?.firstname} {topic.user?.lastname}</div>
                          </Stack>
                        </TableCell>

                        <TableCell scope="row" sx={{ fontSize: 16, color: 'rgb(56,29,29)' }} >
                          <Stack direction="column">
                            <div>{topic.numarComentarii.toString()}</div>
                          </Stack>
                        </TableCell>

                        <TableCell scope="row" sx={{ fontSize: 16, color: 'rgb(56,29,29)' }} >
                          <Stack direction="column">
                            <div>{moment(topic.dataPublicarii).format('DD MMM YYYY')}</div>
                          </Stack>
                        </TableCell>

                      </TableRow>
                    )
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </>
  );
};



