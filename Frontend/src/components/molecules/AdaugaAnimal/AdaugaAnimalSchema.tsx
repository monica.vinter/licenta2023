import { REGEX_NAME } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface AdaugaAnimalSchemaProps {
  titluAnunt: {
    required: string;
  };
  categorie: {
    required: string;
  };
  judet: {
    required: string;
    format: string;
  };
  rasa: {
    required: string;
  };
  varsta: {
    required: string;
  };
  gen: {
    required: string;
    format: string;
  };
  culoare: {
    required: string;
  };
  descriere: {
    required: string;
    format: string;
  };
}

const adaugaAnimalSchema = (validationMsgs: AdaugaAnimalSchemaProps) => {

  const { titluAnunt, categorie, judet, rasa, varsta, gen, culoare, descriere } = validationMsgs;

  return yup.object({
    titluAnunt: yup
      .string()
      .trim()
      .required(titluAnunt.required),
    categorie: yup
      .string()
      .trim()
      .required(categorie.required),
    judet: yup
      .string()
      .trim()
      .required(judet.required)
      .matches(REGEX_NAME, judet.format),
    rasa: yup
      .string()
      .trim()
      .required(rasa.required),
    varsta: yup
      .string()
      .trim()
      .required(varsta.required),
    gen: yup
      .string()
      .trim()
      .required(gen.required)
      .matches(REGEX_NAME, gen.format),
    culoare: yup
      .string()
      .trim()
      .required(culoare.required),
    descriere: yup
      .string()
      .trim()
      .required(descriere.required),

  })
};

export default adaugaAnimalSchema;