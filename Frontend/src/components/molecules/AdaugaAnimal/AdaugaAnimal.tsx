import { useState } from 'react';
import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import adaugaAnimalSchema from './AdaugaAnimalSchema';
import { DateAnimal, categoryOptionsData, colorOptionsData, judetOptionsData, genOptionsData } from './AdaugaAnimalUtils';
import './AdaugaAnimal.scss';
import { Dropdown } from '../../atoms';
import { useDispatch, useSelector } from 'react-redux';
import { ADAUGAREANUNT_SUCCESS, SERVER_ERROR } from '../../../utils/toastMessages';
import { setToast } from '../../../redux/features/toastSlice';
import { adaugaAnimale } from '../../../API/animaleAPI';
import { RootState } from '../../../redux/store';
import Axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { LOGIN_PATH } from '../../../Routes/routesPath';

export const AdaugaAnimal: React.FunctionComponent = () => {
  const dispatch = useDispatch();
  const [categoryOptions, setCategoryOptions] = useState(categoryOptionsData);
  const [colorOptions, setColorOptions] = useState(colorOptionsData);
  const [judetOptions, setJudetOptions] = useState(judetOptionsData);
  const [genOptions, setGenOptions] = useState(genOptionsData);
  const { isConnected, userData } = useSelector((state: RootState) => state.userState);
  const [img, setImg] = useState('');
  const navigate = useNavigate();

  const handleChange = async (files: any) => {
    const formData = new FormData();
    formData.append('upload_preset', 'dcnhkgdy');

    formData.append('file', files[0]);
    Axios.post('https://api.cloudinary.com/v1_1/dssizmzir/image/upload',
      formData
    ).then((response) => {
      setImg(response.data.public_id)
    });

    formik.setFieldValue('imagini', img);
  };

  const formik = useFormik({
    initialValues: {
      titluAnunt: '',
      categorie: '',
      judet: '',
      rasa: '',
      varsta: '',
      gen: '',
      culoare: '',
      imagini: [],
      descriere: '',
    },

    onSubmit: async function (values) {

      if (isConnected) { 
      console.log(values)
      const toastErrorData = { message: SERVER_ERROR, success: false };

      try {
        const DateAnimal = {
          titluAnunt: values.titluAnunt,
          categorie: values.categorie,
          judet: values.judet,
          rasa: values.rasa,
          varsta: values.varsta,
          gen: values.gen,
          culoare: values.culoare,
          imagini: img,
          descriere: values.descriere,
          emailStapan: userData?.email,
          stapan: userData,
          numarTelefonStapan: userData?.phone,
          numeStapan: userData?.firstname + ' ' + userData?.lastname,
          imagineProfilStapan: userData?.imagine,
        };

        const data = await adaugaAnimale(DateAnimal);
        if (data) {
          const toastData = { message: ADAUGAREANUNT_SUCCESS, success: true };
          dispatch(setToast(toastData));
        } else {
          dispatch(setToast(toastErrorData));
        }
      } catch (error) {
        dispatch(setToast(toastErrorData));
      }
    }
    else{
      navigate(LOGIN_PATH);
    }
    },

    validationSchema: adaugaAnimalSchema(DateAnimal),

  });

  const styles = {
    formContainer: 'adauga-animal-form__container',
    dropdownField: 'adauga-animal-form__dropdown-field',
    singleInputRow: 'adauga-animal-form__single-input-row',
    doubleInputRow: 'adauga-animal-form__double-input-row',
    label: 'adauga-animal-form__label',
    submitButton: 'adauga-animal-form__submit-button',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.adaugaAnuntHere} </h1>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.titluAnunt}
            name={FormInputNames.titluAnunt}
            label={FormInputLabels.titluAnunt}
            type={CustomTypes.InputTypes.TEXT}
            styleType={CustomTypes.InputStyleTypes.PRIMARY}
            placeholder={FormInputPlaceholders.titluAnunt}
            required
            error={
              formik.touched.titluAnunt && formik.errors.titluAnunt
                ? formik.errors.titluAnunt
                : undefined
            }
            value={formik.values.titluAnunt}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.doubleInputRow}>
          <div className={styles.dropdownField}>
            <Dropdown
              name={FormInputNames.categorie}
              label={FormInputLabels.categorie}
              placeholder={FormInputPlaceholders.categorie}
              error={formik.touched.categorie && formik.errors.categorie ? formik.errors.categorie : undefined}
              options={categoryOptions}
              onChange={(data) => formik.setFieldValue('categorie', data)}
              backgroundColor={CustomTypes.PageColor.DARK}
            />
          </div>
          <div className={styles.dropdownField}>
            <Dropdown
              name={FormInputNames.culoare}
              label={FormInputLabels.culoare}
              placeholder={FormInputPlaceholders.culoare}
              error={formik.touched.culoare && formik.errors.culoare ? formik.errors.culoare : undefined}
              options={colorOptions}
              onChange={(data) => formik.setFieldValue('culoare', data)}
              backgroundColor={CustomTypes.PageColor.DARK}
            />
          </div>
        </div>
        <div className={styles.doubleInputRow}>
          <div className={styles.dropdownField}>
            <Dropdown
              name={FormInputNames.judet}
              label={FormInputLabels.judet}
              placeholder={FormInputPlaceholders.judet}
              error={formik.touched.judet && formik.errors.judet ? formik.errors.judet : undefined}
              options={judetOptions}
              onChange={(data) => formik.setFieldValue('judet', data)}
              backgroundColor={CustomTypes.PageColor.DARK}
            />
          </div>
          <div className={styles.dropdownField}>
            <Dropdown
              name={FormInputNames.gen}
              label={FormInputLabels.gen}
              placeholder={FormInputPlaceholders.gen}
              error={formik.touched.gen && formik.errors.gen ? formik.errors.gen : undefined}
              options={genOptions}
              onChange={(data) => formik.setFieldValue('gen', data)}
              backgroundColor={CustomTypes.PageColor.DARK}
            />
          </div>
        </div>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.rasa}
            name={FormInputNames.rasa}
            label={FormInputLabels.rasa}
            type={CustomTypes.InputTypes.TEXT}
            styleType={CustomTypes.InputStyleTypes.PRIMARY}
            placeholder={FormInputPlaceholders.rasa}
            required
            error={
              formik.touched.rasa && formik.errors.rasa
                ? formik.errors.rasa
                : undefined
            }
            value={formik.values.rasa}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.varsta}
            name={FormInputNames.varsta}
            label={FormInputLabels.varsta}
            type={CustomTypes.InputTypes.TEXT}
            styleType={CustomTypes.InputStyleTypes.PRIMARY}
            placeholder={FormInputPlaceholders.varsta}
            required
            error={
              formik.touched.varsta && formik.errors.varsta
                ? formik.errors.varsta
                : undefined
            }
            value={formik.values.varsta}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.descriere}
            name={FormInputNames.descriere}
            label={FormInputLabels.descriere}
            type={CustomTypes.InputTypes.TEXT}
            styleType={CustomTypes.InputStyleTypes.PRIMARY}
            placeholder={FormInputPlaceholders.descriere}
            required
            error={
              formik.touched.descriere && formik.errors.descriere
                ? formik.errors.descriere
                : undefined
            }
            value={formik.values.descriere}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div>
          <p className={styles.label}>{FormInputLabels.imagini}</p>
          <input
            style={{ color: 'brown' }}
            type='file'
            name='imagine'
            accept='image/*'
            multiple
            onChange={(e: any) =>
              handleChange(e.currentTarget.files)
            }
          />
        </div>
        <div className={styles.submitButton}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.adaugaAnunt}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => formik.handleSubmit()}
          />
        </div>
      </form>
    </div>
  );
};