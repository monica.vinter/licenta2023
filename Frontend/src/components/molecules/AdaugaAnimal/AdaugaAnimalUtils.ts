import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const DateAnimal = {
    titluAnunt: {
        required: FormRequiredInputs.titluAnunt,
        format: FormFormatInputs.titluAnunt,
    },
    categorie: {
        required: FormRequiredInputs.categorie,
    },
    judet: {
        required: FormRequiredInputs.judet,
        format: FormFormatInputs.judet,
    },
    rasa: {
        required: FormRequiredInputs.rasa,
        format: FormFormatInputs.rasa,
    },
    varsta: {
        required: FormRequiredInputs.varsta,
    },
    gen: {
        required: FormRequiredInputs.gen,
        format: FormFormatInputs.gen,
    },
    culoare: {
        required: FormRequiredInputs.culoare,
    },
    descriere: {
        required: FormRequiredInputs.descriere,
        format: FormFormatInputs.descriere,
    },
};

export const categoryOptionsData = [
    { text: 'Câini', value: 'Câini' },
    { text: 'Pisici', value: 'Pisici' },
    { text: 'Altele', value: 'Altele' },
];

export const colorOptionsData = [
    { text: 'Negru', value: 'black' },
    { text: 'Gri', value: 'gray' },
    { text: 'Alb', value: 'white' },
    { text: 'Portocaliu', value: 'orange' },
    { text: 'Galben', value: 'yellow' },
    { text: 'Maro', value: 'brown' },
];

export const judetOptionsData = [
    { text: 'Alba', value: 'Alba' },
    { text: 'Arad', value: 'Arad' },
    { text: 'Argeș', value: 'Argeș' },
    { text: 'Bacău', value: 'Bacău' },
    { text: 'Bihor', value: 'Bihor' },
    { text: 'Bistrița-Năsăud', value: 'Bistrița-Năsăud' },
    { text: 'Botoșani', value: 'Botoșani' },
    { text: 'Brașov', value: 'Brașov' },
    { text: 'Brăila', value: 'Brăila' },
    { text: 'București', value: 'București' },
    { text: 'Buzău', value: 'Buzău' },
    { text: 'Caraș-Severin', value: 'Caraș-Severin' },
    { text: 'Călărași', value: 'Călărași' },
    { text: 'Cluj', value: 'Cluj' },
    { text: 'Constanța', value: 'Constanța' },
    { text: 'Covasna', value: 'Covasna' },
    { text: 'Dâmbovița', value: 'Dâmbovița' },
    { text: 'Dolj', value: 'Dolj' },
    { text: 'Gorj', value: 'Gorj' },
    { text: 'Harghita', value: 'Harghita' },
    { text: 'Hunedoara', value: 'Hunedoara' },
    { text: 'Ialomița', value: 'Ialomița' },
    { text: 'Iași', value: 'Iași' },
    { text: 'Ilfov', value: 'Ilfov' },
    { text: 'Maramureș', value: 'Maramureș' },
    { text: 'Mehedinți', value: 'Mehedinți' },
    { text: 'Mureș', value: 'Mureș' },
    { text: 'Neamț', value: 'Neamț' },
    { text: 'Olt', value: 'Olt' },
    { text: 'Prahova', value: 'Prahova' },
    { text: 'Satu Mare', value: 'Satu Mare' },
    { text: 'Sălaj', value: 'Sălaj' },
    { text: 'Sibiu', value: 'Sibiu' },
    { text: 'Suceava', value: 'Suceava' },
    { text: 'Teleorman', value: 'Teleorman' },
    { text: 'Timiș', value: 'Timiș' },
    { text: 'Tulcea', value: 'Tulcea' },
    { text: 'Vaslui', value: 'Vaslui' },
    { text: 'Vâlcea', value: 'Vâlcea' },
    { text: 'Vrancea', value: 'Vrancea' },
];

export const genOptionsData = [
    { text: 'Feminin', value: 'Feminin' },
    { text: 'Masculin', value: 'Masculin' },
];  
