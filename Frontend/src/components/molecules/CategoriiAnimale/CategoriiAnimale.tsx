import { CardAnimal } from '../CardAnimal/CardAnimal';
import * as MockData from '../../../mockData/mockDataComponents';
import './CategoriiAnimale.scss'
function scrollWin() {
  window.scrollTo(200, 0);
}
export const CategoriiAnimale = () => {

  const styles = {
    categoriiAnimaleContainer: 'categorii-animale__container',
    title: 'categorii-animale__title',
    card: 'categorii-animale__container-animale'
  };

  return (
    <div className={styles.categoriiAnimaleContainer}>
      <h2 className={styles.title}>Adoptă acum</h2>
      <div className={styles.card}>
        {MockData.CategoriiAnimale.map((category: any, index: number) => (
          <div key={index} onClick={scrollWin}>
            <CardAnimal
              id={category.id}
              name={category.name}
              image={category.image}
              buttonLabel={category.buttonLabel}
              onClick={category.onClick}
            />
          </div>
        ))}
      </div>
    </div>
  );
};