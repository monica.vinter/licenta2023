import './InformatiiAsociatie.scss';
import { CardDonatie } from '../CardDonatie/CardDonatie';
import { AsociatieContentCard } from '../AsociatieContentCard/AsociatieContentCard';
import clsx from 'clsx';
import * as CustomTypes from '../../../utils/customTypes';

export interface InformatiiAsociatieProps {
  type?: string;
  tag?: string;
  title?: string;
  text?: string;
  right?: boolean;
  border?: string;
  blackBox?: boolean;
  isForm?: boolean;
  titluDonatie?: string,
  banca?: string,
  ibanRON?: string,
  ibanEURO?: string,
  codSwift?: string,
  imagine?: string,
}

export const InformatiiAsociatie = ({
  type,
  right,
  blackBox,
  tag,
  title,
  text,
  isForm,
  titluDonatie,
  banca,
  ibanRON,
  ibanEURO,
  codSwift,
  imagine,
}: InformatiiAsociatieProps) => {
  const styles = {
    container: clsx(
      'photo-text-card',
      blackBox && 'photo-text-card--black-box',
      type === CustomTypes.ContentCardTypes.HEADER && 'photo-text-card--background',
    ),
  };

  return right ? (
    <div className={styles.container}>
      <CardDonatie
        titluDonatie={titluDonatie}
        banca={banca}
        ibanRON={ibanRON}
        ibanEURO={ibanEURO}
        codSwift={codSwift}
      />
      <AsociatieContentCard
        type={type}
        tag={tag}
        title={title}
        text={text}
        isForm={isForm}
        imagine={imagine}
      />
    </div>
  ) : (
    <div className={styles.container}>
      <AsociatieContentCard
        type={type}
        tag={tag}
        title={title}
        text={text}
        isForm={isForm}
        imagine={imagine}
      />
      <CardDonatie
        titluDonatie={titluDonatie}
        banca={banca}
        ibanRON={ibanRON}
        ibanEURO={ibanEURO}
        codSwift={codSwift}
      />
    </div>
  );
};
