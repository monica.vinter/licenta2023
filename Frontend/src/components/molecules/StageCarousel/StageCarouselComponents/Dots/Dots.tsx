import './Dots.scss';
export interface DotsProps{
    activeIndex: number,
    onClick:(value:number) => void,
    sliderImage: any,
}

const Dots: React.FC<DotsProps> = ({ activeIndex, onClick, sliderImage }) => {
  const styles = {
    dots:'dots__container',
    dotActive:'dots__component-active',
    dotInactive:'dots__component-inactive',
    dotLabel:'dots__label',
    dotText:'dots__text'
  }
  return (
    <div className={styles.dots}>
      {sliderImage.map((slide: any, index: number) => (
        <span
          key={index}
          className={`${activeIndex === index ? styles.dotActive: styles.dotInactive}`}
          onClick={() => onClick(index)}
        >
         {activeIndex === index && 
         <div className={styles.dotLabel}>
          <p className={styles.dotText}>{slide.title}</p>
         </div> 
        }
        </span>
      ))}
    </div>
  );
}

export default Dots;