import './CarouselContent.scss';
export interface CarouselProps {
  activeIndex: number,
  sliderImage: {
    title: string,
    urls: string,
  }[]
}

const CarouselContent: React.FC<CarouselProps> = ({ activeIndex, sliderImage }) => {
  const styles = {
    slide: 'slide',
    slideActive: 'slide--active',
    slideImage: 'slide__image'
  }
  return (
    <section>
      {sliderImage.map((slide: any, index: number) => (
        <div
          key={index}
          className={index === activeIndex ? styles.slideActive : styles.slide}
        >
          <img className={styles.slideImage} src={slide.urls} alt={slide.title} />
        </div>
      ))}
    </section>
  );
}

export default CarouselContent