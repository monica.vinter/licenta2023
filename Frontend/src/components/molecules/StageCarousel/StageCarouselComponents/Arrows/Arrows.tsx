import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io'
import './Arrows.scss';

export interface ArrowsProps {
  prevSlide: () => void,
  nextSlide: () => void,
}

const Arrows: React.FC<ArrowsProps> = ({ prevSlide, nextSlide }) => {
  const styles = {
    arrows: 'arrows',
    arrowPrev: 'arrows__prev',
    arrowNext: 'arrows__next',
  }

  return (
    <div className={styles.arrows}>
      <span className={styles.arrowPrev} onClick={prevSlide}>
        <IoIosArrowBack />
      </span>
      <span className={styles.arrowNext} onClick={nextSlide}>
        <IoIosArrowForward />
      </span>
    </div>
  );
}

export default Arrows