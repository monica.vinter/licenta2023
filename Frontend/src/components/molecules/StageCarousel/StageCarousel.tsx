import React, { useEffect, useState } from 'react';
import CarouselContent from './StageCarouselComponents/CarouselContent/CarouselContent';
import Dots from './StageCarouselComponents/Dots/Dots'
import Arrows from './StageCarouselComponents/Arrows/Arrows'
import {SliderCarouselImages} from '../../../mockData/mockDataCarousels'
import './StageCarousel.scss'
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import clsx from 'clsx';

export interface CarouselProps {
  title: string;
  buttonLabel: string;
  textAreaTitle: string;
  textAreaContent: string;
  textAreaLink?: string;
  disableTime?: boolean;
  sliderImage: {
    title: string,
    urls: string,
    }[] 
}

const len = SliderCarouselImages.length - 1;

export const Carousel: React.FC<CarouselProps> = ({title, buttonLabel, textAreaTitle, textAreaContent, textAreaLink, disableTime, sliderImage}) => {
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveIndex(activeIndex === len ? 0 : activeIndex + 1);
    }, 5000);
    return () => clearInterval(interval);
  }, [activeIndex]);

  const styles = {
    carouselContainer: 'stage-carousel__container',
    carouselTitle: 'stage-carousel__title',
    carouselText: clsx('stage-carousel__text', disableTime && 'stage-carousel__text--disabled'),
    carouselTime: clsx('stage-carousel__time', disableTime && 'stage-carousel__time--disabled'),
    carouselWrapper: 'stage-carousel__time-wrapper',
    carouselContainerText: 'stage-carousel__container-text',
    carouselButton: 'stage-carousel__button',
  };
  return (
    <div className={styles.carouselContainer}>
      <CarouselContent activeIndex={activeIndex} sliderImage={sliderImage} />
      <Arrows
        prevSlide={() => setActiveIndex(activeIndex < 1 ? len : activeIndex - 1)}
        nextSlide={() => setActiveIndex(activeIndex === len ? 0 : activeIndex + 1)}
      />
      <Dots
        activeIndex={activeIndex}
        sliderImage={sliderImage}
        onClick={(activeIndex) => setActiveIndex(activeIndex)}
      />
    </div>
  );
};
