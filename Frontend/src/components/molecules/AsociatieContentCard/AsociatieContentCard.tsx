import './AsociatieContentCard.scss';
import * as CustomTypes from '../../../utils/customTypes';
import { Link } from 'react-router-dom';

export interface AsociatieContentCardProps {
  type?: string;
  tag?: string;
  title?: string;
  text?: string;
  label?: string;
  isForm?: boolean;
  imagine?: string;
}

export const AsociatieContentCard = ({
  type,
  tag,
  title,
  text,
  imagine,
}: AsociatieContentCardProps) => {
  const styles = {
    content: 'content',
    tag: 'content__tag',
    title: 'content__title',
    headerTag: 'content__header__tag',
    text: 'content__text',
  };

  return (
    <div className={styles.content}>
      {type === CustomTypes.ContentCardTypes.HEADER ? (
        <>
          <p className={styles.headerTag}>{tag}</p>
          <p className={styles.text}>{text}</p>
        </>
      ) : (
        <>
          <p className={styles.tag}>{tag}</p>
          <p className={styles.title}>{title}</p>
          <p className={styles.text}>{text}</p>
        </>
      )}
      <img src={imagine}></img>
    </div>


  );
};
