
import { Button } from '../../atoms';
import './FormularePrimite.scss';
import * as CustomTypes from '../../../utils/customTypes';
import { useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { updateStatusFormularAcceptat, updateStatusFormularRefuzat } from '../../../API/formularAPI';
import { updateAdoptatAnimal } from '../../../API/animaleAPI';
import { FORMULAR_ACCEPTAT_SUCCES, FORMULAR_REFUZAT_SUCCES } from '../../../utils/toastMessages';
import { AiFillCheckCircle, AiFillCloseCircle } from 'react-icons/ai';
import { FaCheckCircle, FaTimesCircle, FaClock } from 'react-icons/fa';

interface FormularePrimiteProps {
  descriereConditii: string;
  numeViitorStapan?: string,
  emailViitorStapan?: string,
  nrTelViitorStapan?: string,
  statusFormular?: string,
  idAnimal?: string,
  imagineAnimal?: string,
  titluAnuntAnimal?: string,
  id?: string;
}

export const FormularePrimite = ({
  descriereConditii,
  numeViitorStapan,
  emailViitorStapan,
  nrTelViitorStapan,
  statusFormular,
  idAnimal,
  imagineAnimal,
  titluAnuntAnimal,
  id,
}: FormularePrimiteProps) => {
  const dispatch = useDispatch();

  const AcceptaFormular = () => {
    updateAdoptatAnimal(`/animale/${idAnimal}`);
    updateStatusFormularAcceptat(`/formulare/${id}`);
    const toastData = { message: FORMULAR_ACCEPTAT_SUCCES, success: true };
    dispatch(setToast(toastData));
  };
  const RefuzaFormular = () => {
    updateStatusFormularRefuzat(`/formulare/${id}`);
    const toastData = { message: FORMULAR_REFUZAT_SUCCES, success: true };
    dispatch(setToast(toastData));
  };
  const styles = {
    formularProfil: 'formulare-primite',
    buttons: 'formulare-primite__button',
    container: 'formulare-primite__container',
    descriere: 'formulare-primite__container__descriere',
    descrierelabel: 'formulare-primite__container__descriere__label',
    titlu: 'formulare-primite__container__titlu',
    formularPrimit: 'formulare-primite__container__formular-primit',
    informatii: 'formulare-primite__container__formular-primit__informatii',
    label: 'formulare-primite__container__formular-primit__informatii__label',
    status: 'formulare-primite__container__status',
  };
  return (
    <div className={styles.formularProfil}>
      <div className={styles.container}>
        <div className={styles.titlu}>
          <p>Titlu anunt: {titluAnuntAnimal} </p>
        </div>
        <div className={styles.formularPrimit}>
          <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${imagineAnimal}`} />
          <div className={styles.informatii}>
            <p><span className={styles.label}>Nume viitor stăpân:</span> {numeViitorStapan}</p>
            <p><span className={styles.label}>Email viitor stăpan:</span> {emailViitorStapan}</p>
            <p><span className={styles.label}>Număr telefon viitor stăpân:</span> {nrTelViitorStapan}</p>
          </div>
        </div>
        <div className={styles.descriere}>
          <p><span className={styles.label}>Descriere conditii:</span> {descriereConditii}</p>
        </div>
        <div className={styles.status}>
          <p>Status: {statusFormular}</p>
          {statusFormular === 'Acceptat' && <FaCheckCircle color='green' />}
          {statusFormular === 'Refuzat' && <FaTimesCircle color='red' />}
          {statusFormular === 'In asteptare' && <FaClock color='gray' />}
        </div>
      </div>
      <div className={styles.buttons}>
        <Button
          type={CustomTypes.ButtonTypes.PRIMARY}
          icon={<AiFillCloseCircle color='rgb(238, 94, 82)' size={'3rem'} />}
          label={'Refuza'}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          onClick={RefuzaFormular}
        />
        <Button
          type={CustomTypes.ButtonTypes.PRIMARY}
          icon={<AiFillCheckCircle color='green' size={'3rem'} />}
          label={'Accepta'}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          onClick={AcceptaFormular}
        />
      </div>
    </div>

  );
};
