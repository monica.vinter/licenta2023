import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const PersonalData = {
    firstName: {
        required: FormRequiredInputs.firstName,
        format: FormFormatInputs.firstName,
    },
    lastName: {
        required: FormRequiredInputs.lastName,
        format: FormFormatInputs.lastName,
    },
    emailAddress: {
        required: FormRequiredInputs.emailAddress,
        format: FormFormatInputs.emailAddress,
    },
    password: {
        required: FormRequiredInputs.password,
        format: FormFormatInputs.password,
    },
    confirmPassword: {
        required: FormRequiredInputs.confirmPassword,
    },
    phoneNumber: {
        required: FormRequiredInputs.phoneNumber,
        format: FormFormatInputs.phoneNumber,
    },
    birthDate: {
        required: FormRequiredInputs.birthDate,
    },
};