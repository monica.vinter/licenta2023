import { REGEX_NAME, REGEX_EMAIL, REGEX_PASSWORD, REGEX_PHONE } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface PersonalInformationSchemaProps {
  firstName: {
    required: string;
    format: string;
  };
  lastName: {
    required: string;
    format: string;
  };
  emailAddress: {
    required: string;
    format: string;
  };
  password: {
    required: string;
    format: string;
  };
  confirmPassword: {
    required: string;
  };
  phoneNumber: {
    required: string;
    format: string;
  };
  birthDate: {
    required: string;
  };
}

const personalInformationSchema = (validationMsgs: PersonalInformationSchemaProps) => {

  const { firstName, lastName, emailAddress, password, confirmPassword, phoneNumber, birthDate, } = validationMsgs;

  return yup.object({
    firstName: yup
      .string()
      .trim()
      .required(firstName.required)
      .matches(REGEX_NAME, firstName.format),
    lastName: yup
      .string()
      .trim()
      .required(lastName.required)
      .matches(REGEX_NAME, lastName.format),
    emailAddress: yup
      .string()
      .trim()
      .required(emailAddress.required)
      .matches(REGEX_EMAIL, emailAddress.format),
    password: yup
      .string()
      .trim()
      .required(password.required)
      .matches(REGEX_PASSWORD, password.format),
    confirmPassword: yup
      .string()
      .trim()
      .required(confirmPassword.required)
      .test(
        'Password-match!',
        'Password must match!',
        function (value) {
          return this.parent.password === value
        }),
    phoneNumber: yup
      .string()
      .required(phoneNumber.required)
      .matches(REGEX_PHONE, phoneNumber.format),
    birthDate: yup
      .string()
      .trim()
      .required(birthDate.required)
      .nullable(),
  })
};

export default personalInformationSchema;