import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import DatePicker from '../../atoms/DatePicker/DatePicker';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import personalInformationSchema from './RegisterSchema';
import { PersonalData } from './RegisterUtils';
import format from 'date-fns/format';
import './RegisterForm.scss';
import { AiFillPhone, AiOutlineCalendar, AiOutlineMail, AiOutlineUser } from 'react-icons/ai';
import { registerUser } from '../../../redux/features/userSlice';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, AppThunkDispatch, RootState } from '../../../redux/store';
import { useNavigate } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { setToast } from '../../../redux/features/toastSlice';
import { ACASA_PATH, LOGIN_PATH } from '../../../Routes/routesPath';
import { INVALID_DATA, REGISTER_SUCCESS_MESSAGE, SERVER_ERROR } from '../../../utils/toastMessages';
import { useRestrict } from '../../../utils/customHooks';

export const RegisterForm: React.FunctionComponent = () => {
  const dispatch = useDispatch<AppDispatch>();
  const dispatchThunk = useDispatch<AppThunkDispatch>();
  const { isConnected } = useSelector((state: RootState) => state.userState);
  const navigate = useNavigate();

  const navigateLogin = () => {
    navigate(LOGIN_PATH);
  };
  useRestrict();
  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      emailAddress: '',
      password: '',
      confirmPassword: '',
      phoneNumber: '',
      birthDate: '',
    },

    onSubmit: function (values) {
      const toastData = { message: SERVER_ERROR, success: false };

      try {
        const userData = {
          firstname: values.firstName,
          lastname: values.lastName,
          email: values.emailAddress,
          password: values.password,
          phone: values.phoneNumber,
          birthday: values.birthDate,
        };

        dispatchThunk(registerUser(userData));
        if (!isConnected) {
          navigate(ACASA_PATH);
          const toastData = { message: REGISTER_SUCCESS_MESSAGE, success: true };
          dispatch(setToast(toastData));
        } else {
          const toastData = { message: INVALID_DATA, success: false };
          dispatch(setToast(toastData));
        }
      } catch (error) {
        dispatch(setToast(toastData));
        console.log(error);
      }
    },

    validationSchema: personalInformationSchema(PersonalData),
  });

  const styles = {
    formContainer: 'register-form__container',
    singleInputRow: 'register-form__single-input-row',
    doubleInputRow: 'register-form__double-input-row',
    loginPortal: 'register-form__login-portal',
    textRequired: 'register-form__text-required',
    submitButton: 'register-form__submit-button',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.registerHere} </h1>
        <div className={styles.doubleInputRow}>
          <Input
            id={FormInputNames.firstName}
            name={FormInputNames.firstName}
            label={FormInputLabels.firstName}
            type={CustomTypes.InputTypes.TEXT}
            placeholder={FormInputPlaceholders.firstName}
            rightIcon={<AiOutlineUser />}
            required
            error={
              formik.touched.firstName && formik.errors.firstName
                ? formik.errors.firstName
                : undefined
            }
            value={formik.values.firstName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <Input
            id={FormInputNames.lastName}
            name={FormInputNames.lastName}
            label={FormInputLabels.lastName}
            type={CustomTypes.InputTypes.TEXT}
            placeholder={FormInputPlaceholders.lastName}
            rightIcon={<AiOutlineUser />}
            required
            error={
              formik.touched.lastName && formik.errors.lastName ? formik.errors.lastName : undefined
            }
            value={formik.values.lastName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.emailAddress}
            name={FormInputNames.emailAddress}
            label={FormInputLabels.emailAddress}
            type={CustomTypes.InputTypes.EMAIL}
            placeholder={FormInputPlaceholders.emailAddress}
            rightIcon={<AiOutlineMail />}
            required
            error={
              formik.touched.emailAddress && formik.errors.emailAddress
                ? formik.errors.emailAddress
                : undefined
            }
            value={formik.values.emailAddress}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.doubleInputRow}>
          <Input
            id={FormInputNames.password}
            name={FormInputNames.password}
            label={FormInputLabels.password}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.password}
            required
            formatInfo={FormInformation.passwordInfo}
            error={
              formik.touched.password && formik.errors.password ? formik.errors.password : undefined
            }
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <Input
            id={FormInputNames.confirmPassword}
            name={FormInputNames.confirmPassword}
            label={FormInputLabels.confirmPassword}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.confirmPassword}
            required
            error={
              formik.touched.confirmPassword && formik.errors.confirmPassword
                ? formik.errors.confirmPassword
                : undefined
            }
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.phoneNumber}
            name={FormInputNames.phoneNumber}
            label={FormInputLabels.phoneNumber}
            type={CustomTypes.InputTypes.TEXT}
            placeholder={FormInputPlaceholders.phoneNumber}
            rightIcon={<AiFillPhone />}
            required
            formatInfo={FormInformation.phoneNumberInfo}
            error={
              formik.touched.phoneNumber && formik.errors.phoneNumber
                ? formik.errors.phoneNumber
                : undefined
            }
            value={formik.values.phoneNumber}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.doubleInputRow}>
          <DatePicker
            id={FormInputNames.birthDate}
            name={FormInputNames.birthDate}
            label={FormInputLabels.birthDate}
            required
            icon={<AiOutlineCalendar fontSize={'2rem'} />}
            error={
              formik.touched.birthDate && formik.errors.birthDate
                ? formik.errors.birthDate
                : undefined
            }
            // value={formik.values.birthDate}
            onChange={(date) => formik.setFieldValue('birthDate', format(date, 'MM/dd/yyyy'))}
          />
          <div className={styles.submitButton}>
            <Button
              type={CustomTypes.ButtonTypes.PRIMARY}
              label={FormButtonsLabel.submit}
              backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
              onClick={() => formik.handleSubmit()}
            />
          </div>
        </div>
        <p className={styles.textRequired}> {FormInformation.requiredField} </p>
        <div className={styles.loginPortal}>
          <h4> {FormInformation.alreadyAccount} </h4>
          <Button
            type={CustomTypes.ButtonTypes.TERTIARY}
            label={FormButtonsLabel.loginHere}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={navigateLogin}
          />
        </div>
      </form>
    </div>
  );
};
