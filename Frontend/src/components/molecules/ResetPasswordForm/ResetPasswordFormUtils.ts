import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const ResetPasswordData = {
    verificationCode: {
        required: FormRequiredInputs.verificationCode,
        format: FormFormatInputs.verificationCode,
    },
    password: {
        required: FormRequiredInputs.password,
        format: FormFormatInputs.password,
    },
    confirmPassword: {
        required: FormRequiredInputs.confirmPassword,
    },
};