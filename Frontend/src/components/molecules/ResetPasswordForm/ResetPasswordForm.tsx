import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import resetPasswordSchema from './ResetPasswordSchema';
import { ResetPasswordData } from './ResetPasswordFormUtils';
import './ResetPasswordForm.scss';
import { useNavigate } from 'react-router-dom';

import { deleteCookie, getCookie } from '../../../utils/utilsFunctions';
import { forgotPassword } from '../../../API/userAPI';
import { EMAIL } from '../../../utils/Constants';
import { useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { LOGIN_PATH } from '../../../Routes/routesPath';
import { RESET_PASS_SUCCESS_MESSAGE, SERVER_ERROR } from '../../../utils/toastMessages';
import { useRestrict } from '../../../utils/customHooks';

export const ResetPasswordForm: React.FunctionComponent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const navigateLogin = () => {
    navigate(LOGIN_PATH);
  };
  useRestrict();

  const formik = useFormik({
    initialValues: {
      verificationCode: '',
      password: '',
      confirmPassword: '',
    },

    onSubmit: async function (values) {
      const toastData = { message: SERVER_ERROR, success: false };

      try {
        const userData = {
          email: getCookie(EMAIL),
          key: values.verificationCode,
          password: values.password,
        };
        const data = await forgotPassword(userData);
        if (data) {
          deleteCookie(EMAIL);
          navigateLogin();
          const toastData = { message: RESET_PASS_SUCCESS_MESSAGE, success: true };
          dispatch(setToast(toastData));
        } else {
          dispatch(setToast(toastData));
        }
      } catch (error) {
        dispatch(setToast(toastData));
        console.log(error);
      }
    },

    validationSchema: resetPasswordSchema(ResetPasswordData),
  });

  const styles = {
    formContainer: 'reset-password-form__container',
    singleInputRow: 'reset-password-form__single-input-row',
    doubleInputRow: 'reset-password-form__double-input-row',
    textRequired: 'reset-password-form__text-required',
    resetButton: 'reset-password-form__reset-button',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.resetPassword} </h1>
        <h4> {FormInformation.resetPasswordInfo} </h4>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.verificationCode}
            name={FormInputNames.verificationCode}
            label={FormInputLabels.verificationCode}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.verificationCode}
            required
            error={
              formik.touched.verificationCode && formik.errors.verificationCode
                ? formik.errors.verificationCode
                : undefined
            }
            value={formik.values.verificationCode}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.doubleInputRow}>
          <Input
            id={FormInputNames.password}
            name={FormInputNames.password}
            label={FormInputLabels.resetPassword}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.password}
            required
            formatInfo={FormInformation.passwordInfo}
            error={
              formik.touched.password && formik.errors.password ? formik.errors.password : undefined
            }
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <Input
            id={FormInputNames.confirmPassword}
            name={FormInputNames.confirmPassword}
            label={FormInputLabels.resetConfirmPassword}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.confirmPassword}
            required
            error={
              formik.touched.confirmPassword && formik.errors.confirmPassword
                ? formik.errors.confirmPassword
                : undefined
            }
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <p className={styles.textRequired}> {FormInformation.requiredField} </p>
        <div className={styles.resetButton}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.resetPassword}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => formik.handleSubmit()}
          />
        </div>
      </form>
    </div>
  );
};
