import { REGEX_PASSWORD, REGEX_VERIFICATION_CODE } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface ResetPasswordSchemaProps {
  verificationCode: {
    required: string;
    format: string;
  };
  password: {
    required: string;
    format: string;
  };
  confirmPassword: {
    required: string;
  };
}

const resetPasswordSchema = (validationMsgs: ResetPasswordSchemaProps) => {

  const { verificationCode, password, confirmPassword } = validationMsgs;

  return yup.object({
    verificationCode: yup
      .string()
      .trim()
      .required(verificationCode.required)
      .matches(REGEX_VERIFICATION_CODE, verificationCode.format),
    password: yup
      .string()
      .trim()
      .required(password.required)
      .matches(REGEX_PASSWORD, password.format),
    confirmPassword: yup
      .string()
      .trim()
      .required(confirmPassword.required)
      .test(
        'Password-match!',
        'Password must match!',
        function (value) {
          return this.parent.password === value
        }),
  })
};

export default resetPasswordSchema;