import './AnimaleAdoptateUser.scss';

interface AnimaleAdoptateUserProps {
  descriereConditii: string;
  emailStapanAnimal?: string,
  statusFormular?: string,
  imagineAnimal?: string,
  id?: string;
}

export const AnimaleAdoptateUser = ({
  descriereConditii,
  emailStapanAnimal,
  statusFormular,
  imagineAnimal,
  id,
}: AnimaleAdoptateUserProps) => {
  const styles = {
    formularProfil: 'animale-adoptate',
    titlu: 'animale-adoptate__titlu',
    informatii: 'animale-adoptate__informatii',
    status: 'animale-adoptate__status',
  };
  return (
    <div className={styles.formularProfil}>

      <div className={styles.titlu}>
        <p>Formular: nume animal </p>
        <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${imagineAnimal}`} />
      </div>
      <div className={styles.informatii}>
        <p>Descriere conditii: {descriereConditii}</p>
        <p>Email stapan:{emailStapanAnimal}</p>
      </div>
      <div className={styles.status}>
        <p>Status:{statusFormular}</p>
      </div>
    </div>
  );
};
