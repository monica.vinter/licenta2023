import './CardAnimalAdoptie.scss'
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import clsx from 'clsx';
import { AiOutlineEye } from 'react-icons/ai';
import { BiDetail } from 'react-icons/bi';

import { STATUS_ADOPTAT, DETALII } from '../../../utils/Constants';
import { ImaginiAnimale } from '../../../assets/imagePaths';

export interface CardAnimalAdoptieProps {
  id: string;
  titluAnunt?: string;
  judet?: string;
  adoptat?: boolean;
  categorie?: string;
  imagine: string;
  gen?: string;

  onClick: (id: string) => void;
}

export const CardAnimalAdoptie = ({
  id,
  titluAnunt,
  judet,
  adoptat,
  categorie,
  imagine,
  gen,

  onClick,
}: CardAnimalAdoptieProps) => {
  const styles = {
    cardAnimalAdoptie: clsx('card-animal-adoptie', adoptat && 'card-animal-adoptie--unavailable'),
    container: 'card-animal-adoptie__container--' + gen,
    imagine: 'card-animal-adoptie__container--' + gen + '__image',
    overlay: 'card-animal-adoptie__container--' + gen + '__overlay',
    eyeIcon: 'card-animal-adoptie__container--' + gen + '__overlay__eye-icon',
    button: 'card-animal-adoptie__button',
  };
  const handleClick = (id: string) => {
    onClick(id);
  };

  return (
    <div className={styles.cardAnimalAdoptie} onClick={() => handleClick(id)}>
      <div className={styles.container}>
        <div className={styles.imagine}>
          <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${imagine}`} alt={titluAnunt} />
        </div>
        <div className={styles.overlay}>
          <div className={styles.eyeIcon}>
            {' '}
            <AiOutlineEye />{' '}
          </div>
        </div>
      </div>
      <h2> {titluAnunt} </h2>
      <p> Categorie: {categorie} </p>
      <p> Județ: {judet} </p>
      <p> Gen: {gen} </p>
      <div className={styles.button}>
        {adoptat ? (
          <Button
            type={CustomTypes.ButtonTypes.SECONDARY}
            label={STATUS_ADOPTAT}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          />
        ) : (
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            icon={<BiDetail />}
            label={DETALII}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          />
        )}
      </div>
    </div>
  );
};