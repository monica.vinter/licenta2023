import './CardInformatiiStapan.scss';

interface CardInformatiiStapanProps {
  numeStapan: string;
  emailStapanAnimal?: string,
  NrTelefonStapan?: string,
}

export const CardInformatiiStapan = ({
  numeStapan,
  emailStapanAnimal,
  NrTelefonStapan,
}: CardInformatiiStapanProps) => {
  const styles = {
    formularProfil: 'formular_profil',
    informatii: 'formular_profil__informatii',
    status: 'formular_profil__status',
  };
  return (
    <div className={styles.formularProfil}>
      <div className={styles.informatii}>
        <p>Nume stapan: {numeStapan}</p>
        <p>Email stapan:{emailStapanAnimal}</p>
        <p>Numar telefon stapan:{NrTelefonStapan}</p>
      </div>
    </div>
  );
};
