import './NewsLetterForm.scss';
import { Button } from '../../atoms/Button/Button';
import { Input } from '../../atoms/Input/Input';
import * as CustomTypes from '../../../utils/customTypes';
import * as FormUtils from '../../../mockData/mockDataForms';
import { useFormik } from 'formik';
import EmailSchema from './NewsLetterSchema';
import { EmailData, MediaLinks } from '../../../mockData/mockDataLinks';
import { useDispatch } from 'react-redux';
import { addNewsLetter } from '../../../API/newsLetter';
import { setToast } from '../../../redux/features/toastSlice';
import { SUBSCRIPTION_SUCCESS, SERVER_ERROR } from '../../../utils/toastMessages';
export interface NewsLetterFormProps {
  formTitle?: string;
  formText?: string;
  buttonType: CustomTypes.ButtonTypes;
  buttonLabel?: string;
  inputPlaceholder?: string;
  mediaTitle?: string;
  mediaText?: string;
  mediaLinks?: { href: string; label: string; icon: JSX.Element }[];
}

export const NewsLetterForm = ({
  formTitle,
  formText,
  buttonType,
  buttonLabel,
  inputPlaceholder,
  mediaTitle,
  mediaText,
  mediaLinks = MediaLinks,
}: NewsLetterFormProps) => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      emailAddress: '',
    },
    onSubmit: async function (values) {
      const toastErrorData = { message: SERVER_ERROR, success: false };
      try {
        const newsLetterData = {
          email: values.emailAddress
        };
        const data = await addNewsLetter(newsLetterData);
        if (data) {
          const toastData = { message: SUBSCRIPTION_SUCCESS, success: true };
          dispatch(setToast(toastData));
        } else {
          dispatch(setToast(toastErrorData));
        }
      } catch (error) {
        console.log(error);
      }
    },
    validationSchema: EmailSchema(EmailData),
  });

  const styles = {
    mediaWrapper: 'media-wrapper',
    form: 'media-wrapper__form',
    formTitle: 'media-wrapper__form__title',
    formText: 'media-wrapper__form__text',
  };

  return (
    <div className={styles.mediaWrapper}>
      <div className={styles.form}>
        <h1 className={styles.formTitle}>{formTitle}</h1>
        <p className={styles.formText}>{formText}</p>
        <div>
          <Input
            type={CustomTypes.InputTypes.EMAIL}
            name={FormUtils.FormInputNames.emailAddress}
            placeholder={inputPlaceholder}
            error={
              formik.touched.emailAddress && formik.errors.emailAddress
                ? formik.errors.emailAddress
                : undefined
            }
            value={formik.values.emailAddress}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          ></Input>
          <Button
            type={buttonType}
            label={buttonLabel}
            onClick={() => formik.handleSubmit()}
          ></Button>
        </div>
      </div>
    </div>
  );
};