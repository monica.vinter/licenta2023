import { REGEX_EMAIL } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface EmailSchemaProps {
  emailAddress: {
    required: string;
    format: string;
  };
}

const EmailSchema = (validationMsgs: EmailSchemaProps) => {
  const { emailAddress } = validationMsgs;

  return yup.object({
    emailAddress: yup
      .string()
      .trim()
      .required(emailAddress.required)
      .matches(REGEX_EMAIL, emailAddress.format),
  });
};

export default EmailSchema;
