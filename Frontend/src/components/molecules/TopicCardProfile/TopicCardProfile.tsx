import { BsPeopleFill } from 'react-icons/bs';
import { FiCalendar, FiClock } from 'react-icons/fi';
import { Button } from '../../atoms';
import './TopicCardProfile.scss';
import * as CustomTypes from '../../../utils/customTypes';
import { useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { userRegisterData } from '../../../Interfaces';

interface TopicCardProfileProps {
  titlu: string;
  id?: string;
  user?: userRegisterData;
}

export const TopicCardProfile = ({
  user,
  titlu,
  id,
}: TopicCardProfileProps) => {
  const styles = {
    formularProfil: 'formular_profil',
    titlu: 'formular_profil__titlu',
    informatii: 'formular_profil__informatii',
    status: 'formular_profil__status',
  };
  return (
    <div className={styles.formularProfil}>
      <div className={styles.titlu}>
        <Link to={`/pagina-topic/${id}`}>{titlu}</Link>
        <p>Titlu: {titlu}</p>
        <p>User firstname: {user?.firstname} last name: {user?.lastname}</p>
      </div>
    </div>
  );
};
