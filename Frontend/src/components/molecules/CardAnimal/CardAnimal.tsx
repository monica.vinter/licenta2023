import './CardAnimal.scss';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import { Link } from 'react-router-dom';
import { ADOPTA_PATH } from '../../../Routes/routesPath';

export interface CardAnimalProps {
  id: string;
  name?: string;
  image: string;
  buttonLabel: string;
  onClick: (id: string) => void;
}

export const CardAnimal = ({
  id,
  name,
  image,
  buttonLabel,
  onClick,
}: CardAnimalProps) => {
  const styles = {
    cardAnimal: 'card-animal',
    container: 'card-animal__container',
    button: 'card-animal__button',
    image: 'card-animal__image',
  };

  const handleClick = (id: string) => {
    onClick(id);
  };

  return (
    // TO DO change mock data
    <Link to={`${ADOPTA_PATH}/${name}`}>
      <div className={styles.cardAnimal} onClick={() => handleClick(id)}>
        <div className={styles.container}>
          <img className={styles.image} src={image} alt={name}></img>
        </div>
        <div className={styles.button}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={buttonLabel}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          />
        </div>
      </div>
    </Link>
  );
};
