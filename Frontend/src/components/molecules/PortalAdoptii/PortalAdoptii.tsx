import './PortalAdoptii.scss';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import { Link } from 'react-router-dom';

export interface ShopPortalProps {
  title: string;
  text: string;
  label?: string;
  type?: CustomTypes.ButtonTypes;
}

export const PortalAdoptii = ({
  title,
  text,
  label = 'Adoptă acum',
  type = CustomTypes.ButtonTypes.PRIMARY,
}: ShopPortalProps) => {
  const styles = {
    portal: 'portal',
    title: 'portal__title',
    text: 'portal__text',
  };

  return (
    <div className={styles.portal}>
      <div className={styles.title}>{title}</div>
      <div className={styles.text}>{text}</div>
      <Link to='/adopta' onClick={() => window.scrollTo(200, 0)}>
        <Button type={type} label={label} />
      </Link>
    </div>
  );
};
