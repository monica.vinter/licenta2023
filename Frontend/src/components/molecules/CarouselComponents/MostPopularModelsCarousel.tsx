import ComponentsCarousel from '../Carousel/Carousel';
import * as MockDataCarousels from '../../../mockData/mockDataCarousels';
import * as CustomTypes from '../../../utils/customTypes';
import { CardAnimal } from '../CardAnimal/CardAnimal';
import '../../../components/molecules/CarouselComponents/MostPopularModelsCarousel.scss'

export const MostPopularModelsCarousel = () => {
  return (
    <ComponentsCarousel
      label={MockDataCarousels.MostPopularModelsData.label}
      type={MockDataCarousels.MostPopularModelsData.type}
      extraLargeDesktopItems={MockDataCarousels.MostPopularModelsData.extraLargeDesktopItems}
      itemsToShowExtraLargeDesktop={MockDataCarousels.MostPopularModelsData.itemsToShowExtraLargeDesktop}
      largeDesktopItems={MockDataCarousels.MostPopularModelsData.largeDesktopItems}
      itemsToShowLargeDesktop={MockDataCarousels.MostPopularModelsData.itemsToShowLargeDesktop}
      desktopItems={MockDataCarousels.MostPopularModelsData.desktopItems}
      itemsToShowDesktop={MockDataCarousels.MostPopularModelsData.itemsToShowDesktop}
      largeTabletItems={MockDataCarousels.MostPopularModelsData.largeTabletItems}
      itemsToShowLargeTablet={MockDataCarousels.MostPopularModelsData.itemsToShowLargeTablet}
      tabletItems={MockDataCarousels.MostPopularModelsData.tabletItems}
      itemsToShowTablet={MockDataCarousels.MostPopularModelsData.itemsToShowTablet}
      mobileItems={MockDataCarousels.MostPopularModelsData.mobileItems}
      itemsToShowMobile={MockDataCarousels.MostPopularModelsData.itemsToShowMobile}
    >
      {MockDataCarousels.MostPopularModels.map((slide: any, index: number) => (
        <div key={index} className='component_carousel__card'>
          <CardAnimal
            id='1'
            name='NOU'
            image={slide.image}
            buttonLabel={CustomTypes.VIEW_SHOP}
            onClick={() => {
              console.log('');
            }}
          />
        </div>
      ))}
    </ComponentsCarousel>
  );
};
