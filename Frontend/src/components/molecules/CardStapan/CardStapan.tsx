import './CardStapan.scss';

export interface CardStapanProps {
  numeStapan?: string,
  emailStapanAnimal?: string,
  nrTelefonStapan?: string,
}

export const CardStapan = ({ numeStapan, emailStapanAnimal, nrTelefonStapan}: CardStapanProps) => {
  const styles = {
    formContainer: 'card-stapan__container',
  };
  return (
      <div className={styles.formContainer}>
      <form>
        <h1> Nume: {numeStapan} </h1>
        <h1> Email: {emailStapanAnimal} </h1>
        <h1> Număr telefon: {nrTelefonStapan} </h1>
      </form>
    </div>
  );
};
