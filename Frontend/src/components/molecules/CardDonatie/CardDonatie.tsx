import { BsBank2 } from 'react-icons/bs';
import './CardDonatie.scss';
import { FaEuroSign, FaMoneyBillWave } from 'react-icons/fa';
import { TbBarcode } from 'react-icons/tb';
import { BiTransfer } from 'react-icons/bi';

export interface CardDonatieProps {
  titluDonatie?: string,
  banca?: string,
  ibanRON?: string,
  ibanEURO?: string,
  codSwift?: string,
}

export const CardDonatie = ({ titluDonatie, banca, ibanRON, ibanEURO, codSwift }: CardDonatieProps) => {
  const styles = {
    formContainer: 'card-donatie__container',
    informatii: 'card-donatie__container__information',
  };
  return (
    <div className={styles.formContainer}>
        <h2> {titluDonatie} &nbsp; <span className={'icon'}><BiTransfer /></span>  </h2>
        <h1>  <span className='icon'><BsBank2 /></span> &nbsp; Banca: <span className={styles.informatii}> <b>{banca}</b> </span> </h1>
        <h1>  <span className='icon'><FaMoneyBillWave /></span> &nbsp; IBAN Ron: <span className={styles.informatii}> <b>{ibanRON}</b> </span></h1>
        <h1>  <span className='icon'><FaEuroSign /></span> &nbsp; IBAN Euro: <span className={styles.informatii}> <b>{ibanEURO}</b> </span> </h1>
        <h1>  <span className='icon'><TbBarcode /></span> &nbsp; Cod Swift: <span className={styles.informatii}> <b>{codSwift}</b> </span> </h1>
    </div>
  );
};
