import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const PersonalData = {
    emailAddress: {
        required: FormRequiredInputs.emailAddress,
        format: FormFormatInputs.emailAddress,
    },
};