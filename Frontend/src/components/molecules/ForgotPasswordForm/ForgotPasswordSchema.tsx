import { REGEX_EMAIL } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface PersonalInformationSchemaProps {
  emailAddress: {
    required: string;
    format: string;
  };
}

const personalInformationSchema = (validationMsgs: PersonalInformationSchemaProps) => {
  const { emailAddress } = validationMsgs;

  return yup.object({
    emailAddress: yup
      .string()
      .trim()
      .required(emailAddress.required)
      .matches(REGEX_EMAIL, emailAddress.format),
  });
};

export default personalInformationSchema;
