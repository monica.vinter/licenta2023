import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import { PersonalData } from './ForgotPasswordFormUtils';
import './ForgotPassword.scss';
import { AiOutlineMail } from 'react-icons/ai';
import personalInformationSchema from './ForgotPasswordSchema';
import { useNavigate } from 'react-router-dom';
import { generateSecurityKey } from '../../../API/userAPI';
import { addCookie } from '../../../utils/utilsFunctions';
import { EMAIL } from '../../../utils/Constants';
import { useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { LOGIN_PATH, RESET_PATH } from '../../../Routes/routesPath';
import { FORGOT_PASS_SUCCESS_MESSAGE, SERVER_ERROR } from '../../../utils/toastMessages';

import { useRestrict } from '../../../utils/customHooks';

export const ForgotPasswordForm: React.FunctionComponent = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const navigateLogin = () => {
    navigate(LOGIN_PATH);
  };
  const navigateResetPassword = () => {
    navigate(RESET_PATH);
  };
  useRestrict();

  const formik = useFormik({
    initialValues: {
      emailAddress: '',
    },

    onSubmit: async function (values) {
      const toastErrorData = { message: SERVER_ERROR, success: false };
      try {
        const userData = {
          email: values.emailAddress,
        };
        const data = await generateSecurityKey(userData);
        if (data) {
          addCookie(EMAIL, userData.email);
          navigateResetPassword();
          const toastData = { message: FORGOT_PASS_SUCCESS_MESSAGE, success: true };
          dispatch(setToast(toastData));
        } else {
          dispatch(setToast(toastErrorData));
        }
      } catch (error) {
        dispatch(setToast(toastErrorData));
        console.log(error);
      }
    },

    validationSchema: personalInformationSchema(PersonalData),
  });

  const styles = {
    formContainer: 'forgot-password-form__container',
    singleInputRow: 'forgot-password-form__single-input-row',
    buttonWrapper: 'forgot-password-form__button-wrapper',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.forgotPassword} </h1>
        <h3> {FormInformation.pleaseEnter} </h3>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.emailAddress}
            name={FormInputNames.emailAddress}
            label={FormInputLabels.emailAddress}
            type={CustomTypes.InputTypes.EMAIL}
            placeholder={FormInputPlaceholders.emailAddress}
            rightIcon={<AiOutlineMail />}
            required
            error={
              formik.touched.emailAddress && formik.errors.emailAddress
                ? formik.errors.emailAddress
                : undefined
            }
            value={formik.values.emailAddress}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.buttonWrapper}>
          <Button
            type={CustomTypes.ButtonTypes.SECONDARY}
            label={FormButtonsLabel.backLogin}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={navigateLogin}
          />
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.sendVerificationCode}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => {
              formik.handleSubmit();
            }}
          />
        </div>
      </form>
    </div>
  );
};
