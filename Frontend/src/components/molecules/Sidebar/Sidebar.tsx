import React from 'react';
import { SidebarLinks } from '../../../mockData/mockDataLinks';
import './Sidebar.scss';
import SubMenu from './SubMenu';

const Sidebar: React.FC = () => {
    const styles = {
        sidebarContainer: 'sidebar__container',
        sidebarContent: 'sidebar__content',
    }
    return (
        <div className={styles.sidebarContainer}>
            <div className={styles.sidebarContent}>
                {SidebarLinks.map((item, index) => (
                    <SubMenu key={index} item={item} />
                ))}
            </div>
        </div>
    );
}

export default Sidebar;