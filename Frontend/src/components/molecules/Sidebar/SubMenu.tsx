import './Sidebar.scss';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';

export interface SubMenu {
    item: any;
}

const SubMenu: React.FC<SubMenu> = ({ item }) => {
    const [subnav, setSubnav] = useState(false);
    const showSubnav = () => setSubnav(!subnav);
    const styles = {
        sidebarWrapper: 'sidebar__wrapper',
        sidebarLink: 'sidebar__link',
        sidebarIcon: 'sidebar__link-icon',
        sidebarText: 'sidebar__link-text',
        sidebarSubmenuLink: 'sidebar-submenu__link',
        sidebarSubmenuIcon: 'sidebar-submenu__link-icon',
        sidebarSubmenuText: 'sidebar-submenu__link-text',
        sidebarSubmenuOpen: 'sidebar-submenu-open',
        sidebarSubmenuClose: 'sidebar-submenu-close',
    }
    return (
        <NavLink
            to={item.href}
            className={styles.sidebarLink}
            onClick={showSubnav}>
            <div className={styles.sidebarWrapper}>
                <img src={item.icon} className={styles.sidebarIcon} />
                <div className={styles.sidebarText}>{item.label}</div>
                <div>
                    {item.subNav && subnav
                        ? item.iconOpened
                        : item.subNav
                            ? item.iconClosed
                            : null}
                </div>
            </div>
            <div className={item.subNav && subnav && item.iconOpened ? styles.sidebarSubmenuOpen : styles.sidebarSubmenuClose}>
                {subnav && item.subNav.map((item: any, index: any) => {
                    return (
                        <NavLink to={item.path} key={index} className={styles.sidebarSubmenuLink}>
                            <div className={styles.sidebarWrapper}>
                                <div className={styles.sidebarSubmenuIcon}>{item.icon}</div>
                                <div className={styles.sidebarSubmenuText}>{item.label}</div>
                            </div>
                        </NavLink>
                    );
                })}
            </div>
        </NavLink>
    );
}

export default SubMenu;