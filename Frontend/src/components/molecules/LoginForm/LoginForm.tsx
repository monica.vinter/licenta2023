import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import loginInformationSchema from './LoginSchema';
import { loginData } from './LoginUtils';
import './LoginForm.scss';
import { AiOutlineMail } from 'react-icons/ai';
import { useNavigate } from 'react-router-dom';
import { loginUser } from '../../../redux/features/userSlice';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, AppThunkDispatch, RootState } from '../../../redux/store';
import { setToast } from '../../../redux/features/toastSlice';
import { FORGOT_PATH, ACASA_PATH, REGISTER_PATH } from '../../../Routes/routesPath';
import {
  INVALID_CREDENTIALS,
  LOGIN_SUCCESS_MESSAGE,
  SERVER_ERROR,
} from '../../../utils/toastMessages';
import { useRestrict } from '../../../utils/customHooks';

export const LoginForm: React.FunctionComponent = () => {
  const dispatch = useDispatch<AppDispatch>();
  const dispatchThunk = useDispatch<AppThunkDispatch>();
  const navigate = useNavigate();

  const { isConnected } = useSelector((state: RootState) => state.userState);
  const navigateRegister = () => {
    navigate(REGISTER_PATH);
  };
  const navigateForgotPassword = () => {
    navigate(FORGOT_PATH);
  };

  useRestrict();
  const formik = useFormik({
    initialValues: {
      emailAddress: '',
      password: '',
    },

    onSubmit: function (values) {
      const toastData = { message: SERVER_ERROR, success: false };

      try {
        const userData = {
          email: values.emailAddress,
          password: values.password,
        };
        dispatchThunk(loginUser(userData));
        if (!isConnected) {
          navigate(ACASA_PATH);
          const toastData = { message: LOGIN_SUCCESS_MESSAGE, success: true };
          dispatch(setToast(toastData));
        } else {
          const toastData = { message: INVALID_CREDENTIALS, success: false };
          dispatch(setToast(toastData));
        }
      } catch (error) {
        dispatch(setToast(toastData));
        console.log(error);
      }
    },

    validationSchema: loginInformationSchema(loginData),
  });

  const styles = {
    formContainer: 'login-form__container',
    singleInputRow: 'login-form__single-input-row',
    registerPortal: 'login-form__register-portal',
    forgotPassword: 'login-form__forgot-password-button',
    loginButton: 'login-form__login-button',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.loginHere} </h1>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.emailAddress}
            name={FormInputNames.emailAddress}
            label={FormInputLabels.emailAddress}
            type={CustomTypes.InputTypes.EMAIL}
            placeholder={FormInputPlaceholders.emailAddress}
            rightIcon={<AiOutlineMail />}
            required
            error={
              formik.touched.emailAddress && formik.errors.emailAddress
                ? formik.errors.emailAddress
                : undefined
            }
            value={formik.values.emailAddress}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.password}
            name={FormInputNames.password}
            label={FormInputLabels.loginPassword}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.password}
            required
            error={
              formik.touched.password && formik.errors.password ? formik.errors.password : undefined
            }
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div className={styles.forgotPassword}>
          <Button
            type={CustomTypes.ButtonTypes.TERTIARY}
            label={FormButtonsLabel.forgotPassword}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={navigateForgotPassword}
          />
        </div>
        <div className={styles.loginButton}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.login}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => formik.handleSubmit()}
          />
        </div>
        <div className={styles.registerPortal}>
          <h4> {FormInformation.notMember} </h4>
          <Button
            type={CustomTypes.ButtonTypes.TERTIARY}
            label={FormButtonsLabel.signupHere}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={navigateRegister}
          />
        </div>
      </form>
    </div>
  );
};
