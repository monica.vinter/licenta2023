import { REGEX_EMAIL, REGEX_PASSWORD } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface LoginInformationSchemaProps {
  emailAddress: {
    required: string;
    format: string;
  };
  password: {
    required: string;
    format: string;
  };
}

const loginInformationSchema = (validationMsgs: LoginInformationSchemaProps) => {

  const { emailAddress, password } = validationMsgs;

  return yup.object({
    emailAddress: yup
      .string()
      .trim()
      .required(emailAddress.required)
      .matches(REGEX_EMAIL, emailAddress.format),
    password: yup
      .string()
      .trim()
      .required(password.required)
      .matches(REGEX_PASSWORD, password.format),
  })
};

export default loginInformationSchema;