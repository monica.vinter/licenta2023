import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const loginData = {
    emailAddress: {
        required: FormRequiredInputs.emailAddress,
        format: FormFormatInputs.emailAddress,
    },
    password: {
        required: FormRequiredInputs.password,
        format: FormFormatInputs.password,
    },
};