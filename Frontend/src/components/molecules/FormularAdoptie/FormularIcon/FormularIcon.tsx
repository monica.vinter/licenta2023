import FormularAdoptie from '../FormularAdoptie/FormularAdoptie';
import { useCart } from '../../../../utils/customHooks';
import './FormularIcon.scss';
import * as MockData from '../../../../mockData/mockDataComponents';
import { getTotalItems } from '../../../../utils/utilsFunctions';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../redux/store';

import { AiOutlineForm } from 'react-icons/ai';

export const FormularIcon: React.FunctionComponent = () => {
  const { isOpen, toggle } = useCart();
  const formulare = useSelector((state: RootState) => state.formulareFereastraState.formulare);
  const styles = {
    formularIcon: 'formular-icon__container',
    formularIconButton: 'formular-icon__button',
    formularNrOfItems: 'formular-icon__nr-items',
  };
  return (
    <div className={styles.formularIcon}>
      <div className={styles.formularNrOfItems}>
        <p> {getTotalItems(formulare).totalQuantity} </p>
      </div>
      <button className={styles.formularIconButton} onClick={toggle}>
        <AiOutlineForm fontSize={'2rem'} color={'white'} />{' '}
      </button>
      {toggle && (
        <FormularAdoptie
          isOpen={isOpen}
          toggle={toggle}
          title={MockData.FormularAdoptie.title}
          subTitle={MockData.FormularAdoptie.subTitle}
          buttonLabel={MockData.FormularAdoptie.buttonLabel}
        ></FormularAdoptie>
      )}
    </div>
  );
};
