import * as yup from 'yup';

interface FormularInformationSchemaProps {
  descriereConditii: {
    required: string;
  };
}

const FormularInformationSchema = (validationMsgs: FormularInformationSchemaProps) => {
  const { descriereConditii } = validationMsgs;

  return yup.object({
    descriereConditii: yup
      .string()
      .trim()
      .required(descriereConditii.required),
  });
};

export default FormularInformationSchema;