import { useFormik } from 'formik';
import { Input } from '../../../atoms/Input/Input';
import { Button } from '../../../atoms/Button/Button';
import * as CustomTypes from '../../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInputNames,
} from '../../../../mockData/mockDataForms';
import formularInformationSchema from './FormularAdoptieAnimalSchema';
import { adaugaFormular } from '../../../../API/formularAPI';
import React from 'react';
import './FormularAdoptieAnimal.scss';
import { IoMdClose } from 'react-icons/io';
import {
  removeItem,
} from '../../../../redux/features/formulareSlice';
import { FORMULAR_ADAUGAT_SUCCES, SERVER_ERROR } from '../../../../utils/toastMessages';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../../redux/store';
import { setToast } from '../../../../redux/features/toastSlice';
import { FormularData } from './FormularAdoptieAnimalUtils';
import { useNavigate } from 'react-router-dom';
import { LOGIN_PATH } from '../../../../Routes/routesPath';

export interface FormularAdoptieProps {
  animalId: string;
}

const FormularAdoptieAnimal: React.FC<FormularAdoptieProps> = ({ animalId }) => {
  const dispatch = useDispatch<AppDispatch>();
  const formulare = useSelector((state: RootState) => state.formulareFereastraState.formulare);
  const { isConnected, userData } = useSelector((state: RootState) => state.userState);
  const navigate = useNavigate();

  const currentAnimal = formulare.find((item) => {
    return item.animal?._id === animalId;
  });

  const handleDeleteItem = () => {
    dispatch(removeItem({ animalId }));
  };

  const formik = useFormik({
    initialValues: {
      descriereConditii: '',
    },

    onSubmit: async function (values) {
      if (isConnected) {
        const toastErrorData = { message: SERVER_ERROR, success: false };
        try {
          const FormularData = {
            descriereConditii: values.descriereConditii,
            numeViitorStapan: userData?.firstname + ' ' + userData?.lastname,
            emailViitorStapan: userData?.email,
            nrTelViitorStapan: userData?.phone,
            numeStapanAnimal: currentAnimal?.animal.numeStapan,
            nrTelStapanAnimal: currentAnimal?.animal.numarTelefonStapan,
            emailStapanAnimal: currentAnimal?.animal.emailStapan,
            status: currentAnimal?.animal.adoptat,
            idAnimal: currentAnimal?.animal._id,
            imagineAnimal: currentAnimal?.animal.imagini[0],
            titluAnuntAnimal: currentAnimal?.animal.titluAnunt,
          };
          console.log(FormularData);
          const data = await adaugaFormular(FormularData);
          dispatch(removeItem({ animalId }));
          if (data) {
            const toastData = { message: FORMULAR_ADAUGAT_SUCCES, success: true };
            dispatch(setToast(toastData));
          } else {
            dispatch(setToast(toastErrorData));
          }
        } catch (error) {
          dispatch(setToast(toastErrorData));
          console.log(error);
        }
      }
      else {
        navigate(LOGIN_PATH);
      }
    },

    validationSchema: formularInformationSchema(FormularData),
  });

  const styles = {
    formularAdoptieAnimalContainer: 'formular-adoptie-animal__container--' + currentAnimal?.animal?.gen,
    formularAdoptieAnimalContent: 'formular-adoptie-animal__content',
    formularAdoptieAnimalImage: 'formular-adoptie-animal__image',
    formularAdoptieAnimalDetails: 'formular-adoptie-animal__details',
    formularAdoptieAnimalText: 'formular-adoptie-animal__text',
    formularAdoptieAnimalEnd: 'formular-adoptie-animal__end',
    formularAdoptieAnimalDelete: 'formular-adoptie-animal__delete',
  };
  return (
    <div className={styles.formularAdoptieAnimalContainer}>
      <div className={styles.formularAdoptieAnimalContent}>
        <div className={styles.formularAdoptieAnimalImage}>
          <img
            src={`https://res.cloudinary.com/dssizmzir/image/upload/${currentAnimal?.animal?.imagini[0]}`}
            alt={currentAnimal?.animal?.titluAnunt}
          />
        </div>
        <div className={styles.formularAdoptieAnimalDetails}>
          <div className={styles.formularAdoptieAnimalText}>

          </div>
        </div>
        <form>
          <Input
            id={FormInputNames.descriereConditii}
            name={FormInputNames.descriereConditii}
            label={FormInputLabels.descriereConditii}
            type={CustomTypes.InputTypes.MULTILINE}
            styleType={CustomTypes.InputStyleTypes.TEXTAREA}
            placeholder={FormInputPlaceholders.descriereConditii}
            required
            error={
              formik.touched.descriereConditii && formik.errors.descriereConditii
                ? formik.errors.descriereConditii
                : undefined
            }
            value={formik.values.descriereConditii}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.submit}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => formik.handleSubmit()}
          />
        </form>
      </div>
      <div className={styles.formularAdoptieAnimalEnd}>
        <button className={styles.formularAdoptieAnimalDelete} onClick={() => handleDeleteItem()}>
          <IoMdClose fontSize={'2.8rem'} color={'white'} />
        </button>
      </div>
    </div>
  );
};

export default FormularAdoptieAnimal;
