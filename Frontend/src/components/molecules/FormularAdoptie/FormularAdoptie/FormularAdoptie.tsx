import './FormularAdoptie.scss';
import * as CustomTypes from '../../../../utils/customTypes';
import FormularAdoptieAnimal from '../FormularAdoptieAnimal/FormularAdoptieAnimal';
import { IoMdClose } from 'react-icons/io';
import { Button } from '../../../atoms';
import { useNavigate } from 'react-router-dom';
import { DateFormular } from '../../../../Interfaces/formularInterface';
import { useSelector } from 'react-redux';
import { RootState } from '../../../../redux/store';
import { getTotalItems } from '../../../../utils/utilsFunctions';
import { ACASA_PATH } from '../../../../Routes/routesPath';
import { TbTruckDelivery } from 'react-icons/tb';
import clsx from 'clsx';
import { useState } from 'react';

interface FormularAdoptieProps {
  isOpen: boolean;
  toggle: () => void;
  title: string;
  subTitle: string;
  buttonLabel: string;
}

const FormularAdoptie: React.FC<FormularAdoptieProps> = ({ isOpen, toggle, title, subTitle, buttonLabel }) => {
  const navigate = useNavigate();
  const [closed, setClosed] = useState(false);
  const formulare = useSelector((state: RootState) => state.formulareFereastraState.formulare);

  const handleNrOfItems = () => {
    const nrOfItems = getTotalItems(formulare).totalQuantity;
    if (nrOfItems > 1) {
      return <p className={styles.formularAdoptieNrOfItems}>({nrOfItems} formulare)</p>;
    } else if (nrOfItems == 1) {
      return <p className={styles.formularAdoptieNrOfItems}>({nrOfItems} formular)</p>;
    } else {
      return <p className={styles.formularAdoptieNrOfItems}>(Nu ai introdus niciun formular pentru vreun animal)</p>;
    }
  };


  const handleToggle = () => {
    setClosed(true);
    setTimeout(() => {
      toggle();
      setClosed(false);
    }, 1000);
  };

  const styles = {
    formularAdoptie: 'formular-adoptie',
    formularAdoptieOverlay: clsx('formular-adoptie__overlay', closed && 'formular-adoptie__overlay--closed'),
    formularAdoptieContainer: clsx('formular-adoptie__container', closed && 'formular-adoptie__container--closed'),
    formularAdoptieBox: 'formular-adoptie__box',
    formularAdoptieHeader: 'formular-adoptie__header',
    formularAdoptieClose: 'formular-adoptie__close',
    formularAdoptieContent: 'formular-adoptie__content',
    formularAdoptieTitle: 'formular-adoptie__title',
    formularAdoptieTitleColor: 'formular-adoptie__title--color',
    formularAdoptieNrOfItems: 'formular-adoptie__nr-items',
  };

  return (
    <>
      {isOpen && (
        <div className={styles.formularAdoptie}>
          <div className={styles.formularAdoptieOverlay}></div>
          <div className={styles.formularAdoptieContainer}>
            <button className={styles.formularAdoptieClose} onClick={handleToggle}>
              <IoMdClose fontSize={'4rem'} color={'white'} />
            </button>
            <div className={styles.formularAdoptieBox}>
              <div className={styles.formularAdoptieHeader}>
                <div className={styles.formularAdoptieTitle}>
                  <p>{title}&nbsp;</p>
                  <p className={styles.formularAdoptieTitleColor}>{subTitle}</p>
                </div>
                {handleNrOfItems()}
              </div>
              <div className={styles.formularAdoptieContent}>
                {formulare.map((item: DateFormular) => (
                  <div key={item.animal?._id}>
                    <FormularAdoptieAnimal animalId={item.animal?._id} />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default FormularAdoptie;
