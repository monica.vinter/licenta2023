import React from 'react';
import './Carousel.scss'
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { CarouselInterface } from '../../../Interfaces/carouselInterface';

const CarouselSlider: React.FC<CarouselInterface> = ({
  children,
  label,
  extraLargeDesktopItems = 4,
  itemsToShowExtraLargeDesktop = 10,
  largeDesktopItems = 4,
  itemsToShowLargeDesktop = 0,
  desktopItems = 3,
  itemsToShowDesktop = 0,
  largeTabletItems = 2,
  itemsToShowLargeTablet = 40,
  tabletItems = 2,
  itemsToShowTablet = 0,
  mobileItems = 1,
  itemsToShowMobile = 0,
}) => {

  const styles = { componentCarousel: 'component_carousel__container', componentCarouselLabel: 'component_carousel__label' }

  const responsive = {
    desktop5: {
      breakpoint: { max: 1920, min: 1500 },
      items: extraLargeDesktopItems,
      partialVisibilityGutter: itemsToShowExtraLargeDesktop
    },
    desktop4: {
      breakpoint: { max: 1499, min: 1399 },
      items: largeDesktopItems,
      partialVisibilityGutter: itemsToShowLargeDesktop
    },
    desktop3: {
      breakpoint: { max: 1400, min: 1100 },
      items: desktopItems,
      partialVisibilityGutter: itemsToShowDesktop
    },
    tablet3: {
      breakpoint: { max: 1099, min: 850 },
      items: largeTabletItems,
      partialVisibilityGutter: itemsToShowLargeTablet
    },
    tablet2: {
      breakpoint: { max: 849, min: 780 },
      items: tabletItems,
      partialVisibilityGutter: itemsToShowTablet
    },
    mobile: {
      breakpoint: { max: 779, min: 0 },
      items: mobileItems,
      partialVisibilityGutter: itemsToShowMobile
    },
  };
  return (
    <div className={styles.componentCarousel}>
      <h3 className={styles.componentCarouselLabel}>{label}</h3>
      <Carousel
        partialVisible={true}
        swipeable={true}
        draggable={true}
        showDots={false}
        responsive={responsive}
        ssr={true}
        infinite={true}
        autoPlay={true}
        autoPlaySpeed={1000}
        keyBoardControl={true}
        customTransition='all .5'
        transitionDuration={500}
        containerClass='carousel-container'
        removeArrowOnDeviceType={['desktop5', 'desktop4', 'desktop3', 'tablet3', 'tablet2', 'mobile']}
      >
        {children}
      </Carousel>
    </div>
  )
}

export default CarouselSlider