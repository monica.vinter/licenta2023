import { CardAnimalSfaturi } from '../CardAnimaSfaturi/CardAnimaSfaturi';
import * as MockData from '../../../mockData/mockDataComponents';
import './CategoriiAnimaleSfaturi.scss'
import { IoMdPaw } from 'react-icons/io';
function scrollWin() {
  window.scrollTo(200, 0);
}
export const CategoriiAnimaleSfaturi = () => {

  const styles = {
    categoriiAnimaleContainer: 'categorii-animale__container',
    title: 'categorii-animale__title',
    card: 'categorii-animale__container-animale'
  };

  return (
    <div className={styles.categoriiAnimaleContainer}>
      <h2 className={styles.title}>Află mai multe despre prietenul tău <span className='icon'><IoMdPaw /></span> </h2>
      <div className={styles.card}>
        {MockData.CategoriiAnimaleSfaturi.map((category: any, index: number) => (
          <div key={index} onClick={scrollWin}>
            <CardAnimalSfaturi
              id={category.id}
              name={category.name}
              image={category.image}
              buttonLabel={category.buttonLabel}
              onClick={category.onClick}
            />
          </div>
        ))}
      </div>
    </div>
  );
};