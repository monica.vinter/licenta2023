import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../redux/store';
import { clearToast } from '../../../redux/features/toastSlice';
import { useEffect } from 'react';
import './ToastFile.scss';

function ToastFile() {
  const dispatch = useDispatch();
  const { toastState } = useSelector((state: RootState) => state);

  useEffect(() => {
    if (toastState?.success != null && toastState?.message != null) {
      if (toastState?.success) {
        toast.success(toastState?.message, {
          position: toast.POSITION.BOTTOM_RIGHT,
          className: 'custom-toast',
        });
      } else {
        toast.error(toastState?.message, {
          position: toast.POSITION.BOTTOM_RIGHT,
          className: 'custom-toast',
        });
      }
      dispatch(clearToast());
    }
  }, [toastState]);

  return <ToastContainer autoClose={3000} />;
}

export default ToastFile;
