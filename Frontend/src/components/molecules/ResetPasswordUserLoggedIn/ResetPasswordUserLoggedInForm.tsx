import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import './ResetPasswordUserLoggedIn.scss';
import { resetPassword } from '../../../API/userAPI';
import resetPasswordUserLoggedInSchema from './ResetPasswordUserLoggedInSchema';
import { ResetPasswordUserLoggedInData } from './ResetPasswordUserLoggedInUtils';
import { deleteCookie } from '../../../utils/utilsFunctions';
import { TOKEN } from '../../../utils/Constants';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { LOGIN_PATH } from '../../../Routes/routesPath';
import { RESET_PASS_SUCCESS_MESSAGE, SERVER_ERROR } from '../../../utils/toastMessages';

export const ResetPasswordUserLoggedInForm: React.FunctionComponent = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      password: '',
      confirmPassword: '',
    },

    onSubmit: async function (values) {
      try {
        const userData = {
          password: values.password,
        };
        const data = await resetPassword(userData);
        if (data) {
          deleteCookie(TOKEN);
          navigate(LOGIN_PATH);
          const toastData = { message: RESET_PASS_SUCCESS_MESSAGE, success: true };
          dispatch(setToast(toastData));
        } else {
          const toastData = { message: SERVER_ERROR, success: false };
          dispatch(setToast(toastData));
        }
      } catch (error) {
        const toastData = { message: SERVER_ERROR, success: false };

        dispatch(setToast(toastData));
        console.log(error);
      }
    },

    validationSchema: resetPasswordUserLoggedInSchema(ResetPasswordUserLoggedInData),
  });

  const styles = {
    formContainer: 'reset-pass-user-logged-in__container',
    singleInputRow: 'reset-pass-user-logged-in__single-input-row',
    doubleInputRow: 'reset-pass-user-logged-in__double-input-row',
    textRequired: 'reset-pass-user-logged-in__text-required',
    resetButton: 'reset-pass-user-logged-in__reset-button',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.resetPassword} </h1>

        <div className={styles.doubleInputRow}>
          <Input
            id={FormInputNames.password}
            name={FormInputNames.password}
            label={FormInputLabels.resetPassword}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.password}
            required
            formatInfo={FormInformation.passwordInfo}
            error={
              formik.touched.password && formik.errors.password ? formik.errors.password : undefined
            }
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div>
          <Input
            id={FormInputNames.confirmPassword}
            name={FormInputNames.confirmPassword}
            label={FormInputLabels.resetConfirmPassword}
            type={CustomTypes.InputTypes.PASSWORD}
            placeholder={FormInputPlaceholders.confirmPassword}
            required
            error={
              formik.touched.confirmPassword && formik.errors.confirmPassword
                ? formik.errors.confirmPassword
                : undefined
            }
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <p className={styles.textRequired}> {FormInformation.requiredField} </p>
        <div className={styles.resetButton}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.resetPassword}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => formik.handleSubmit()}
          />
        </div>
      </form>
    </div>
  );
};
