import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const ResetPasswordUserLoggedInData = {
  password: {
    required: FormRequiredInputs.password,
    format: FormFormatInputs.password,
  },
  confirmPassword: {
    required: FormRequiredInputs.confirmPassword,
  },
};
