import { REGEX_PASSWORD } from '../../../utils/schemaUtils';
import * as yup from 'yup';

interface ResetPasswordUserLoggedInSchemaProps {
  password: {
    required: string;
    format: string;
  };
  confirmPassword: {
    required: string;
  };
}

const resetPasswordUserLoggedInSchema = (validationMsgs: ResetPasswordUserLoggedInSchemaProps) => {
  const { password, confirmPassword } = validationMsgs;

  return yup.object({
    password: yup
      .string()
      .trim()
      .required(password.required)
      .matches(REGEX_PASSWORD, password.format),
    confirmPassword: yup
      .string()
      .trim()
      .required(confirmPassword.required)
      .test('Password-match!', 'Password must match!', function (value) {
        return this.parent.password === value;
      }),
  });
};

export default resetPasswordUserLoggedInSchema;
