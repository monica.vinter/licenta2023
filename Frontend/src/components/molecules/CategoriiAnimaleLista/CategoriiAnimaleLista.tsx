import './CategoriiAnimaleLista.scss'
import React, { useEffect, useState } from 'react';
import * as MockData from '../../../mockData/mockDataComponents';
import clsx from 'clsx';
import { useParams } from 'react-router-dom';

export interface ShopCategoriesListProps {
  categories: { label: string; index: number }[];
  onClick: (label: string) => void;
}

export const CategoriiAnimaleLista = ({ categories, onClick }: ShopCategoriesListProps) => {
  const [currentSelected, SetCurrentSelected] = useState(1);
  const handleClick = (label: string, id: number) => {
    SetCurrentSelected(id);
    onClick(label);
  };
  const paramCategory = useParams();
  useEffect(() => {
    switch (paramCategory.categorie) {
      case 'Câini': {
        SetCurrentSelected(1);
        break;
      }
      case 'Pisici': {
        SetCurrentSelected(2);
        break;
      }
      case 'Altele': {
        SetCurrentSelected(3);
        break;
      }
      default:
        break;
    }
  }, []);

  const styles = {
    container: 'categorii-animale-lista__container',
    categorie: 'categorii-animale-lista__container__category',
    activeCategory: clsx(
      'categorii-animale-lista__container__category',
      'categorii-animale-lista__container__category--active',
    ),
  };

  return (
    <div className={styles.container}>
      <h1> {MockData.CategoriiPagina.categoriesTitle} </h1>
      <ul>
        {categories.map((categorie) => {
          return (
            <li
              key={categorie.index}
              className={
                categorie.index === currentSelected ? styles.activeCategory : styles.categorie
              }
              onClick={() => handleClick(categorie.label, categorie.index)}
            >
              {categorie.label}
            </li>
          );
        })}
      </ul>
    </div>
  );
};