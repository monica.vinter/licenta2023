import React from 'react';
import './Popup.scss';
import { GrClose } from 'react-icons/gr';
import { PopupReset } from '../../../mockData/mockDataForms';
import { Icon } from '../../../assets/imagePaths';

interface PopupProps {
  isOpen: boolean;
  toggle: () => void;
  message: string;
}

const Popup: React.FC<PopupProps> = ({ isOpen, toggle, message }) => {
  const styles = {
    popupOverlay: 'popup__overlay',
    popupBox: 'popup__box',
    popupHeader: 'popup__header',
    popupClose: 'popup__close',
    popupContent: 'popup__content',
  };
  return (
    <>
      {isOpen && (
        <div className={styles.popupOverlay} onClick={toggle}>
          <div onClick={(e) => e.stopPropagation()} className={styles.popupBox}>
            <div className={styles.popupHeader}>
              <img src={Icon} />
              <p>{message}</p>
              <GrClose onClick={toggle} className={styles.popupClose} />
            </div>
            <div className={styles.popupContent}>
              <p>{PopupReset.updated}</p>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Popup;