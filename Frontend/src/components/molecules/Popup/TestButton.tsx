import Popup from './Popup'
import { usePopup } from '../../../utils/customHooks'

export const TestButton: React.FunctionComponent = () => {
  const { isOpen, toggle } = usePopup();

  return (
    <div>
      <button onClick={toggle}>Open Popup </button>
      <Popup isOpen={isOpen} toggle={toggle} message={'mesaj'}></Popup>
    </div>
  );
};