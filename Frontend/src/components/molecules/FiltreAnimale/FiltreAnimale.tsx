import { Accordion, AccordionItem, Checkbox, ColorFilter } from '../../atoms/index';
import * as MockData from '../../../mockData/mockDataComponents';
import { Culori, Judete, Gen } from '../../../utils/Constants';
import * as CustomTypes from '../../../utils/customTypes';
import './FiltreAnimale.scss';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../redux/store';
import {
  setFilterJudet,
  setFilterGen,
  setFilterColor,
} from '../../../redux/features/animalSlice';
import { Select } from 'antd';

export const FiltreAnimale: React.FunctionComponent = () => {
  const dispatch = useDispatch<AppDispatch>();
  const { Option } = Select;

  const styles = {
    container: 'filtre-animale__container',
    accordion: 'filtre-animale__accordion',
    colorFilter: 'filtre-animale__color-filter-group',
    checkboxGroup: 'filtre-animale__checkbox-group',
    buttons: 'filtre-animale__buttons',
  };
  const handleSelectChange = (selectedValues: string[]) => {
    dispatch(setFilterJudet(selectedValues));
  };


  return (
    <div className={styles.container}>
      <h1> {MockData.FiltreAnimale.title} </h1>
      <div className={styles.accordion}>
        <Accordion backgroundColor={CustomTypes.AccordionBackground.DARK_BLUE}>
          <Accordion backgroundColor={CustomTypes.AccordionBackground.DARK_BLUE}>
            <AccordionItem title={MockData.FiltreAnimale.judet}>
              <div className={styles.checkboxGroup}>
                <Select
                  mode='multiple'
                  placeholder='Alege județul'
                  onChange={handleSelectChange}
                >
                  {Judete.map((judet) => (
                    <Option key={judet} value={judet}>
                      {judet}
                    </Option>
                  ))}
                </Select>
              </div>
            </AccordionItem>
          </Accordion>

          <AccordionItem title={MockData.FiltreAnimale.gen}>
            <div className={styles.checkboxGroup}>
              {Gen.map((gen, index) => {
                return (
                  <Checkbox
                    value={gen}
                    label={gen}
                    onChange={() => dispatch(setFilterGen(gen))}
                    key={index}
                  />
                );
              })}
            </div>
          </AccordionItem>

          <AccordionItem title={MockData.FiltreAnimale.culoare} firstOpen>
            <div className={styles.colorFilter}>
              {Culori.map((color, index) => {
                return (
                  <ColorFilter
                    value={color}
                    color={color}
                    onChange={() => dispatch(setFilterColor(color))}
                    key={index}
                  />
                );
              })}
            </div>
          </AccordionItem>
        </Accordion>
      </div>
      <div className={styles.buttons}>
        {/* <Button
          type={CustomTypes.ButtonTypes.PRIMARY}
          label={MockData.AnimaleFilter.buttonLabelApply}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
        />
        <Button
          type={CustomTypes.ButtonTypes.SECONDARY}
          label={MockData.AnimaleFilter.buttonLabelClear}
          backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
        /> */}
      </div>
    </div>
  );
};

