import { BsPeopleFill } from 'react-icons/bs';
import { FiCalendar, FiClock } from 'react-icons/fi';
import { Button } from '../../atoms';
import './MesajCardProfile.scss';
import * as CustomTypes from '../../../utils/customTypes';
import { useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

interface MesajCardProfileProps {
  mesaj: string;
  numeUtilizator?: string,
  dataPublicarii: Date,
  imagineProfilUtilizator?: string,
  id?: string;
}

export const MesajCardProfile = ({
  imagineProfilUtilizator,
  mesaj,
  numeUtilizator,
  dataPublicarii,
  id,
}: MesajCardProfileProps) => {
  const styles = {
    mesajCardProfil: 'mesaj-card',
    top: 'mesaj-card__top',
    mesaj: 'mesaj-card__mesaj',
    bottom: 'mesaj-card__bottom',
  };
  return (
    <div className={styles.mesajCardProfil}>
      <div className={styles.top}>
        <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${imagineProfilUtilizator}`} />
        <p>{numeUtilizator}</p>
      </div>
      <div className={styles.mesaj}>
        <p>{mesaj}</p>
      </div>
      <div className={styles.bottom}>
        <div>Trimis pe: {moment(dataPublicarii).format('DD MMM YYYY')}, la ora: {moment(dataPublicarii).format('hh:mm:ss')} </div>        </div>
    </div>
  );
};
