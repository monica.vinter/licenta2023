import './CardAnimaSfaturi.scss';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import { Link } from 'react-router-dom';
import { SFATURI_PATH } from '../../../Routes/routesPath';

export interface CardAnimalSfaturiProps {
  id: string;
  name: string;
  image: string;
  buttonLabel: string;
  onClick: (id: string) => void;
}

export const CardAnimalSfaturi = ({
  id,
  name,
  image,
  buttonLabel,
  onClick,
}: CardAnimalSfaturiProps) => {
  const styles = {
    cardAnimal: 'card-animal',
    container: 'card-animal__container',
    button: 'card-animal__button',
    image: 'card-animal__image',
    price: 'card-animal__price',
  };

  const handleClick = (id: string) => {
    onClick(id);
  };

  return (
    // TO DO change mock data
    <Link to={`${SFATURI_PATH}/${name}`}>
      <div className={styles.cardAnimal} onClick={() => handleClick(id)}>
        <div className={styles.container}>
          <img className={styles.image} src={image} alt={name}></img>
        </div>
        <div className={styles.button}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={buttonLabel}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
          />
        </div>
      </div>
    </Link>
  );
};
