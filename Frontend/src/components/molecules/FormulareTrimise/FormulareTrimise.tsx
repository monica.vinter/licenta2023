import { FaCheckCircle, FaClock, FaTimesCircle } from 'react-icons/fa';
import './FormulareTrimise.scss';

interface FormulareTrimiseProps {
  descriereConditii: string,
  numeStapanAnimal?: string,
  emailStapanAnimal?: string,
  nrTelStapanAnimal?: string,
  statusFormular?: string,
  imagineAnimal?: string,
  titluAnuntAnimal?: string,
  id?: string;
}

export const FormulareTrimise = ({
  descriereConditii,
  numeStapanAnimal,
  emailStapanAnimal,
  nrTelStapanAnimal,
  statusFormular,
  imagineAnimal,
  titluAnuntAnimal,
  id,
}: FormulareTrimiseProps) => {
  const styles = {
    formularProfil: 'formulare-trimise',
    container: 'formulare-trimise__container',
    descriere: 'formulare-trimise__container__descriere',
    descrierelabel: 'formulare-trimise__container__descriere__label',
    titlu: 'formulare-trimise__container__titlu',
    formularPrimit: 'formulare-trimise__container__formular-primit',
    informatii: 'formulare-trimise__container__formular-primit__informatii',
    label: 'formulare-trimise__container__formular-primit__informatii__label',
    status: 'formulare-trimise__container__status',
  };
  return (
    <div className={styles.formularProfil}>
      <div className={styles.container}>
        <div className={styles.titlu}>
          <p>Titlu anunt: {titluAnuntAnimal} </p>
        </div>
        <div className={styles.formularPrimit}>
          <img src={`https://res.cloudinary.com/dssizmzir/image/upload/${imagineAnimal}`} />
          <div className={styles.informatii}>
            <p><span className={styles.label}>Nume stăpân:</span> {numeStapanAnimal}</p>
            <p><span className={styles.label}>Email stăpan:</span> {emailStapanAnimal}</p>
            <p><span className={styles.label}>Număr de telefon stăpân:</span> {nrTelStapanAnimal}</p>
          </div>
        </div>
        <div className={styles.descriere}>
          <p><span className={styles.label}>Descriere conditii:</span> {descriereConditii}</p>
        </div>
        <div className={styles.status}>
          <p>Status: {statusFormular}</p>
          {statusFormular === 'Acceptat' && <FaCheckCircle color='green' />}
          {statusFormular === 'Refuzat' && <FaTimesCircle color='red' />}
          {statusFormular === 'In asteptare' && <FaClock color='gray' />}
        </div>
      </div>
    </div>
  );
};
