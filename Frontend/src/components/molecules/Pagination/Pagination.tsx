import './Pagination.scss';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai';

const Pagination: React.FC<any> = ({
  path,
  postsPerPage,
  totalPosts,
  paginate,
  currentPage,
  category,
}) => {
  const styles = {
    buttonPagination: 'button-pagination',
    pagesTextClass: 'button-pagination__text',
  };

  const navigate = useNavigate();
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return Number(pageNumbers.length) > 1 ? (
    <div className={styles.buttonPagination}>
      <button
        onClick={() => {
          if (currentPage > 1) paginate(currentPage - 1);
          if (category) {
            navigate(path + `/${category}`);
          } else {
            navigate(path);
          }
        }}
      >
        <AiOutlineArrowLeft
          style={{ fontSize: '2.3rem', color: '#FFF', border: '2px solod #000' }}
        />
      </button>

      <p className={styles.pagesTextClass}>
        {currentPage} of {pageNumbers.length}
      </p>
      <button
        onClick={() => {
          if (currentPage < pageNumbers.length) {
            paginate(currentPage + 1);
            if (category) {
              navigate(path + `/${category}`);
            } else {
              navigate(path);
            }
          }
        }}
      >
        <AiOutlineArrowRight
          style={{ fontSize: '2.3rem', color: '#FFF', border: '#000', marginTop: '0.5rem' }}
        />
      </button>
    </div>
  ) : (
    <div></div>
  );
};

export default Pagination;

