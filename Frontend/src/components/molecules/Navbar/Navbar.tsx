import React, { ReactNode, useState, useRef, useEffect } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './Navbar.scss';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import { NavbarLinks } from '../../../mockData/mockDataLinks';
import clsx from 'clsx';
import { TiThMenuOutline } from 'react-icons/ti';
import { Accordion } from '../../atoms/Accordion/Accordion';
import { AccordionItem } from '../../atoms/Accordion/AccordionItem/AccordionItem';

import { deleteCookie } from '../../../utils/utilsFunctions';
import { TOKEN } from '../../../utils/Constants';
import { AppDispatch, AppThunkDispatch, RootState } from '../../../redux/store';
import { useSelector, useDispatch } from 'react-redux';
import { setToast } from '../../../redux/features/toastSlice';
import { PetFriends } from '../../../assets/imagePaths';
import { CgProfile } from 'react-icons/cg';
import {
  ADMIN_PATH,
  LOGIN_PATH,
  REGISTER_PATH,
} from '../../../Routes/routesPath';
import { LOGGED_OUT } from '../../../utils/toastMessages';
import { getUser } from '../../../redux/features/userSlice';
const linksString = JSON.stringify(NavbarLinks);
const links1 = JSON.parse(linksString).links1;
const links2 = JSON.parse(linksString).links2;

type NavbarProps = {
  logo1?: boolean;
  logo2?: boolean;
  label: string;
  href: string;
  shop?: ReactNode;
  search?: ReactNode;
};

export const Navbar = ({ shop, search }: NavbarProps) => {
  const [isNavExpanded, setIsNavExpanded] = useState(false);
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    document.addEventListener('click', hideOnClickOutside, true);
  }, []);
  const hideOnClickOutside = (e: { target: any }) => {
    if (ref.current && !ref.current.contains(e.target)) {
      setIsNavExpanded(false);
    }
  };
  const { isConnected, userData } = useSelector((state: RootState) => state.userState);
  const dispatch = useDispatch<AppThunkDispatch>();

  useEffect(() => {
    if (!userData) {
      dispatch(getUser());
    }
  }, [userData, dispatch]);

  const styles = {
    navbarButton: 'button-navbar',
    icon: clsx(shop && 'icon__1', search && 'icon__2'),
    navbarFirst: clsx('navbar', 'navbar__first'),
    navbarSecond: clsx('navbar', 'navbar__second'),
    logoContainer: 'logo__container',
    text: 'navbar__header_text',
    acordion: 'acordion',
    linkItems: 'link__items',
  };

  const Links1: React.FC<{ links1: NavbarProps[] }> = ({ links1 }) => {
    const dispatch = useDispatch<AppDispatch>();

    const logoutHandler = () => {
      deleteCookie(TOKEN);
      const toastData = { message: LOGGED_OUT, success: true };
      dispatch(setToast(toastData));
      const timeout = setTimeout(() => {
        window.location.reload();
      }, 3000);
      return () => {
        clearTimeout(timeout);
      };
    };

    const styles = {
      linkContainer: 'link__container_first',
      link: 'link__first',
    };
    return (
      <div className={styles.linkContainer}>
        <ul className={styles.linkContainer}>
          {links1.map((link: NavbarProps) => {
            return (
              <li key={link.href} className={styles.link}>
                <NavLink
                  style={({ isActive }) => (isActive ? { color: '#ad9c8e' } : {})}
                  to={link.href}
                  end
                >
                  {link.label}
                </NavLink>
              </li>
            );
          })}
          {isConnected ? (
            <li onClick={logoutHandler} className={styles.link}>
              <NavLink
                style={({ isActive }) => (isActive ? { color: '#ad9c8e' } : {})}
                to={LOGIN_PATH}
                end
              >
                Logout
              </NavLink>
            </li>
          ) : (
            <li className={styles.link}>
              <NavLink
                style={({ isActive }) => (isActive ? { color: '#ad9c8e' } : {})}
                to={LOGIN_PATH}
                end
              >
                Login
              </NavLink>
            </li>
          )}
        </ul>
      </div>
    );
  };
  const Links2: React.FC<{ links2: NavbarProps[] }> = ({ links2 }) => {
    const styles = {
      linkContainer: 'link__container_second',
      link: 'link__second',
      linkActive: 'link__second--active',
    };
    return (
      <ul className={styles.linkContainer}>
        {links2.map((link: NavbarProps) => {
          return (
            <li key={link.href} className={styles.link}>
              <NavLink
                className={({ isActive }) => (isActive ? styles.linkActive : '')}
                to={link.href}
                end
              >
                {link.label}
              </NavLink>
            </li>
          );
        })}
      </ul>
    );
  };

  return (
    <div>
      <div className={styles.navbarFirst}>
        <div className={styles.logoContainer}>
        </div>
        <Links1 links1={links1} />
      </div>
      <div className={styles.navbarSecond}>
        <div className={styles.logoContainer}>{<img src={PetFriends} />}</div>
        <Links2 links2={links2} />
        {!isConnected ? (
          <div className={styles.navbarButton}>
            <Link to={REGISTER_PATH}>
              <Button
                type={CustomTypes.ButtonTypes.PRIMARY}
                label={'User Profile'}
                backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
              />
            </Link>
          </div>
        ) : userData?.role === 'user' ? (
          <div className={styles.navbarButton}>
            <a href='/user-profile'>
              <Button
                type={CustomTypes.ButtonTypes.PRIMARY}
                label={userData?.firstname + ' ' + userData?.lastname}
                backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
              />
            </a>
          </div>
        ) : (
          <div className={styles.navbarButton}>
            <a href={ADMIN_PATH}>
              <Button
                type={CustomTypes.ButtonTypes.PRIMARY}
                label={'Dashboard'}
                backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
              />
            </a>
          </div>
        )}
        <CgProfile className='navbar__profileIcon' onClick={() => console.log('')} />
        <button
          className='hamburger'
          onClick={() => {
            setIsNavExpanded((isNavExpanded) => !isNavExpanded);
          }}
        >
          <TiThMenuOutline fontSize={'2.2rem'} />
        </button>
      </div>

      {isNavExpanded && (
        <div ref={ref} className={styles.acordion}>
          <Accordion backgroundColor={CustomTypes.AccordionBackground.DARK_BLUE}>
            <AccordionItem title='Opțiuni principale'>
              <Links1 links1={links1} />
            </AccordionItem>
            <AccordionItem title='Opțiuni secundare'>
              <Links2 links2={links2} />
            </AccordionItem>
          </Accordion>
        </div>
      )}
    </div>
  );
};
