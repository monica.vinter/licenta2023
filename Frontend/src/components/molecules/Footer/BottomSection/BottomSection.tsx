import './BottomSection.scss';
import { MediaLinks } from '../../../../mockData/mockDataLinks';

interface BottomSectionProps {
    bottomFooterLinks: { href: string; label: string; }[];
};

const BottomSection: React.FC<BottomSectionProps> = ({ bottomFooterLinks }) => {
    const styles = {
        bottomSection: 'footer__bottom-section',
        Title: 'footer__bottom-section__title',
        formTitle: 'footer__bottom-section__title',
        follow: 'footer__bottom-section__follow',
        mediaTitle: 'footer__bottom-section__follow__title',
        mediaText: 'footer__bottom-section__follow__text',
        mediaIcons: 'footer__bottom-section__follow__icons',
    };
    return (
        <div className={styles.bottomSection}>
            <h1 className={styles.Title}>©PetFriends</h1>
            <div className={styles.follow}>
                <h1 className={styles.formTitle}>Urmărește-ne:</h1>
                <p className={styles.mediaText}></p>
                <div className={styles.mediaIcons}>
                    {MediaLinks.map((media) => {
                        return (
                            <a href={media.href} key={media.label}>
                                {media.icon}
                            </a>
                        );
                    })}
                </div>
            </div>
        </div>
    )
};

export default BottomSection;