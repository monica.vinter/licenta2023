import BottomSection from './BottomSection/BottomSection';
import { FooterLinks } from '../../../mockData/mockDataLinks';
import './Footer.scss';
import { NewsLetterForm } from '../NewsLetterForm/NewsLetterForm';
import * as MockData from '../../../mockData/mockDataComponents';

const linksString = JSON.stringify(FooterLinks);
const defaultBottomLinks: { href: string; label: string; }[] = JSON.parse(linksString).bottomFooterLinks;

interface FooterProps {
  footerMenuLinks?: { category: string, items: { href: string; label: string; }[] }[];
  bottomFooterLinks?: { href: string; label: string; }[];
};

export const Footer: React.FC<FooterProps> = ({
  bottomFooterLinks = defaultBottomLinks,
}) => {
  const styles = {
    footer: 'footer',
    footerMenuContainer: 'footer__menu-container',
    footerMenu: 'footer__menu',
    bottomSectionContainer: 'footer__bottom-container',
  };


  return (
    <div className={styles.footer}>
      <div className={styles.footerMenuContainer}>
        <div className={styles.footerMenu}>
          <NewsLetterForm {...MockData.NewssLetterFormData} />
        </div>
      </div>
      <div className={styles.bottomSectionContainer}>
        <BottomSection bottomFooterLinks={bottomFooterLinks} />
      </div>
    </div>
  );
};