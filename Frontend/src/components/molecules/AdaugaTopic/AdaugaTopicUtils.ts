import { FormRequiredInputs, FormFormatInputs } from '../../../mockData/mockDataForms';

export const DateTopic = {
    titlu: {
        required: FormRequiredInputs.titlu,
        format: FormFormatInputs.titlu,
    },
};
