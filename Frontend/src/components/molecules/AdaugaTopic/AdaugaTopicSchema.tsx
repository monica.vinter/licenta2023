import * as yup from 'yup';

interface AdaugaTopicSchemaProps {
  titlu: {
    required: string;
    format: string;
  };
}

const adaugaTopicSchema = (validationMsgs: AdaugaTopicSchemaProps) => {

  const { titlu } = validationMsgs;

  return yup.object({
    titlu: yup
      .string()
      .trim()
      .required(titlu.required)
  })
};

export default adaugaTopicSchema;