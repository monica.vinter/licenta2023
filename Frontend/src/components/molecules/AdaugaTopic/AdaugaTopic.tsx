import { useFormik } from 'formik';
import { Input } from '../../atoms/Input/Input';
import { Button } from '../../atoms/Button/Button';
import * as CustomTypes from '../../../utils/customTypes';
import {
  FormButtonsLabel,
  FormInputLabels,
  FormInputPlaceholders,
  FormInformation,
  FormInputNames,
} from '../../../mockData/mockDataForms';
import adaugaTopicSchema from './AdaugaTopicSchema';
import { DateTopic } from './AdaugaTopicUtils';
import './AdaugaTopic.scss';
import { useDispatch, useSelector } from 'react-redux';
import { ADAUGARETOPIC_SUCCESS, SERVER_ERROR } from '../../../utils/toastMessages';
import { setToast } from '../../../redux/features/toastSlice';
import { adaugaTopic } from '../../../API/topicuriAPI';
import { AppThunkDispatch, RootState } from '../../../redux/store';
import { LOGIN_PATH } from '../../../Routes/routesPath';
import { useNavigate } from 'react-router-dom';
import { getTopicuri } from '../../../redux/features/topicSlice';

export const AdaugaTopic: React.FunctionComponent = () => {
  const dispatch = useDispatch<AppThunkDispatch>();
  const { isConnected, userData } = useSelector((state: RootState) => state.userState);
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      titlu: '',
    },

    onSubmit: async function (values) {
      if (isConnected) {
        console.log(values)
        const toastErrorData = { message: SERVER_ERROR, success: false };
        try {
          const DateTopic = {
            titlu: values.titlu,
            user: userData,
          };
          console.log(DateTopic.titlu);
          const data = await adaugaTopic(DateTopic);
          dispatch(getTopicuri());
          if (data) {
            const toastData = { message: ADAUGARETOPIC_SUCCESS, success: true };
            dispatch(setToast(toastData));
          } else {
            dispatch(setToast(toastErrorData));
          }
        } catch (error) {
          dispatch(setToast(toastErrorData));
        }
      }
      else {
        navigate(LOGIN_PATH);
      }
    },

    validationSchema: adaugaTopicSchema(DateTopic),

  });

  const styles = {
    formContainer: 'adauga-topic-form__container',
    singleInputRow: 'adauga-topic-form__single-input-row',
    label: 'adauga-topic-form__label',
    submitButton: 'adauga-topic-form__submit-button',
  };

  return (
    <div className={styles.formContainer}>
      <form>
        <h1> {FormInformation.adaugaTopicHere} </h1>
        <div className={styles.singleInputRow}>
          <Input
            id={FormInputNames.titlu}
            name={FormInputNames.titlu}
            label={FormInputLabels.titlu}
            type={CustomTypes.InputTypes.TEXT}
            styleType={CustomTypes.InputStyleTypes.SECONDARY}
            placeholder={FormInputPlaceholders.titlu}
            required
            error={
              formik.touched.titlu && formik.errors.titlu
                ? formik.errors.titlu
                : undefined
            }
            value={formik.values.titlu}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>

        <div className={styles.submitButton}>
          <Button
            type={CustomTypes.ButtonTypes.PRIMARY}
            label={FormButtonsLabel.adaugaTopic}
            backgroundColor={CustomTypes.BackgroundThreeColor.MEDIUM}
            onClick={() => formik.handleSubmit()}
          />
        </div>
      </form>
    </div>
  );
};