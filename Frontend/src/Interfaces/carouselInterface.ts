import {ReactNode} from 'react';

export interface CarouselInterface{
    children:ReactNode,
    label?:string,
    type?:string,
    extraLargeDesktopItems?: number;
    itemsToShowExtraLargeDesktop?: number;
    largeDesktopItems?: number;
    itemsToShowLargeDesktop?: number;
    desktopItems?: number;
    itemsToShowDesktop?: number;
    largeTabletItems?: number;
    itemsToShowLargeTablet?: number;
    tabletItems?: number;
    itemsToShowTablet?: number;
    mobileItems?: number;
    itemsToShowMobile?: number;
}