import { userRegisterData } from './userInterfaces';

export interface DateAnimale {
  titluAnunt: string;
  categorie: string;
  judet: string;
  rasa:  string;
  varsta:  string;
  gen:  string;
  culoare: string;
  imagini:  string[];
  adoptat:  boolean;
  descriere: string;
  emailStapan?: string;
  stapan?: userRegisterData;
  _id: string;
  statusAnunt?: string;
  numarTelefonStapan?: string;
  numeStapan?: string;
  imagineProfilStapan?: string;
}

export interface PostDateAnimale {
  titluAnunt: string;
  categorie: string;
  judet: string;
  rasa:  string;
  varsta:  string;
  gen:  string;
  culoare: string;
  imagini:  string;
  descriere: string;
  emailStapan?: string;
  stapan?: userRegisterData | null;
  statusAnunt?: string;
  numarTelefonStapan?: string;
  numeStapan?: string;
  imagineProfilStapan?: string;
}


export interface AnimaleInitialStateInterface {
  loading: boolean;
  animale: DateAnimale[];
  error: string | null;
  filter: any;
}
