export interface ToastInitialStateInterface {
  message: string | null;
  success: boolean | null;
}
