import { userRegisterData } from './userInterfaces';
import { DateFormular } from './formularInterface';

export interface FormulareData {
  descriereConditii: string;
  numeViitorStapan?: string,
  emailViitorStapan?: string,
  nrTelViitorStapan?: string,
  numeStapanAnimal?: string;
  emailStapanAnimal?: string;
  nrTelStapanAnimal?: string;
  user?: userRegisterData;
  statusFormular?: string;
  idAnimal?: string;
  _id?: string;
  imagineAnimal?: string;
}

export interface FormularStateInterface {
  loading: boolean;
  formulare: FormulareData[];
  error: string | null;
}
