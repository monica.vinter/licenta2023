export interface NewsInterface {
    title: string;
    body: string;
    image: string;
    author: string;
    date: Date;
}

export interface NewsInitialStateInterface {
    loading: boolean;
    news: NewsInterface[];
    error: string | null;
}