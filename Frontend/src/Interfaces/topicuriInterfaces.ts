import { userRegisterData } from './userInterfaces';

export interface DateTopicuri {
  titlu: string;
  user?: userRegisterData;
  _id: string;
  dataPublicarii: Date;
  numarComentarii: Number;
}

export interface PostDateTopicuri {
  titlu: string;
  user?: userRegisterData | null;
}


export interface TopicuriInitialStateInterface {
  loading: boolean;
  topicuri: DateTopicuri[];
  error: string | null;
  topicData: DateTopicuri | null;
}
