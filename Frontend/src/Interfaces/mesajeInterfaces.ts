import { userRegisterData } from './userInterfaces';
import { DateTopicuri } from './topicuriInterfaces';


export interface DateMesaje {
  mesaj: string;
  user?: userRegisterData;
  numeUtilizator?: string;
  dataPublicarii: Date;
  topic?: string;
  imagineProfilUtilizator?: string;
  _id: string;
}

export interface PostDateMesaje {
  mesaj: string;
  user?: userRegisterData | null;
  topic?: string;
  imagineProfilUtilizator?: string;
  numeUtilizator?: string;
}


export interface MesajeInitialStateInterface {
  loading: boolean;
  mesaje: DateMesaje[];
  error: string | null;
}
