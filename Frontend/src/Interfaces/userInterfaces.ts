export interface userRegisterData {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  phone: string;
  birthday: string;
  role?: string;
  imagine?: string;
  _id?: string;
}
export interface userLoginData {
  email: string;
  password: string;
}

export interface SecurityKeyData {
  email: string;
}
export interface changeForgotPassowordInterface {
  email: string;
  key: string;
  password: string;
}


export interface changeResetPasswordInterface {
  password: string;
}

export interface initialStateInterface {
  loading: boolean;
  error: string | null;
  isConnected: boolean;
  userData: userRegisterData | null;
}
 