import { DateAnimale } from './animaleInterfaces';

export interface DateFormular {
    animal: DateAnimale;
}
   
export interface FormularInitialStateInterface {
    loading: boolean;
    formulare: DateFormular[];
    error: string | null;
}