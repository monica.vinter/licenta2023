

import PaginaAdoptaBackground from '../assets/PaginaAdoptaBackground.png';
import PaginaFormSpreAdoptie from '../assets/PaginaFormSpreAdoptie.png';
import PaginaFormRegister from '../assets/PaginaFormRegister.png';

import HomeCarousel1 from '../assets/HomeCarousel1.png';
import HomeCarousel2 from '../assets/HomeCarousel2.png';
import HomeCarousel3 from '../assets/HomeCarousel3.png';
import HomeCarousel4 from '../assets/HomeCarousel4.png';
import HomeCarousel5 from '../assets/HomeCarousel5.png';
import Donate from '../assets/Donate.png';
import AsociatiaHope from '../assets/AsociatiaHope.png'
import AsociatiaMariuta from '../assets/AsociatiaMariuta.png'
import SidebarDashboard from '../assets/sidebarDashboard.png';
import SidebarAnunturi from '../assets/sidebarAnunturi.png';

import AdaugaAnuntAdmin from '../assets/AdaugaAnuntAdmin.png';
import AnunturiAcceptate from '../assets/AnunturiAcceptate.png';
import AnunturiRefuzate from '../assets/AnunturiRefuzate.png';
import AnunturiAsteptare from '../assets/AnunturiAsteptare.png';
import BackgroundAdmin from '../assets/BackgroundAdmin.png';
import ImagineForum from '../assets/ImagineForum.png';
import PaginaAnimalAdminBackground from '../assets/PaginaAnimalAdmin.png';
import SfaturiCaini from '../assets/SfaturiCaini.png';
import SfaturiPisici from '../assets/SfaturiPisici.png';
import SfaturiActivitateCaini from '../assets/SfaturiActivitateCaini.png';
import SfaturiHranaCaini from '../assets/SfaturiHranaCaini.png';
import SfaturiMediuCaini from '../assets/SfaturiMediuCaini.png';
import SfaturiActivitatePisici from '../assets/SfaturiActivitatePisici.png';
import SfaturiHranaPisici from '../assets/SfaturiHranaPisici.png';
import SfaturiMediuPisici from '../assets/SfaturiMediuPisici.png';

import PetFriends from '../assets/Logo.png';
import Icon from '../assets/Icon.png';

import ImagineFormularIcon from '../assets/ImagineFormularIcon.png';;
import CategorieAltele from '../assets/CategorieAltele.png';
import CategorieCaini from '../assets/CategorieCaini.png';
import CategoriePisici from '../assets/CategoriePisici.png';
import BackgroundCaini from '../assets/BackgroundCaini.png';
import ImaginePisica1 from '../assets/ImaginePisica1.png';
import ImaginePisica2 from '../assets/ImaginePisica2.png';
import ImaginePisica3 from '../assets/ImaginePisica3.png';
import ImagineCaine1 from '../assets/ImagineCaine1.png';
import ImagineCaine2 from '../assets/ImagineCaine2.png';
import ImagineCaine3 from '../assets/ImagineCaine3.png';
import ImagineAltele1 from '../assets/ImagineAltele1.png';
import ImagineAltele2 from '../assets/ImagineAltele2.png';
import ImagineAltele3 from '../assets/ImagineAltele3.png';

import BasicProfilePic from '../assets/BasicProfilePic.png';
import SidebarNews from '../assets/sidebarNews.png';

export const ImaginiAnimale: any = {

  Pisica1: ImaginePisica1,
  Pisica2: ImaginePisica2,
  Pisica3: ImaginePisica3,

  Caine1: ImagineCaine1,
  Caine2: ImagineCaine2,
  Caine3: ImagineCaine3,

  Altele1: ImagineAltele1,
  Altele2: ImagineAltele2,
  Altele3: ImagineAltele3,

  CategoriePisici: CategoriePisici


};

import checkIcon from '../assets/checkIcon.png';

export {
  PaginaFormSpreAdoptie,
  PaginaFormRegister,
  PaginaAdoptaBackground,

  HomeCarousel1,
  HomeCarousel2,
  HomeCarousel3,
  HomeCarousel4,
  HomeCarousel5,
  Donate,
  SidebarAnunturi,
  SidebarDashboard,
  AdaugaAnuntAdmin,
  AnunturiAcceptate,
  AnunturiRefuzate,
  AnunturiAsteptare,
  AsociatiaHope,
  AsociatiaMariuta,
  BackgroundAdmin,
  ImagineFormularIcon,
  CategorieAltele,
  CategorieCaini,
  CategoriePisici,
  ImagineForum,
  PaginaAnimalAdminBackground,
  SfaturiCaini,
  SfaturiActivitateCaini,
  SfaturiHranaCaini,
  SfaturiMediuCaini, 
  SfaturiPisici,
  SfaturiActivitatePisici,
  SfaturiHranaPisici,
  SfaturiMediuPisici, 
  ImaginePisica1,
  ImaginePisica2,
  ImaginePisica3,
  ImagineAltele1,
  ImagineAltele2,
  ImagineAltele3,
  ImagineCaine1,
  ImagineCaine2,
  ImagineCaine3,
  BackgroundCaini,
  PetFriends,
  Icon,
  checkIcon,
  BasicProfilePic,
  SidebarNews,
};
