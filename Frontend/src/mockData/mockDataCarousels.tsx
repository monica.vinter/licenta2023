import * as CustomTypes from '../utils/customTypes';
 
 
import {

  HomeCarousel1,
  HomeCarousel2,
  HomeCarousel3,
  HomeCarousel4,
  HomeCarousel5,

  ImaginePisica1,
  ImaginePisica2,
  ImaginePisica3,
  ImagineCaine1,
  ImagineCaine2,
  ImagineCaine3,
  ImagineAltele1,
  ImagineAltele2,
} from '../assets/imagePaths';

export const StageCarouselShopPage = {
  title: 'St Andrews Links Shop',
  buttonLabel: 'Shop Now',
  textAreaTitle: 'The Castle Course Collection',
  textAreaContent:
    'A distinctive check, which boasts a striking scarlet red stripe alongside grey, black and white tones. ',
  disableTime: true,
};

export const SliderCarouselImages = [
  {
    title: 'Adoptă',
    urls: HomeCarousel1,
  },
  {
    title: 'Fă-ți un prieten',
    urls: HomeCarousel5,
  },
  {
    title: 'Nu mai cumpăra',
    urls: HomeCarousel2,
  },
  {
    title: 'Oferă-le o casă',
    urls: HomeCarousel3,
  },
  {
    title: 'Adoptă acum',
    urls: HomeCarousel4,
  },
];



export const MostPopularModels = [
  {
    image: ImaginePisica1,
  },
  {
    image: ImaginePisica2,
  },
  {
    image: ImaginePisica3,
  },
  {
    image: ImagineCaine1,
  },
  {
    image: ImagineCaine2,
  },
  {
    image: ImagineCaine3,
  },
  {
    image: ImagineAltele1,
  },
  {
    image: ImagineAltele2,
  },
];

export const OurCoursesData = {
  label: 'Our Courses',
  type: CustomTypes.CarouselTypes.TEXT,
  extraLargeDesktopItems: 4,
  itemsToShowExtraLargeDesktop: 10,
  largeDesktopItems: 4,
  itemsToShowLargeDesktop: 0,
  desktopItems: 3,
  itemsToShowDesktop: 40,
  largeTabletItems: 2,
  itemsToShowLargeTablet: 0,
  tabletItems: 2,
  itemsToShowTablet: 0,
  mobileItems: 1,
  itemsToShowMobile: 0,
};

export const MoreNewsData = {
  label: 'More News',
  type: CustomTypes.CarouselTypes.TITLE,
  extraLargeDesktopItems: 4,
  itemsToShowExtraLargeDesktop: 10,
  largeDesktopItems: 4,
  itemsToShowLargeDesktop: 0,
  desktopItems: 3,
  itemsToShowDesktop: 40,
  largeTabletItems: 2,
  itemsToShowLargeTablet: 0,
  tabletItems: 2,
  itemsToShowTablet: 0,
  mobileItems: 1,
  itemsToShowMobile: 0,
};

export const LatestNewsData = {
  label: 'Latest News',
  type: CustomTypes.CarouselTypes.TITLE,
  extraLargeDesktopItems: 4,
  itemsToShowExtraLargeDesktop: 10,
  largeDesktopItems: 4,
  itemsToShowLargeDesktop: 0,
  desktopItems: 3,
  itemsToShowDesktop: 40,
  largeTabletItems: 2,
  itemsToShowLargeTablet: 0,
  tabletItems: 2,
  itemsToShowTablet: 0,
  mobileItems: 1,
  itemsToShowMobile: 0,
};

export const YourStAndrewsLinksData = {
  label: '#YourStAndrewsLinks',
  type: CustomTypes.CarouselTypes.SIMPLE,
  extraLargeDesktopItems: 4,
  itemsToShowExtraLargeDesktop: 10,
  largeDesktopItems: 4,
  itemsToShowLargeDesktop: 0,
  desktopItems: 3,
  itemsToShowDesktop: 40,
  largeTabletItems: 2,
  itemsToShowLargeTablet: 0,
  tabletItems: 2,
  itemsToShowTablet: 0,
  mobileItems: 1,
  itemsToShowMobile: 0,
};

export const ComponentsCarouselVideo = {
  extraLargeDesktopItems: 5,
  itemsToShowExtraLargeDesktop: 0,
  largeDesktopItems: 5,
  itemsToShowLargeDesktop: 0,
  desktopItems: 4,
  itemsToShowDesktop: 40,
  largeTabletItems: 3,
  itemsToShowLargeTablet: 50,
  tabletItems: 3,
  itemsToShowTablet: 0,
  mobileItems: 2,
  itemsToShowMobile: 0,
};

export const MostPopularModelsData = {
  label: 'Ultimele adăugări',
  type: CustomTypes.CarouselTypes.TEXT,
  extraLargeDesktopItems: 4,
  itemsToShowExtraLargeDesktop: 10,
  largeDesktopItems: 4,
  itemsToShowLargeDesktop: 0,
  desktopItems: 3,
  itemsToShowDesktop: 40,
  largeTabletItems: 2,
  itemsToShowLargeTablet: 0,
  tabletItems: 2,
  itemsToShowTablet: 0,
  mobileItems: 1,
  itemsToShowMobile: 0,
};
