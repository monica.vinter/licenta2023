import { FormRequiredInputs, FormFormatInputs } from '../mockData/mockDataForms';
import { AiFillCheckCircle, AiFillCloseCircle, AiFillFacebook, AiFillFileAdd, AiFillTwitterCircle, AiOutlineFall, AiOutlineLoading3Quarters } from 'react-icons/ai';
import {IoIosArrowDown, IoIosArrowUp } from 'react-icons/io';
import { 
  ADMIN_PATH,
  PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH,
  PAGINA_ADMIN_ANUNTURI_REFUZATE_PATH,
  PAGINA_ADMIN_ANUNTURI_PATH,
  DONEAZA_PATH,
  TRIMITESPREADOPTIE_PATH,
  ADOPTA_PATH,
  PAGINA_ADMIN_ADAUGA_ANUNT,
  SFATURI_PATH,
  USERPROFILE_PATH,
} from '../Routes/routesPath';
import { SidebarDashboard, 
        SidebarAnunturi,
} from '../assets/imagePaths';
import { CgProfile } from 'react-icons/cg';


export const NavbarLinks = {
  links1: [
    {
      label: 'Forum',
      href: '/Forum',
    },
  ],
  links2: [
    {
      label: 'Adoptă',
      href: ADOPTA_PATH,
    },
    {
      label: 'Trimite spre adopție',
      href: TRIMITESPREADOPTIE_PATH,
    },
    {
      label: 'Donează',
      href: DONEAZA_PATH,
    },
    {

      label: 'Sfaturi',
      href: SFATURI_PATH,
    },
  ],
};

export const FooterLinks = {
  FooterLinks: [
    {
      label: '© PetFriends',
    },
  ],
};

export const EmailData = {
  emailAddress: {
    required: FormRequiredInputs.emailAddress,
    format: FormFormatInputs.emailAddress,
  },
};

export const MediaLinks = [
  {
    label: 'Facebook',
    href: 'https://www.facebook.com/',
    icon: <AiFillFacebook />,
  },
  {
    label: 'Twitter',
    href: 'https://twitter.com/?lang=en',
    icon: <AiFillTwitterCircle />,
  },
];

export const SidebarLinks = [
  {
    label: 'Dashboard',
    icon: SidebarDashboard,
    iconClosed: <IoIosArrowDown size={'2rem'}/>,
    iconOpened: <IoIosArrowUp size={'2rem'}/>,
    subNav: [
      {
        label: 'Vizualizează statistici ',
        path: ADMIN_PATH,
        icon: <AiOutlineFall size={'2.5rem'}/>
      },
      {
        label: 'Vizualizează pagină de profil ',
        path: USERPROFILE_PATH,
        icon: <CgProfile size={'2.5rem'}/>
      },
    ]
  },
  {
    label: 'Administrarea anunțurilor',
    icon: SidebarAnunturi,
    iconClosed: <IoIosArrowDown size={'2rem'}/>,
    iconOpened: <IoIosArrowUp size={'2rem'}/>,
    subNav: [
      {
        label: 'Adaugă anunț',
        path: PAGINA_ADMIN_ADAUGA_ANUNT,
        icon: <AiFillFileAdd size={'1.8rem'}/>,
      },
      {
        label: 'Vizualizează anunțuri in asteptare',
        path: PAGINA_ADMIN_ANUNTURI_PATH,
        icon: <AiOutlineLoading3Quarters size={'1.8rem'}/>,
      },
      {
        label: 'Vizualizează anunturi acceptate',
        path: PAGINA_ADMIN_ANUNTURI_ACCEPTATE_PATH,
        icon: <AiFillCheckCircle size={'1.8rem'}/>,
      },
      {
        label: 'Vizualizează anunturi refuzate',
        path: PAGINA_ADMIN_ANUNTURI_REFUZATE_PATH,
        icon: <AiFillCloseCircle size={'1.8rem'}/>,
      }
    ]
  },
];