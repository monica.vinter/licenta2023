import * as CustomTypes from '../utils/customTypes';
import { AiOutlineSearch } from 'react-icons/ai';

import { HiOutlineShoppingBag } from 'react-icons/hi';
import { BsSearch } from 'react-icons/bs';
import {
  ACASA_PATH,
  FORUM_PATH,
  SFATURI_PATH
} from '../Routes/routesPath';
import {
  ImaginePisica1,
  ImagineAltele1,
  ImagineAltele2,
  CategorieCaini,
  CategoriePisici,
  CategorieAltele,
  AsociatiaMariuta,
  AsociatiaHope,
} from '../assets/imagePaths';

export const NavabarSimpleData = {
  header: true,
  logo1: true,
  label: '',
  href: '',
};

export const NavabarIconsData = {
  label: '',
  href: '',
  shop: <HiOutlineShoppingBag fontSize={'2.2rem'} />,
  search: <BsSearch fontSize={'2rem'} />,
};

export const NavabarLogoData = {
  logo2: true,
  label: '',
  href: '',
  shop: <HiOutlineShoppingBag fontSize={'2.2rem'} />,
  search: <BsSearch fontSize={'2rem'} />,
};

export const InputData = {
  label: 'Input',
  type: CustomTypes.InputTypes.PASSWORD,
  placeholder: 'Text',
  error: 'Error message here!',
};

export const SliderData = {
  text: 'Slider',
  backgroundColor: CustomTypes.BackgroundThreeColor.MEDIUM,
  index: 0,
  onChange: (value: number) => {
    console.log(value);
  },
};

export const ButtonGroupData = {
  type: CustomTypes.ButtonGroupTypes.TEE,
  afterClick: (label: string) => {
    console.log(label);
  },
  buttons: [
    { label: 'One', index: 1 },
    { label: 'Two', index: 2 },
    { label: 'Three', index: 3 },
  ],
};

export const PortalAdoptiiDate = {
  title: 'Situația în România',
  text: 'Potrivit unui raport din 2019 al organizației non-profit "Fundația pentru Protecția Animalelor și a Naturii" (FPAN), există aproximativ 2,5 milioane de câini fără stăpân în România, dintre care 75% trăiesc în afara orașelor.',
  text1: 'Situația câinilor și pisicilor fără stăpân în România este una dificilă. Aceste animale se confruntă cu numeroase probleme, precum lipsa hranei și a adăpostului, îngrijire precară, precum și abuz din partea oamenilor.',
  text2: 'În plus, suprapopularea animalelor fără stăpân este o problemă majoră în România. Astfel, există riscul creșterii numărului de câini și pisici fără stăpân în orașe și zone rurale, ceea ce duce la situații de pericol pentru sănătatea publică și de asemenea cresc costurile administrative pentru autorități în îngrijirea acestora.',
  text3:'Cu toate acestea, problemele rămân mari, iar eforturile pentru protejarea și îngrijirea animalelor fără stăpân continuă să fie o provocare în România.',
  label: 'Adoptă',
  type: CustomTypes.ButtonTypes.PRIMARY,
};
export const NewssLetterFormData = {
  buttonType: CustomTypes.ButtonTypes.PRIMARY,
  buttonLabel: 'Abonează-te',
  inputPlaceholder: 'Adresa de email',
  formText: 'Fii primul care primește noile anunțuri',
  formTitle: 'Înscrie-te pentru anunțuri noi',
  mediaTitle: 'Urmărește-ne',
};



export const PaginaDoneazaDate = {
  breadCrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: '',
        label: ' Donează',
        active: true,
      },
    ],
  },
  header: {
    title: 'Donează acum!',
    text: 'Alege asociația la care dorești să donezi',
  },
  content: {
    Mariuta: {
      type: CustomTypes.ContentCardTypes.CONTENT,
      border: CustomTypes.PhotoCardBorders.BIG,
      tag: 'ASOCIAȚIA pentru',
      title: '"Protecția animalelor MĂRIUȚA"',
      text: 'Asociația pentru "Protectia Animalelor Măriuța", se află în localitatea Desa. județul Dolj, iar președinta Asociației Măriuța se numește Daniela Popovici. În 2.5 ani această asociație a reușit să salveze 600 de animale de la ananghie.',
      titluDonatie: 'Transfer bancar',
      banca: 'BRD',
      ibanRON: 'RO63BRDE170SV37819291700',
      ibanEURO: 'RO18BRDE170SV40406211700',
      codSwift: 'BRDEROBU',
      imagine: AsociatiaMariuta,
    },
    Hope: {
      type: CustomTypes.ContentCardTypes.CONTENT,
      border: CustomTypes.PhotoCardBorders.BIG,
      tag: 'ASOCIAȚIA pentru',
      title: '"Protecția animalelor HOPE"',
      text: 'Asociația a luat ființă în Mai 2016. Adăpostul, situat în București, a ajuns să aibă constant în grija în jur de 150  de animăluțe cu eforturi, sacrificii și costuri lunare',
      right: true,
      titluDonatie: 'Transfer bancar',
      banca: 'ING',
      ibanRON: 'RO63INGB0000999905937826',
      ibanEURO: 'RO63INGB0000999905937826',
      codSwift: 'NGBROBU',
      imagine: AsociatiaHope,
    },
  },
};

export const PaginaForumDate = {
  breadCrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: '',
        label: ' Forum',
        active: true,
      },
    ],
  },
};

export const PaginaTopicDate = {
  breadCrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: FORUM_PATH,
        label: ' Forum',
        active: true,
      },
      {
        url: '',
        label: ' Comentarii',
        active: true,
      },
    ],
  },
};

export const UltimeleAnunturi = [
  {
    id: '1',
    name: 'Leo',
    data: 'Data publicării: 20.01.2023',
    image: ImaginePisica1,
    buttonLabel: 'Detalii',
  },
  {
    id: '2',
    name: 'Papagal',
    data: 'Data publicării: 21.01.2023',
    image: ImagineAltele1,
    buttonLabel: 'Detalii',
  },
  {
    id: '3',
    name: 'Iepuras',
    data: 'Data publicării: 24.01.2023',
    image: ImagineAltele2,
    buttonLabel: 'Detalii',
  },

];
export const FormularAdoptie = {
  title: 'Formulare',
  subTitle: 'în desfășurare',
  buttonLabel: 'TRIMITE',
};

export const CategoriiAnimaleSfaturi = [
  {
    id: '1',
    name: 'Câini',
    image: CategorieCaini,
    buttonLabel: CustomTypes.CATEGORII_ANIMALE_SFATURI,
    onClick: () => console.log(),
  },
  {
    id: '2',
    name: 'Pisici',
    image: CategoriePisici,
    buttonLabel: CustomTypes.CATEGORII_ANIMALE_SFATURI,
    onClick: () => console.log(),
  },
];

export const CategoriiAnimale = [
  {
    id: '1',
    name: 'Câini',
    image: CategorieCaini,
    buttonLabel: CustomTypes.CATEGORII_ANIMALE,
    onClick: () => console.log(),
  },
  {
    id: '2',
    name: 'Pisici',
    image: CategoriePisici,
    buttonLabel: CustomTypes.CATEGORII_ANIMALE,
    onClick: () => console.log(),
  },
  {
    id: '3',
    name: 'Altele',
    image: CategorieAltele,
    buttonLabel: CustomTypes.CATEGORII_ANIMALE,
    onClick: () => console.log(),
  },
];

export const FiltreAnimale = {
  title: 'Filtrează după',
  judet: 'Județ',
  gen: 'Gen',
  culoare: 'Culoare',
  rasa: 'Rasă',
  buttonLabelApply: 'Apply',
  buttonLabelClear: 'Clear',
};

export const AnimalePageManagement = {
  inputData: {
    styleType: CustomTypes.InputStyleTypes.SECONDARY,
    inputType: CustomTypes.InputTypes.TEXT,
    inputPlaceholder: 'Caută animalul dorit',
    icon: <AiOutlineSearch />,
  },
};


export const CategoriiPagina = {
  categoriesTitle: 'Categorii',
  categoriesTypes: [
    { label: 'Câini', index: 1 },
    { label: 'Pisici', index: 2 },
    { label: 'Altele', index: 3 },
  ],
  inputData: {
    styleType: CustomTypes.InputStyleTypes.SECONDARY,
    inputType: CustomTypes.InputTypes.TEXT,
    inputPlaceholder: 'Ce căutați?',
    icon: <AiOutlineSearch />,
  },
  dropdownData: {
    options: [
      { text: 'Titlu Anunț: A-Z', value: 'Titlu Anunț: A-Z' },
      { text: 'Titlu Anunț: Z-A', value: 'Titlu Anunț: Z-A' },
      { text: 'Județ: A-Z', value: 'Județ: A-Z' },
      { text: 'Județ: Z-A', value: 'Județ: Z-A' },
    ],
    name: 'sorting',
    backgroundColor: CustomTypes.PageColor.DARK,
    placeholder: 'Sortează',
  },
};

export const SearchForum = {
  inputData: {
    styleType: CustomTypes.InputStyleTypes.SECONDARY,
    inputType: CustomTypes.InputTypes.TEXT,
    inputPlaceholder: 'Căutați topicul aici',
    icon: <AiOutlineSearch />,
  },
};

export const OptiuniSortareAnimale = {
  ascAlphabetical: 'Titlu Anunț: A-Z',
  descAlphabetical: 'Titlu Anunț: Z-A',
  ascJudet: 'Județ: A-Z',
  descJudet: 'Județ: Z-A',
};
export const PaginaSfaturiDate = {
  breadcrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: '',
        label: ' Sfaturi',
        active: true,
      },
    ],
  },
};
export const PaginaSfaturiDatePisici = {
  breadcrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: SFATURI_PATH,
        label: ' Sfaturi',
        active: true,
      },
      {
        url: '',
        label: ' Pisici',
        active: true,
      },
    ],
  },
};
export const PaginaSfaturiDateCaini = {
  breadcrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: SFATURI_PATH,
        label: ' Sfaturi',
        active: true,
      },
      {
        url: '',
        label: ' Caini',
        active: true,
      },
    ],
  },
};
export const PaginaTrimiteSpreAdoptieDate = {
  breadcrumbs: {
    color: CustomTypes.BreadcrumbsNavColors.BROWN,
    links: [
      {
        url: ACASA_PATH,
        label: 'Acasă ',
      },
      {
        url: '',
        label: ' Trimite spre adopție',
      },
    ],
  },
  }
  export const PaginaInregistrare = {
    breadcrumbs: {
      color: CustomTypes.BreadcrumbsNavColors.BROWN,
      links: [
        {
          url: ACASA_PATH,
          label: 'Acasă',
        },
        {
          url: '',
          label: ' Înregistrare',
        },
      ],
    },
    }

    export const PaginaAdoptaDate = {
      breadcrumbs: {
        color: CustomTypes.BreadcrumbsNavColors.BROWN,
        links: [
          {
            url: ACASA_PATH,
            label: 'Acasă ',
          },
          {
            url: '',
            label: ' Adoptă',
          },
        ],
      },
      }

