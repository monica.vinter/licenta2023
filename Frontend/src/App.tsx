import PlatformRoutes from './Routes/Routes';
import { useEffect } from 'react';
import ToastFile from './components/molecules/Toast/ToastFile';
import { getUser } from './redux/features/userSlice';
import { AppThunkDispatch, RootState } from './redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { io } from 'socket.io-client';

function App() {
  const dispatch = useDispatch<AppThunkDispatch>();
  const { userData } = useSelector((state: RootState) => state.userState);

  useEffect(() => {
    if (userData == null) {
      dispatch(getUser());
    }
  }, [userData]);
  return (
    <>
      <ToastFile />
      <PlatformRoutes />
    </>
  );
}

export default App;
