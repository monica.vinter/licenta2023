import { AxiosInstance } from '../Axios/AxiosInstance';
import { newsLetter, userUrl } from '../utils/APIconsts';
import { TOKEN } from '../utils/Constants';
import { getCookie } from '../utils/utilsFunctions';
import { SecurityKeyData } from '../Interfaces/userInterfaces';

export const addNewsLetter = async (newsLetterData: SecurityKeyData) => {
    try {
        const requestHeader = {
            headers: {
                authorization: `Bearer ${getCookie(TOKEN)}`,
            },
        };
        const url = userUrl + newsLetter;
        const res = await AxiosInstance.post(url, newsLetterData, requestHeader);
        console.log(res.data);
        return res.data;
    } catch (error: any) {
        console.log(error);
    }
};