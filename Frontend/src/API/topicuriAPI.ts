import { AxiosInstance } from '../Axios/AxiosInstance';

import { topicuriUrl } from '../utils/APIconsts';

import { PostDateTopicuri } from '../Interfaces/topicuriInterfaces';
import { getCookie } from '../utils/utilsFunctions';
import { TOKEN } from '../utils/Constants';

export const fetchTopicuri = async (rejectWithValue: any) => {
  try {
    const url = topicuriUrl;
    const res = await AxiosInstance.get(url);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error.response.data.msg);
  }
};

export const adaugaTopic = async (dateTopic: PostDateTopicuri) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    console.log(dateTopic);
    const url = topicuriUrl;
    const res = await AxiosInstance.post(url, dateTopic, requestHeader);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const updateNumarComentariiTopic = async (url: string, nrComentarii: number) => {
  try {
    const statusChange = {
      numarComentarii: nrComentarii,
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};
