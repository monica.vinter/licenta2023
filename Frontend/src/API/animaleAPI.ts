import { AxiosInstance } from '../Axios/AxiosInstance';

import { animaleUrl } from '../utils/APIconsts';

import { PostDateAnimale, DateAnimale } from '../Interfaces/animaleInterfaces';
import { getCookie } from '../utils/utilsFunctions';
import { TOKEN } from '../utils/Constants';

export const fetchAnimale = async (rejectWithValue: any) => {
  try {
    const url = animaleUrl;
    const res = await AxiosInstance.get(url);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error.response.data.msg);
  }
};

export const adaugaAnimale = async (dateAnimal: PostDateAnimale) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    console.log(dateAnimal);
    const url = animaleUrl;
    const res = await AxiosInstance.post(url, dateAnimal, requestHeader);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};


export const updateAdoptatAnimal = async (url: string) => {
  try {
    const statusChange = {
      adoptat: false,
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const updateStatusAnuntAcceptat = async (url: string) => {
  try {
    const statusChange = {
      statusAnunt: 'Anunt acceptat',
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const updateStatusAnuntRefuzat = async (url: string) => {
  try {
    const statusChange = {
      statusAnunt: 'Anunt refuzat',
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const StergeAnunt = async (url: string) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    const res = await AxiosInstance.delete(url, requestHeader);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};