import { AxiosInstance } from '../Axios/AxiosInstance';

import { newsUrl } from '../utils/APIconsts';

export const fetchNews = async (rejectWithValue: any) => {
  try {
    const url = newsUrl;
    const res = await AxiosInstance.get(url);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};
