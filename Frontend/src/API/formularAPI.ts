import { AxiosInstance } from '../Axios/AxiosInstance';
import {
  FormulareData
} from '../Interfaces/formulareInterfaces';
import { formularUrl } from '../utils/APIconsts';
import { TOKEN } from '../utils/Constants';
import { getCookie } from '../utils/utilsFunctions';

export const adaugaFormular = async (formularData: FormulareData) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    const url = formularUrl;
    const res = await AxiosInstance.post(url, formularData, requestHeader);
    console.log(formularData);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const fetchFormulare = async (rejectWithValue: any) => {
  try {
    const url = formularUrl;
    const res = await AxiosInstance.get(url);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error.response.data.msg);
  }
};

export const updateStatusFormularAcceptat = async (url: string) => {
  try {
    const statusChange = {
      statusFormular: 'Acceptat',
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const updateStatusFormularRefuzat = async (url: string) => {
  try {
    const statusChange = {
      statusFormular: 'Refuzat',
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};