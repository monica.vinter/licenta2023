import { AxiosInstance } from '../Axios/AxiosInstance';

import { mesajeUrl } from '../utils/APIconsts';

import { PostDateMesaje } from '../Interfaces/mesajeInterfaces';
import { getCookie } from '../utils/utilsFunctions';
import { TOKEN } from '../utils/Constants';

export const fetchMesaje = async (rejectWithValue: any) => {
  try {
    const url = mesajeUrl;
    const res = await AxiosInstance.get(url);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error.response.data.msg);
  }
};

export const adaugaMesaj = async (dateMesaj: PostDateMesaje) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    console.log(dateMesaj);
    const url = mesajeUrl;
    const res = await AxiosInstance.post(url, dateMesaj, requestHeader);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};
