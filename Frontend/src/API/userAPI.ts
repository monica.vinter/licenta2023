import { AxiosInstance } from '../Axios/AxiosInstance';
import {
  userRegisterData,
  userLoginData,
  SecurityKeyData,
  changeResetPasswordInterface,
} from '../Interfaces';
import { userUrl } from '../utils/APIconsts';
import { TOKEN } from '../utils/Constants';
import { getCookie } from '../utils/utilsFunctions';

export const register = async (userData: userRegisterData, rejectWithValue: any) => {
  try {
    const url = userUrl;
    const res = await AxiosInstance.post(url, userData);

    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error?.response?.data?.msg);
  }
};

export const login = async (userData: userLoginData, rejectWithValue: any) => {
  try {
    const url = userUrl + '/login';
    const res = await AxiosInstance.post(url, userData);

    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error?.response?.data?.msg);
  }
};

export const generateSecurityKey = async (userData: SecurityKeyData) => {
  try {
    const url = userUrl + '/generateKeyForgot';
    const res = await AxiosInstance.post(url, userData);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return error;
  }
};

export const forgotPassword = async (userData: SecurityKeyData) => {
  try {
    const url = userUrl + '/forgotPassword';
    const res = await AxiosInstance.put(url, userData);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return error;
  }
};

export const resetPassword = async (userData: changeResetPasswordInterface) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    const url = userUrl + '/changePassword';
    const res = await AxiosInstance.put(url, userData, requestHeader);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return error;
  }
};

export const fetchUserData = async (rejectWithValue: any) => {
  try {
    const requestHeader = {
      headers: {
        authorization: `Bearer ${getCookie(TOKEN)}`,
      },
    };
    const url = userUrl + '/getUser';
    const res = await AxiosInstance.get(url, requestHeader);
    return res.data;
  } catch (error: any) {
    console.log(error);
    return rejectWithValue(error?.response?.data?.msg);
  }
};

export const UpdateImagineUser = async (url: string, imagineAvatar: string) => {
  try {
    const statusChange = {
      imagine: imagineAvatar,
    };
    const res = await AxiosInstance.patch(url, statusChange);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};

export const StergeUser = async (url: string) => {
  try {
    const res = await AxiosInstance.delete(url);
    return res.data;
  } catch (error: any) {
    console.log(error);
  }
};