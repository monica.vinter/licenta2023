import { createSlice } from '@reduxjs/toolkit';
import { ToastInitialStateInterface } from '../../Interfaces/toastInterfaces';

const initialState: ToastInitialStateInterface = {
  message: null,
  success: null,
};
export const toastSlice = createSlice({
  name: 'toast',
  initialState,
  reducers: {
    setToast: (state, action) => {
      state.message = action.payload.message;
      state.success = action.payload.success;
    },
    clearToast: () => {
      return initialState;
    },
  },
});

export const { setToast, clearToast } = toastSlice.actions;
export default toastSlice.reducer;
