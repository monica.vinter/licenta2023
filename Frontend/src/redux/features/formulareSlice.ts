import { createSlice } from '@reduxjs/toolkit';
import { FormularInitialStateInterface } from '../../Interfaces/formularInterface';

const initialState: FormularInitialStateInterface = {
    loading: false,
    formulare: [],
    error: null,
};

export const formulareSlice = createSlice({
  name: 'formulare',
  initialState,
  reducers: {
    addToFormulare: (state, action) => {
      const itemInCart = state.formulare.find((item) => (item.animal?._id === action.payload.foundAnimal?._id ));
        const newItem = {
          animal:action.payload.foundAnimal,
        }
        state.formulare.push(newItem);
    },
    removeItem: (state, action) => {
      const removeItem = state.formulare.filter((item) => item.animal?._id !== action.payload?.animalId);
      state.formulare = removeItem;
    },
    
    clearFormulare: (state) => {
      state.formulare = initialState.formulare;
    },
  },
});

export const {
  addToFormulare,
  removeItem,
  clearFormulare,
} = formulareSlice.actions;
export default formulareSlice.reducer;