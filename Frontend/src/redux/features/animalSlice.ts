import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { fetchAnimale } from '../../API/animaleAPI';
import { AnimaleInitialStateInterface } from '../../Interfaces/animaleInterfaces';
import { animaleAction } from '../../utils/APIconsts';

export const getAnimale = createAsyncThunk(animaleAction, async (rejectWithValue) => {
  try {
    const res = await fetchAnimale(rejectWithValue);
    return res;
  } catch (error) {
    console.log(error);
  }
});

const initialState: AnimaleInitialStateInterface = {
  loading: false,
  animale: [],
  error: null,
  filter: { judet: '', gen: '', color: '', rasa: [] },
};

export const animaleSlice = createSlice({
  name: 'animale',
  initialState,
  reducers: {
    setFilterJudet(state, action: PayloadAction<string[]>) {
      state.filter.judet = action.payload.join(' ');
    },
    setFilterGen(state, action: PayloadAction<string>) {
      if (!state.filter.gen.includes(action.payload))
        state.filter.gen += ' ' + action.payload;
      else state.filter.gen = state.filter.gen.replace(' ' + action.payload, '');
    },
    setFilterColor(state, action: PayloadAction<string>) {
      if (!state.filter.color.includes(action.payload)) state.filter.color += ' ' + action.payload;
      else state.filter.color = state.filter.color.replace(' ' + action.payload, '');
    },
    setFilterRasa(state, action: PayloadAction<string>) {
      if (state.filter.rasa.includes(action.payload)) {
        for (let i = 0; i < state.filter.rasa.length; i++)
          if (state.filter.rasa[i] === action.payload) state.filter.rasa.splice(i, 1);
      } else state.filter.rasa.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getAnimale.pending, (state) => {
      state.loading = true;
      state.error = '';
    });
    builder.addCase(getAnimale.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.animale = payload;
      state.error = '';
    });
    builder.addCase(getAnimale.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export const { setFilterJudet, setFilterGen, setFilterColor, setFilterRasa } =
  animaleSlice.actions;
export default animaleSlice.reducer;
