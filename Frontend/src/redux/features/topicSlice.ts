import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { fetchTopicuri } from '../../API/topicuriAPI';
import { TopicuriInitialStateInterface, DateTopicuri } from '../../Interfaces/topicuriInterfaces';
import { topicuriAction } from '../../utils/APIconsts';

export const getTopicuri = createAsyncThunk(topicuriAction, async (rejectWithValue) => {
  try {
    const res = await fetchTopicuri(rejectWithValue);
    return res;
  } catch (error) {
    console.log(error);
  }
});

export const getTopic = createAsyncThunk(topicuriAction, async (topicData: DateTopicuri, {rejectWithValue}) => {
  try {
    const res = await fetchTopicuri(rejectWithValue);
    return res;
  } catch (error) {
    console.log(error);
  }
});

const initialState: TopicuriInitialStateInterface = {
  loading: false,
  topicuri: [],
  error: null,
  topicData: null,
};

export const topicSlice = createSlice({
  name: 'topicuri',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getTopicuri.pending, (state) => {
      state.loading = true;
      state.error = '';
    });
    builder.addCase(getTopicuri.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.topicuri = payload;
      state.error = '';
    });
    builder.addCase(getTopicuri.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export default topicSlice.reducer;
