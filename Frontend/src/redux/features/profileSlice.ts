import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { fetchFormulare } from '../../API/formularAPI';
import { FormularStateInterface } from '../../Interfaces/formulareInterfaces';
import { formulareAction } from '../../utils/APIconsts';

export const getFormulare = createAsyncThunk(formulareAction, async (rejectWithValue) => {
  try {
    const res = await fetchFormulare(rejectWithValue);
    return res;
  } catch (error) {
    console.log(error);
  }
});

const initialState: FormularStateInterface = {
  loading: false,
  formulare: [],
  error: null,
};

export const formularSlice = createSlice({
  name: 'formulare',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getFormulare.pending, (state) => {
      state.loading = true;
      state.error = '';
    });
    builder.addCase(getFormulare.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.formulare = payload;
      state.error = '';
    });
    builder.addCase(getFormulare.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export default formularSlice.reducer;
