import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { register, login, fetchUserData } from '../../API/userAPI';
import { initialStateInterface, userRegisterData, userLoginData } from '../../Interfaces';
import { getUserAction, loginUserAction, registerUserAction } from '../../utils/APIconsts';
import { TOKEN } from '../../utils/Constants';
import { addCookie, existToken } from '../../utils/utilsFunctions';

export const registerUser = createAsyncThunk(
  registerUserAction,
  async (userData: userRegisterData, { rejectWithValue }) => {
    try {
      const res = await register(userData, rejectWithValue);
      return res;
    } catch (error) {
      console.log(error);
    }
  },
);

export const loginUser = createAsyncThunk(
  loginUserAction,
  async (userData: userLoginData, { rejectWithValue }) => {
    try {
      const res = await login(userData, rejectWithValue);

      return res;
    } catch (error) {
      console.log(error);
    }
  },
);

export const getUser = createAsyncThunk(getUserAction, async (rejectWithValue) => {
  try {
    const res = await fetchUserData(rejectWithValue);
    return res;
  } catch (error) {
    console.log(error);
  }
});

const initialState: initialStateInterface = {
  loading: false,
  error: null,
  isConnected: existToken(),
  userData: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(registerUser.pending, (state) => {
      state.loading = true;
      state.error = '';
    });

    builder.addCase(registerUser.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.isConnected = true;
      state.error = '';
      addCookie(TOKEN, payload.token);
    });
    builder.addCase(registerUser.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });

    builder.addCase(loginUser.pending, (state) => {
      state.loading = true;
      state.error = '';
    });
    builder.addCase(loginUser.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.isConnected = true;
      state.error = '';
      addCookie(TOKEN, payload.token);
    });
    builder.addCase(loginUser.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });

    builder.addCase(getUser.pending, (state) => {
      state.loading = true;
      state.error = '';
    });
    builder.addCase(getUser.fulfilled, (state, { payload }) => {
      state.loading = true;
      state.userData = payload;
      state.error = '';
    });
    builder.addCase(getUser.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export default userSlice.reducer;
