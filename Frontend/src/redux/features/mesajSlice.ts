import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { fetchMesaje } from '../../API/mesajeAPI';
import { MesajeInitialStateInterface } from '../../Interfaces/mesajeInterfaces';
import { mesajeAction } from '../../utils/APIconsts';

export const getMesaje = createAsyncThunk(mesajeAction, async (rejectWithValue) => {
  try {
    const res = await fetchMesaje(rejectWithValue);
    return res;
  } catch (error) {
    console.log(error);
  }
});

const initialState: MesajeInitialStateInterface = {
  loading: false,
  mesaje: [],
  error: null,
};

export const mesajSlice = createSlice({
  name: 'mesaje',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getMesaje.pending, (state) => {
      state.loading = true;
      state.error = '';
    });
    builder.addCase(getMesaje.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.mesaje = payload;
      state.error = '';
    });
    builder.addCase(getMesaje.rejected, (state, { payload }: { payload: any }) => {
      state.loading = false;
      state.error = payload;
    });
  },
});

export default mesajSlice.reducer;
