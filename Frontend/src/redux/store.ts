import { configureStore, AnyAction, ThunkDispatch } from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import userReducer from './features/userSlice';
import toastReducer from './features/toastSlice';
import animalReducer from './features/animalSlice';
import formularReducer from './features/profileSlice';
import topicReducer from './features/topicSlice';
import mesajReducer from './features/mesajSlice';
import formulareFereastraReducer from './features/formulareSlice';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, formulareFereastraReducer);

export const store = configureStore({
  reducer: {
    userState: userReducer,
    toastState: toastReducer,
    animalState: animalReducer,
    formulareState: formularReducer,
    topicuriState: topicReducer,
    mesajeState: mesajReducer,
    formulareFereastraState: persistedReducer,
  },
  middleware: [thunk],
});

export const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
export type AppThunkDispatch = ThunkDispatch<RootState, any, AnyAction>;
