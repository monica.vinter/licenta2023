import { Topic } from '../models/topicModel.js';
import {
    ERROR_TYPE_200,
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    ERROR_TYPE_500,
} from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import {
    ERROR_MESSAGE,
    BAD_REQUEST,
    SUCCESS_MESSAGE_TOPIC_ADAUGAT,
    SUCCESS_MESSAGE_UPDATED_TOPIC
} from '../utils/textConsts.js';
import {
    valideazaDescriereConditii,
} from '../utils/utilFunctions.js';

const adaugaTopic = async (req, res) => {
    try {
        const {
            titlu,
            dataPublicarii,
            numarComentarii,
        } = req.body;

        const topicCampuri = {
            titlu: titlu,
            user: req.user.id,
            dataPublicarii: Date.now(),
            numarComentarii: numarComentarii,
        };

        const newTopic = new Topic(topicCampuri);
        await newTopic.save();
        res.status(ERROR_TYPE_200).json({ msg: SUCCESS_MESSAGE_TOPIC_ADAUGAT });
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getAllTopicuri = async (req, res) => {
    try {
        const topicuri = await Topic.find().populate('user', [
            'firstname',
            'lastname',
            'email',
        ]);      
          if (topicuri) {
            res.status(ERROR_TYPE_200).send(topicuri);
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ERROR_MESSAGE });
            res.status(ERROR_TYPE_500).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const updateTopic = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            numarComentarii,
        } = req.body;
        let updateTopic = await Topic.findById(id);
        if (updateTopic) {
            updateTopic.numarComentarii = numarComentarii;
            await updateTopic.save();
            res.status(ERROR_TYPE_200).json({
                msg: SUCCESS_MESSAGE_UPDATED_TOPIC,
            });
        } else {
            res.status(ERROR_TYPE_400).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export {
    adaugaTopic,
    getAllTopicuri,
    updateTopic,
};
