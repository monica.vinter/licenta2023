import jwt from 'jsonwebtoken';
import {
    setNewsletter,
    setOptions,
    transporter,
} from '../config/nodemailer.js';
import { User } from '../models/userModel.js';
import {
    ERROR_TYPE_200,
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    TIME,
} from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import {
    ERROR_MESSAGE,
    SUCCESS_MESSAGE_UPDATED_IMAGINE_USER,
    SUCCESS_MESSAGE_DELETED_USER,
    BAD_REQUEST,
    CANT_SEND_EMAIL,
    CANT_SEND_SECURITY_KEY,
    CHANGE_PASSWORD,
    INCORRECT_PASSWORD,
    INVALID_ID,
    KEY_SENT,
    NULL_PASSWORD,
    USER_ADDED_DB,
    USER_NOT_FOUND,
} from '../utils/textConsts.js';
import bcrypt from 'bcryptjs';

import { Key } from '../models/keyModel.js';
import { customErrorCode, encryptPassword } from '../utils/utilFunctions.js';
import { Newsletter } from '../models/newsLetterModel.js';

//register user
const addUsers = async (req, res) => {
    try {
        const { imagine, id, firstname, lastname, email, phone, birthday, password, role } =
            req.body;

        const encryptedPassword = await encryptPassword(password);
        const newEmail = email.toLowerCase();

        const user = new User({
            id: id,
            firstname,
            lastname,
            email: newEmail,
            phone,
            birthday,
            password: encryptedPassword,
            role,
            imagine:imagine,
        });

        await user.save();
        generateToken(user, res);
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//login user
const loginUsers = async (req, res) => {
    try {
        const { email, password } = req.body;

        const newEmail = email.toLowerCase();

        const userFound = await User.findOne({ email: newEmail });

        await comparePassword(password, userFound.password);

        generateToken(userFound, res);
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//get user by id

const getUserById = async (req, res) => {
    try {
        const { _id } = req.user;

        let user = await User.findById(_id).select('-password');
        if (user) {
            res.status(ERROR_TYPE_200).json(user);
        } else {
            res.status(ERROR_TYPE_400).json({ msg: INVALID_ID });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//reset password
const changePassword = async (req, res) => {
    try {
        const user = await User.findById(req.user._id);
        if (!user) {
            throw new CustomStatusCodeError(USER_NOT_FOUND, ERROR_TYPE_404);
        } else {
            if (req.body.password) {
                user.password = await encryptPassword(req.body.password);
                await user.save();
                res.status(ERROR_TYPE_200).json({ msg: CHANGE_PASSWORD });
            } else {
                res.status(ERROR_TYPE_400).json({ msg: NULL_PASSWORD });
            }
        }
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//forgot password
const forgotPassword = async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email: email });
        if (!user) {
            throw new CustomStatusCodeError(USER_NOT_FOUND, ERROR_TYPE_404);
        } else {
            if (password) {
                user.password = await encryptPassword(password);
                await user.save();
                res.status(ERROR_TYPE_200).json({ msg: CHANGE_PASSWORD });
            } else {
                res.status(ERROR_TYPE_400).json({ msg: NULL_PASSWORD });
            }
        }
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//subscribe newsletter
const subscribeNewsletter = async (req, res) => {
    try {
        const { email } = req.body;

        const newEmail = email.toLowerCase();
        const emailFields = {
            email: newEmail,
        };

        const subscriber = new Newsletter(emailFields);
        transporter.sendMail(
            setNewsletter(newEmail),

            async (err, data) => {
                if (err) {
                    console.log(err);
                    res.status(ERROR_TYPE_400).json({
                        msg: CANT_SEND_EMAIL,
                    });
                } else {
                    await subscriber.save();
                    res.status(ERROR_TYPE_200).json({ msg: USER_ADDED_DB });
                }
            }
        );
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//generate security key forgot password
const generateSecuriryKeyForgotPass = async (req, res) => {
    try {
        const { email } = req.body;
        const secret = genRandomString();

        const user = await User.findOne({ email: email });

        customErrorCode(user);
        transporter.sendMail(
            setOptions(email, secret),

            async (err, data) => {
                if (err) {
                    console.log(err);
                    res.status(ERROR_TYPE_400).json({
                        msg: CANT_SEND_SECURITY_KEY,
                    });
                } else {
                    const existingKey = await Key.findOne({
                        OwnerID: user._id,
                    });
                    if (existingKey) {
                        existingKey.content = secret;
                        await existingKey.save();
                        res.status(ERROR_TYPE_200).json({ msg: KEY_SENT });
                    } else {
                        await Key.create({
                            content: secret,
                            OwnerID: user._id,
                        });
                        res.status(ERROR_TYPE_200).json({ msg: KEY_SENT });
                    }
                }
            }
        );
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

//comapre password
async function comparePassword(password, hash) {
    const result = await bcrypt.compare(password, hash);
    if (!result) {
        throw new CustomStatusCodeError(INCORRECT_PASSWORD, ERROR_TYPE_404);
    }
}
//generate token
function generateToken(user, res) {
    const payload = {
        user: {
            id: user._id,
        },
    };
    jwt.sign(
        payload,
        process.env.JWT_SECRET,
        {
            expiresIn: TIME,
        },
        (err, token) => {
            if (err) throw err;
            res.status(ERROR_TYPE_200).json({ token });
        }
    );
}
function genRandomString() {
    return Math.random().toString(36).substr(2, 5);
}

const updateSpecificUser = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            imagine,
        } = req.body;
        let updateUser = await User.findById(id);
        if (updateUser) {
            if (imagine) {
                updateUser.imagine = imagine;
                await updateUser.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_IMAGINE_USER,
                });
            } else {
                res.status(ERROR_TYPE_400).json({ msg: ERROR_MESSAGE });
            }
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ANIMAL_NOT_FOUND });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};


const deleteUserById = async (req, res) => {
    try {
        const { userId } = req.params;
        const user = await User.findByIdAndDelete(userId);
        if (user) {
            res.status(ERROR_TYPE_200).json({
                msg: SUCCESS_MESSAGE_DELETED_USER,
            });
        } else {
            res.status(ERROR_TYPE_400).json({ msg: CANNOT_FIND_ID });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export {
    addUsers,
    loginUsers,
    getUserById,
    changePassword,
    forgotPassword,
    generateSecuriryKeyForgotPass,
    subscribeNewsletter,
    updateSpecificUser,
    deleteUserById,
};
