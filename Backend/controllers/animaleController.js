import { Animal } from '../models/animalModel.js';
import {
    ERROR_TYPE_200,
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    ERROR_TYPE_500,
} from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import {
    CANNOT_FIND_ID,
    ERROR_MESSAGE,
    INVALID_ID,
    ANIMAL_NOT_FOUND,
    SUCCESS_MESSAGE_ADDED_ANIMAL,
    SUCCESS_MESSAGE_DELETED_ANIMAL,
    SUCCESS_MESSAGE_UPDATED_ANIMAL,
    BAD_REQUEST,
    PAGE_NOT_FOUND,
    SUCCESS_MESSAGE_UPDATED_ANUNT,
    SUCCESS_MESSAGE_UPDATED_TITLU_ANUNT,
    SUCCESS_MESSAGE_UPDATED_JUDET,
    SUCCESS_MESSAGE_UPDATED_RASA,
    SUCCESS_MESSAGE_UPDATED_VARSTA,
    SUCCESS_MESSAGE_UPDATED_GEN,
    SUCCESS_MESSAGE_UPDATED_CULOARE,
    SUCCESS_MESSAGE_UPDATED_ADOPTAT,
    SUCCESS_MESSAGE_UPDATED_DESCRIERE
} from '../utils/textConsts.js';
import {
    valideazaTitluAnuntAnimal,
    valideazaCategorieAnimal,
    valideazaJudetAnimal,
    valideazaRasaAnimal,
    valideazaVarstaAnimal,
    valideazaGenAnimal,
    valideazaCuloareAnimal,
    valideazaImagineAnimal,
    valideazaDescriereAnimal
} from '../utils/utilFunctions.js';


const adaugaAnimale = async (req, res) => {
    try {
        const {
            titluAnunt,
            id,
            categorie,
            judet,
            rasa,
            varsta,
            gen,
            culoare,
            imagini,
            adoptat,
            descriere, 
            emailStapan,
            statusAnunt,
            numarTelefonStapan,
            numeStapan,
            imagineProfilStapan,
        } = req.body;

        const animalCampuri = {
            titluAnunt: titluAnunt,
            id: id,
            categorie: categorie,
            judet: judet,
            rasa: rasa,
            varsta: varsta,
            gen: gen,
            culoare: culoare,
            imagini: imagini,
            adoptat: adoptat,
            descriere: descriere,
            emailStapan: emailStapan,
            stapan: req.user.id,
            statusAnunt: statusAnunt,
            numarTelefonStapan: numarTelefonStapan,
            numeStapan: numeStapan,
            imagineProfilStapan: imagineProfilStapan,
        };
        console.log(animalCampuri);
        const newAnimal = new Animal(animalCampuri);
        await newAnimal.save();
        res.status(ERROR_TYPE_200).json({ msg: SUCCESS_MESSAGE_ADDED_ANIMAL });
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getAllAnimale = async (req, res) => {
    try {
        const animale = await Animal.find();
        if (animale) {
            res.status(ERROR_TYPE_200).send(animale);
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ERROR_MESSAGE });
            res.status(ERROR_TYPE_500).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getAnimalById = async (req, res) => {
    try {
        const { animalId } = req.params;

        let animal = await Animal.findById(animalId);
        if (animal) {
            res.status(ERROR_TYPE_200).json(animal);
        } else {
            res.status(ERROR_TYPE_400).json({ msg: INVALID_ID });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const deleteAnimalById = async (req, res) => {
    try {
        const { animalId } = req.params;
        const animal = await Animal.findByIdAndDelete(animalId);
        if (animal) {
            res.status(ERROR_TYPE_200).json({
                msg: SUCCESS_MESSAGE_DELETED_ANIMAL,
            });
        } else {
            res.status(ERROR_TYPE_400).json({ msg: CANNOT_FIND_ID });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const updateAnimal = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            titluAnunt,
            categorie,
            judet,
            rasa,
            varsta,
            gen,
            culoare,
            imagine,
            adoptat,
            descriere,
            statusAnunt,
            numarTelefonStapan,
            numeStapan,
            imagineProfilStapan,
        } = req.body;
        let updateAnimal = await Animal.findById(id);
        if (updateAnimal) {
            updateAnimal.titluAnunt = titluAnunt;
            updateAnimal.categorie = categorie;
            updateAnimal.judet = judet;
            updateAnimal.rasa = rasa;
            updateAnimal.varsta = varsta;
            updateAnimal.gen = gen;
            updateAnimal.culoare = culoare;
            updateAnimal.imagine = imagine;
            updateAnimal.adoptat = adoptat;
            updateAnimal.descriere = descriere;
            updateAnimal.statusAnunt = statusAnunt;
            updateAnimal.numarTelefonStapan = numarTelefonStapan;
            updateAnimal.numeStapan =numeStapan;
            updateAnimal.imagineProfilStapan = imagineProfilStapan;
            await updateAnimal.save();
            res.status(ERROR_TYPE_200).json({
                msg: SUCCESS_MESSAGE_UPDATED_ANIMAL,
            });
        } else {
            res.status(ERROR_TYPE_400).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const updateSpecificAnimal = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            titluAnunt,
            categorie,
            judet,
            rasa,
            varsta,
            gen,
            culoare,
            imagine,
            adoptat,
            descriere, 
            statusAnunt,
            numarTelefonStapan,
            numeStapan,
            imagineProfilStapan,
        } = req.body;
        let updateAnimal = await Animal.findById(id);
        if (updateAnimal) {
            if (titluAnunt) {
                valideazaTitluAnuntAnimal(titluAnunt);
                updateAnimal.titluAnunt = titluAnunt;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_TITLU_ANUNT,
                });
            } else if (categorie) {
                valideazaCategorieAnimal(categorie);
                updateAnimal.categorie = categorie;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_CATEGORIE,
                });
            } else if (judet) {
                valideazaJudetAnimal(judet);
                updateAnimal.judet = judet;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_JUDET,
                });
            } else if (rasa) {
                valideazaRasaAnimal(rasa);
                updateAnimal.rasa = rasa;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_RASA,
                });
            } else if (varsta) {
                valideazaVarstaAnimal(varsta);
                updateAnimal.varsta = varsta;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_VARSTA,
                });
            } else if (gen) {
                valideazaGenAnimal(gen);
                updateAnimal.gen = gen;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_GEN,
                });
            } else if (culoare) {
                valideazaCuloareAnimal(culoare);
                updateAnimal.culoare = culoare;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_CULOARE,
                });
            } else if (imagine) {
                valideazaImagineAnimal(imagine);
                updateAnimal.imagine = imagine;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_ADOPTAT,
                });
            } else if (adoptat == false) {
                updateAnimal.adoptat = adoptat;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_ADOPTAT,
                });
            } else if (descriere) {
                valideazaDescriereAnimal(descriere);
                updateAnimal.descriere = descriere;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_DESCRIERE,
                })
            } else if (statusAnunt) {
                valideazaDescriereAnimal(statusAnunt);
                updateAnimal.statusAnunt = statusAnunt;
                await updateAnimal.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_ANUNT,
                })
            } else {
                res.status(ERROR_TYPE_400).json({ msg: ERROR_MESSAGE });
            }
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ANIMAL_NOT_FOUND });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export {
    adaugaAnimale,
    getAllAnimale,
    getAnimalById,
    updateAnimal,
    deleteAnimalById,
    updateSpecificAnimal,
};
