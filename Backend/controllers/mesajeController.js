import { Mesaj } from '../models/mesajModel.js';
import {
    ERROR_TYPE_200,
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    ERROR_TYPE_500,
} from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import {
    ERROR_MESSAGE,
    BAD_REQUEST,
    SUCCESS_MESSAGE_MESAJ_TRIMIS,
} from '../utils/textConsts.js';
import {
    valideazaDescriereConditii,
} from '../utils/utilFunctions.js';

const adaugaMesaj = async (req, res) => {
    try {
        const {
            mesaj,
            data, 
            numeUtilizator,
            topic,
            imagineProfilUtilizator,
            dataPublicarii
        } = req.body;

        const mesajCampuri = {
            mesaj: mesaj,
            data: data,
            numeUtilizator: numeUtilizator,
            topic: topic,
            user: req.user.id,
            imagineProfilUtilizator: imagineProfilUtilizator,
            dataPublicarii: Date.now(),
        };
        console.log(mesajCampuri);
        const newMesaj = new Mesaj(mesajCampuri);
        await newMesaj.save();
        res.status(ERROR_TYPE_200).json({ msg: SUCCESS_MESSAGE_MESAJ_TRIMIS });
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getAllMesaje = async (req, res) => {
    try {
        const mesaje = await Mesaj.find().populate('user', [
            'firstname',
            'lastname',
            'email',
        ]);      
          if (mesaje) {
            res.status(ERROR_TYPE_200).send(mesaje);
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ERROR_MESSAGE });
            res.status(ERROR_TYPE_500).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export {
    adaugaMesaj,
    getAllMesaje,
};
