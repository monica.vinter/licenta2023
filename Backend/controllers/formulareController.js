import { Formular } from '../models/formularModel.js';
import {
    ERROR_TYPE_200,
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    ERROR_TYPE_500,
} from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import {
    CANNOT_FIND_ID,
    ERROR_MESSAGE,
    INVALID_ID,
    ANIMAL_NOT_FOUND,
    SUCCESS_MESSAGE_ADDED_FORMULAR,
    SUCCESS_MESSAGE_DELETED_FORMULAR,
    SUCCESS_MESSAGE_UPDATED_FORMULAR,
    SUCCESS_MESSAGE_UPDATED_STATUS_FORMULAR,
    BAD_REQUEST,
    PAGE_NOT_FOUND,
    SUCCESS_MESSAGE_UPDATED_DESCRIERE_CONDITII
} from '../utils/textConsts.js';
import {
    valideazaDescriereConditii,
} from '../utils/utilFunctions.js';

const adaugaFormular = async (req, res) => {
    try {
        const {
            descriereConditii,
            statusFormular,
            numeStapanAnimal,
            nrTelStapanAnimal,
            emailViitorStapan,
            numeViitorStapan,
            nrTelViitorStapan,
            emailStapanAnimal,
            idAnimal,
            imagineAnimal,
            titluAnuntAnimal,
        } = req.body;

        const formularCampuri = {
            user: req.user.id,
            descriereConditii: descriereConditii,
            statusFormular: statusFormular,
            numeStapanAnimal: numeStapanAnimal,
            emailStapanAnimal: emailStapanAnimal,
            nrTelStapanAnimal: nrTelStapanAnimal,
            numeViitorStapan: numeViitorStapan,
            emailViitorStapan: emailViitorStapan,
            nrTelViitorStapan: nrTelViitorStapan,
            idAnimal: idAnimal,
            imagineAnimal: imagineAnimal,
            titluAnuntAnimal: titluAnuntAnimal,
        };

        const newFormular = new Formular(formularCampuri);
        await newFormular.save();
        res.status(ERROR_TYPE_200).json({ msg: SUCCESS_MESSAGE_UPDATED_STATUS_FORMULAR });
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getAllFormulare = async (req, res) => {
    try {
        const formulare = await Formular.find().populate('user', [
            'firstname',
            'lastname',
            'email',
        ]);      
          if (formulare) {
            res.status(ERROR_TYPE_200).send(formulare);
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ERROR_MESSAGE });
            res.status(ERROR_TYPE_500).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getFormularById = async (req, res) => {
    try {
        const { formularId } = req.params;

        let formular = await Animal.findById(formularId);
        if (formular) {
            res.status(ERROR_TYPE_200).json(formular);
        } else {
            res.status(ERROR_TYPE_400).json({ msg: INVALID_ID });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const getFormulareByUser = async (req, res) => {
    try {
        const formulare = await Formular.find({
            user: { _id: req.user.id },
        });
        if (formulare) {
            res.status(ERROR_TYPE_200).send(formulare);
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const deleteFormular = async (req, res) => {
    try {
        const { formularId } = req.params;
        const formular = await Formular.findByIdAndDelete(formularId);
        if (formular) {
            res.status(ERROR_TYPE_200).json({
                msg: SUCCESS_MESSAGE_DELETED_FORMULAR,
            });
        } else {
            res.status(ERROR_TYPE_400).json({ msg: CANNOT_FIND_ID });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const updateFormular = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            descriereConditii,
            statusFormular,
        } = req.body;
        let updateFormular = await Formular.findById(id);
        if (updateFormular) {
            updateFormular.descriereConditii = descriereConditii;
            updateFormular.statusFormular = statusFormular;
            await updateFormular.save();
            res.status(ERROR_TYPE_200).json({
                msg: SUCCESS_MESSAGE_UPDATED_FORMULAR,
            });
        } else {
            res.status(ERROR_TYPE_400).json({ msg: ERROR_MESSAGE });
        }
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const updateSpecificFormular = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            descriereConditii,
            statusFormular,
        } = req.body;
        let updateFormular = await Formular.findById(id);
        if (updateFormular) {
            if (descriereConditii) {
                valideazaDescriereConditii(descriereConditii);
                updateFormular.descriereConditii = descriereConditii;
                await updateFormular.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_DESCRIERE_CONDITII,
                });
            } else if (statusFormular) {
                updateFormular.statusFormular = statusFormular;
                await updateFormular.save();
                res.status(ERROR_TYPE_200).json({
                    msg: SUCCESS_MESSAGE_UPDATED_STATUS_FORMULAR,
                });
            } else {
                res.status(ERROR_TYPE_400).json({ msg: ERROR_MESSAGE });
            }
        } else {
            res.status(ERROR_TYPE_404).json({ msg: ANIMAL_NOT_FOUND });
        }
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export {
    adaugaFormular,
    getAllFormulare,
    getFormulareByUser,
    getFormularById,
    deleteFormular,
    updateFormular,
    updateSpecificFormular,
};
