import express from 'express';
import {
    adaugaMesaj,
    getAllMesaje,
} from '../controllers/mesajeController.js';
import { auth} from '../middleware/auth.js';

const router = express.Router();

//@route POST api/mesaje
//Public route
//Desc: Add a new mesaj
router.post('/', auth, adaugaMesaj);

//@GET api/mesaje
//Public route
//Desc: Get all mesaje
router.get('/', getAllMesaje);

export default router;
