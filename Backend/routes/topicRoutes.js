import express from 'express';
import {
    adaugaTopic,
    getAllTopicuri,
    updateTopic
} from '../controllers/topicuriController.js';
import { auth} from '../middleware/auth.js';

const router = express.Router();

//@route POST api/topicuri
//Public route
//Desc: Add a new topic
router.post('/', auth, adaugaTopic);

//@GET api/topicuri
//Public route
//Desc: Get all topicuri
router.get('/', getAllTopicuri);


//@PATCH api/topicuri/id
//Public route
//Desc: Update specific field
router.patch('/:id', updateTopic);

export default router;
