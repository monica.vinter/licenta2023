import express from 'express';

 import { auth } from '../middleware/auth.js';
 import {
     addUsers,
     loginUsers,
     changePassword,
     forgotPassword,
     generateSecuriryKeyForgotPass,
     subscribeNewsletter,
     getUserById,
     updateSpecificUser,
     deleteUserById,
 } from '../controllers/usersController.js';
 import {
     validateChangePassword,
     validateNewsletterSub,
     validateUserLogin,
     validateUserRegister,
     verifySecurityKey,
 } from '../middleware/userMiddleware.js';

 
 const router = express.Router();

 //@route POST api/users
 //Public route
 //Desc: Register user and get token
 router.post('/', validateUserRegister, addUsers);

 //@route POST api/users/login
 //Public route
 //Desc: Login user and get token
 router.post('/login', validateUserLogin, loginUsers);

 //@GET api/users/getUser
 //Public route
 //Desc: Get the user by id
 router.get('/getUser', auth, getUserById);

 
 //@route PUT api/users/changePassword
 //Private route
 //Desc: Reset password
 router.put(
     '/changePassword',
     auth,
     validateChangePassword,

     changePassword
 );
 //@route POST api/users//generateKeyForgot
 //Public route
 //Desc: Generate key for change password
 router.post('/generateKeyForgot', generateSecuriryKeyForgotPass);

 //@route PUT api/users//forgotPassword
 //Public route
 //Desc: Change password
 router.put('/forgotPassword', verifySecurityKey, forgotPassword);

 //@route POST api/users/newsLetter
 //Public route
 //Desc: subscribe newsletter
 router.post('/newsLetter', validateNewsletterSub, subscribeNewsletter);
 
 //@PATCH api/users/id
//Public route
//Desc: Update specific field
router.patch('/:id', updateSpecificUser);

 //@PATCH api/users/id
//Public route
//Desc: Delete specific user
router.delete('/:userId', deleteUserById);

export default router;
