import express from 'express';
import {
    adaugaAnimale,
    deleteAnimalById,
    getAllAnimale,
    getAnimalById,
    updateAnimal,
    updateSpecificAnimal,
} from '../controllers/animaleController.js';
import { valideazaAnimal } from '../middleware/animaleMiddleware.js';
import { auth, authorizeRoles} from '../middleware/auth.js';

const router = express.Router();
import multer from 'multer';

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, "../../Frontend/src/uploads/");
    },
    filename: (req, file, callback) => {
     callback(null, file.imagini);
    }
});

const upload =  multer({storage: storage});

//@route POST api/animale
//Public route
//Desc: Add a new animal
router.post('/', auth, valideazaAnimal, adaugaAnimale);

//@GET api/animale
//Public route
//Desc: Get all animale
router.get('/', getAllAnimale);

//@GET api/animale/id
//Public route
//Desc: Get the animal by id
router.get('/:animalId', getAnimalById);

//@PUT api/animale/id
//Public route
//Desc: Update animal
router.put('/:id', auth, authorizeRoles('admin'), valideazaAnimal, updateAnimal);

//@PATCH api/animale/id
//Public route
//Desc: Update specific field
router.patch('/:id', updateSpecificAnimal);

//@DELETE api/animale/id
//Public route
//Desc: Delete the animal by id
router.delete('/:animalId', auth, deleteAnimalById);



export default router;
