import express from 'express';
import {
    adaugaFormular,
    getAllFormulare,
    getFormulareByUser,
    getFormularById,
    deleteFormular,
    updateFormular,
    updateSpecificFormular,
} from '../controllers/formulareController.js';
import { valideazaFormular } from '../middleware/formulareMiddleware.js';
import { auth, authorizeRoles} from '../middleware/auth.js';

const router = express.Router();

//@route POST api/formulare
//Public route
//Desc: Add a new formular
router.post('/', auth, valideazaFormular, adaugaFormular);

//@GET api/formulare
//Public route
//Desc: Get all formulare
router.get('/', getAllFormulare);

//@GET api/formulare/id
//Public route
//Desc: Get the formular by id
router.get('/user', getFormulareByUser);

//@GET api/formulare/id
//Public route
//Desc: Get the formular by id
router.get('/:formularId', getFormularById);

//@DELETE api/formulare/id
//Public route
//Desc: Delete the formulare by id
router.delete('/:formularId', deleteFormular);

//@PUT api/formulare/id
//Public route
//Desc: Update formulare
router.put('/:id', valideazaFormular, updateFormular);

//@PATCH api/formulare/id
//Public route
//Desc: Update specific field
router.patch('/:id', updateSpecificFormular);

export default router;
