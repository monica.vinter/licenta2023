import mongoose from 'mongoose';

const mesajSchema = new mongoose.Schema({
    mesaj:{
        type: String,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    dataPublicarii: {
        type: Date,
        default: Date.now(),
    },
     numeUtilizator:{
        type: String,
    },
    topic: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Topic',
    },
    imagineProfilUtilizator: {
        type: String,
        default: 'nepus',
    },
});

const Mesaj = mongoose.model('Mesaj', mesajSchema);
export { Mesaj };
