import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    phone: {
        type: String,
    },
    birthday: {
        type: Date,
    },
    password: {
        type: String,
        required: true,
    },
    resetLink: {
        data: String,
        default: '',
    },
    role: {
        type: String,
        default: 'user',
    },
    imagine: {
        type: String,
        default: 'faraImagine',
    },
});

const User = mongoose.model('User', userSchema);
export { User };
