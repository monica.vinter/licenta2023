import mongoose from 'mongoose';

const formularSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    numeViitorStapan:{
        type: String,
    },
    emailViitorStapan:{
        type: String,
    },
    nrTelViitorStapan:{
        type: String,
    },
    emailStapanAnimal:{
        type: String,
    },
    numeStapanAnimal:{
        type: String,
    },
    nrTelStapanAnimal:{
        type: String,
    },
    descriereConditii: {
        type: String,
    },
    statusFormular: {
        type: String,
        default: 'In asteptare',
    },
    idAnimal: {
        type: String,
    },
    imagineAnimal: {
        type: String,
    },
    titluAnuntAnimal: {
        type: String,
    },
});

const Formular = mongoose.model('Formular', formularSchema);
export { Formular };
