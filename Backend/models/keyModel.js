import mongoose from 'mongoose';
import { User } from './userModel.js';

const keySchema = new mongoose.Schema({
    content: { type: String, required: true },
    OwnerID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: User,
    },
});
const Key = mongoose.model('Key', keySchema);
export { Key };
