import mongoose from 'mongoose';

const newsLetterSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
    },
});

const Newsletter = mongoose.model('Newsletter ', newsLetterSchema);
export { Newsletter };
