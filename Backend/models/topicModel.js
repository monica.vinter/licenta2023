import mongoose from 'mongoose';

const topicSchema = new mongoose.Schema({
    titlu:{
        type: String,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    dataPublicarii: {
        type: Date,
        default: Date.now(),
    },
    numarComentarii: {
        type: Number,
        default: 0,
    }
});

const Topic = mongoose.model('Topic', topicSchema);
export { Topic };
