import mongoose from 'mongoose';

const animalSchema = new mongoose.Schema({
    titluAnunt: {
        type: String,
    },
    categorie: {
        type: String,
    },
    judet: {
        type: String,
    },
    rasa: {
        type: String,
    },
    varsta: {
        type: String,
    },
    gen: {
        type: String,
    },
    culoare: {
        type: String,
    },
    imagini: [
        {
            type: String,
        },
    ],
    adoptat: {
        type: Boolean,
        default: true,
    },
    descriere: {
        type: String,
    },
    emailStapan: {
        type: String,
    },
    stapan: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    statusAnunt: {
        type: String,
        default: 'In asteptare',
    },
    numarTelefonStapan: {
        type: String,
    },
    numeStapan: {
        type: String,
    },
    imagineProfilStapan: {
        type: String,
        default: 'nepus',
    },
});

const Animal = mongoose.model('Animal', animalSchema);
export { Animal };
