export const RASA_GOLDEN_RETRIEVER = 'Golden Retriever';
export const RASA_AKITA_INU = 'Akita Inu';
export const RASA_CIOBANESC_GERMAN = 'Ciobănesc german';
export const CATEGORIE_CAINI = 'Câini';
export const CATEGORIE_PISICI = 'Pisici';
export const CATEGORIE_ALTELE = 'Altele';
export const COLOR_BLACK = 'black';
export const COLOR_GRAY = 'gray';
export const COLOR_WHITE = 'white';
export const COLOR_RED = 'red';
export const COLOR_ORANGE = 'orange';
export const COLOR_YELLOW = 'yellow';
export const COLOR_GREEN = 'green';
export const COLOR_DARK_GREEN = 'darkGreen';
export const COLOR_BLUE = 'blue';
export const COLOR_DARK_BLUE = 'darkBlue';
export const COLOR_PURPLE = 'purple';
export const COLOR_PINK = 'pink';

export const ERROR_TYPE_400 = 400;
export const ERROR_TYPE_404 = 404;
export const ERROR_TYPE_200 = 200;
export const ERROR_TYPE_500 = 500;

export const REGEX_PASSWORD = new RegExp(
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*£-]).{8,}$/
);

export const REGEX_EMAIL = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/);
export const REGEX_NAME = new RegExp(/^([a-z A-Z]{3,})$/);
export const REGEX_PHONE = new RegExp(
    /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
);

//valid formats
// (123) 456-7890
// (123)456-7890
// 123-456-7890
// 123.456.7890
// 1234567890
// +31636363634
// 075-63546725 
export const REGEX_BIRTHDAY = new RegExp(
    /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/
);
export const TIME = 36000;