import { User } from '../models/userModel.js';
import bcrypt from 'bcryptjs';
import {
    CATEGORIE_CAINI,
    CATEGORIE_PISICI,
    CATEGORIE_ALTELE,
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    REGEX_EMAIL,
    REGEX_NAME,
    REGEX_PASSWORD,
    REGEX_PHONE,
    COLOR_BLACK,
    COLOR_GRAY,
    COLOR_WHITE,
    COLOR_ORANGE,
    COLOR_YELLOW,
} from './constsFile.js';
import CustomStatusCodeError from './customError.js';
import {
    COLOR_IS_REQUIRED,
    IMAGE_IS_REQUIRED,
    INCORRECT_PHONE_NUMBER,
    NAME_IS_SHORT,
    PHONE_EXIST,
    USER_ALREADY_EXIST,
    AUTHOR_IS_SHORT,
    WRONG_EMAIL_FORMAT,
    YEAR_GREATER,
    TOTAL_AMOUNT_IS_SHORT,
    CATEGORIE_NECESARA,
    JUDET_NECESAR,
} from './textConsts.js';


export const valideazaDescriereConditii = (descriereConditii) => {
    if (descriereConditii.length < 3)
    throw new CustomStatusCodeError(NAME_IS_SHORT, ERROR_TYPE_404);
};

export const valideazaTitluAnuntAnimal = (titluAnunt) => {
    if (titluAnunt.length < 3)
    throw new CustomStatusCodeError(NAME_IS_SHORT, ERROR_TYPE_404);
};

export const valideazaCategorieAnimal = (categorie) => {
    if (
        categorie !== CATEGORIE_CAINI &&
        categorie !== CATEGORIE_PISICI &&
        categorie !== CATEGORIE_ALTELE 
    )
        throw new CustomStatusCodeError(CATEGORIE_NECESARA, ERROR_TYPE_404);
};

export const valideazaJudetAnimal= (judet) => {
    if (judet.length < 3)
        throw new CustomStatusCodeError(JUDET_NECESAR, ERROR_TYPE_404);
};

export const valideazaRasaAnimal = (rasa) => {
    if (rasa.length < 3)
    throw new CustomStatusCodeError(NAME_IS_SHORT, ERROR_TYPE_404);
};

export const valideazaVarstaAnimal = (varsta) => {
    if (varsta.length < 3)
    throw new CustomStatusCodeError(NAME_IS_SHORT, ERROR_TYPE_404);
};

export const valideazaGenAnimal = (gen) => {
    if (gen.length < 3)
    throw new CustomStatusCodeError(NAME_IS_SHORT, ERROR_TYPE_404);
};

export const valideazaCuloareAnimal = (culoare) => {
    if (
        culoare !== COLOR_BLACK &&
        culoare !== COLOR_GRAY &&
        culoare !== COLOR_BROWN &&
        culoare !== COLOR_ORANGE &&
        culoare !== COLOR_YELLOW &&
        culoare !== COLOR_WHITE 
    )
        throw new CustomStatusCodeError(COLOR_IS_REQUIRED, ERROR_TYPE_404);
};

export const valideazaImagineAnimal = (imagini) => {
        if (imagini.length <= 0)
            throw new CustomStatusCodeError(IMAGE_IS_REQUIRED, ERROR_TYPE_404);
};

export const valideazaDescriereAnimal = (descriere) => {
    if (descriere.length < 3)
    throw new CustomStatusCodeError(NAME_IS_SHORT, ERROR_TYPE_404);
};

export async function validateRegistration(email) {
    const user = await User.findOne({ email });

    if (user) {
        throw new CustomStatusCodeError(USER_ALREADY_EXIST, ERROR_TYPE_400);
    }
}

export async function verifyIfUserExist(email) {
    const user = await User.findOne({ email });
    customErrorCode(user);
}

export async function encryptPassword(password) {
    const salt = await bcrypt.genSaltSync(10);
    return await bcrypt.hashSync(password, salt);
}

export const validateName = (name) => {
    if (!REGEX_NAME.test(name))
        throw new CustomStatusCodeError(AUTHOR_IS_SHORT,ERROR_TYPE_404);
};

export const validateUserEmail = (email) => {
    const newEmail = String(email).toLowerCase();
    if (!REGEX_EMAIL.test(newEmail)) {
        throw new CustomStatusCodeError(WRONG_EMAIL_FORMAT, ERROR_TYPE_404);
    }
};

export const validateUserPassword = (password, msg) => {
    if (!REGEX_PASSWORD.test(password))
        throw new CustomStatusCodeError(msg, ERROR_TYPE_404);
};
export async function validateUserPhone(phone) {
    const user = await User.findOne({ phone: phone });

    if (user) {
        throw new CustomStatusCodeError(PHONE_EXIST, ERROR_TYPE_404);
    }
    if (!REGEX_PHONE.test(phone)) {
        throw new CustomStatusCodeError(INCORRECT_PHONE_NUMBER, ERROR_TYPE_404);
    }
}

export async function validateBirthday(birthday) {
    const userDate = new Date(birthday);
    const currentDate = new Date();

    if (userDate.getFullYear() > currentDate.getFullYear()) {
        throw new CustomStatusCodeError(YEAR_GREATER, ERROR_TYPE_404);
    }
}
export const customErrorCode = (user) => {
    if (!user) {
        throw new CustomStatusCodeError(USER_NOT_FOUND, ERROR_TYPE_404);
    }
};

export const validateOrderTotalAmount = (totalAmount) => {
    if (totalAmount <= 0)
        throw new CustomStatusCodeError(TOTAL_AMOUNT_IS_SHORT, ERROR_TYPE_404);
};
