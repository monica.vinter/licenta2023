import nodemailer from 'nodemailer';

import dotenv from 'dotenv';
import {
    GMAIL,
    REQUEST_SECURITY_KEY,
    RESET_PASSWORD,
    SUBJECT_NEWSLETTER,
    TEXT_NEWSLETTER,
} from '../utils/textConsts.js';

dotenv.config();

const transporter = nodemailer.createTransport({
    service: GMAIL,

    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASS,
    },
});

const setOptions = (email, key) => {
    return {
        from: process.env.EMAIL,
        to: email,
        subject: RESET_PASSWORD,
        text: REQUEST_SECURITY_KEY + key,
    };
};
const setNewsletter = (email) => {
    return {
        from: process.env.EMAIL,
        to: email,
        subject: SUBJECT_NEWSLETTER,
        text: TEXT_NEWSLETTER,
    };
};

export { transporter, setOptions, setNewsletter };
