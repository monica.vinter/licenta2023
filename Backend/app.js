import express from 'express';
import dotenv from 'dotenv';
import { connectDB } from './config/db.js';
import userRoutes from './routes/userRoutes.js';
import animalRoutes from './routes/animalRoutes.js';
import formularRoutes from './routes/formularRoutes.js';
import topicRoutes from './routes/topicRoutes.js';
import mesajRoutes from './routes/mesajRoutes.js';
import cors from 'cors';

dotenv.config();
const app = express();
connectDB();

app.use(express.json({ extended: false }));

app.use(cors({ origin: process.env.CORS_URL, credentials: true }));

app.use('/api/users', userRoutes);
app.use('/api/formulare', formularRoutes);
app.use('/api/animale', animalRoutes);
app.use('/api/topicuri', topicRoutes);
app.use('/api/mesaje', mesajRoutes);

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});
