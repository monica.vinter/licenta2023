import { ERROR_TYPE_400 } from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import { BAD_REQUEST } from '../utils/textConsts.js';
import {
    valideazaDescriereConditii,
} from '../utils/utilFunctions.js';

const valideazaFormular = async (req, res, next) => {
    try {
        const {
            descriereConditii,
        } = req.body;

        valideazaDescriereConditii(descriereConditii);
        next();
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export { valideazaFormular };
