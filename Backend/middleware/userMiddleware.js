import { Key } from '../models/keyModel.js';
import { Newsletter } from '../models/newsLetterModel.js';
import {
    ERROR_TYPE_400,
    ERROR_TYPE_404,
    ERROR_TYPE_500,
} from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import {
    BAD_REQUEST,
    INCORRECT_PASSWORD,
    PASSWORD_DOESNT_CONTAIN,
    SERVER_ERROR,
    USER_ALREADY_SUBSCRIBED,
} from '../utils/textConsts.js';
import {
    validateBirthday,
    validateRegistration,
    validateUserEmail,
    validateName,
    validateUserPassword,
    validateUserPhone,
} from '../utils/utilFunctions.js';

const validateUserRegister = async (req, res, next) => {
    try {
        const { firstname, lastname, email, phone, birthday, password } =
            req.body;
        await validateRegistration(email);
        validateName(firstname);
        validateName(lastname);
        await validateUserPhone(phone);
        validateUserEmail(email);
        await validateBirthday(birthday);

        validateUserPassword(password, PASSWORD_DOESNT_CONTAIN);

        next();
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const validateUserLogin = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        validateUserEmail(email);
        validateUserPassword(password, INCORRECT_PASSWORD);
        next();
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const validateNewsletterSub = async (req, res, next) => {
    try {
        const { email } = req.body;
        validateUserEmail(email);
        const foundEmail = await Newsletter.findOne({ email: email });
        if (foundEmail) {
            throw new CustomStatusCodeError(
                USER_ALREADY_SUBSCRIBED,
                ERROR_TYPE_404
            );
        }
        next();
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const verifySecurityKey = async (req, res, next) => {
    try {
        const key = await Key.findOne({ content: req.body.key });

        next();
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

const validateChangePassword = async (req, res, next) => {
    try {
        const { password } = req.body;

        validateUserPassword(password, PASSWORD_DOESNT_CONTAIN);
        next();
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export {
    validateUserRegister,
    validateUserLogin,
    verifySecurityKey,
    validateChangePassword,
    validateNewsletterSub,
};
