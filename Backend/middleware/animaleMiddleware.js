import { ERROR_TYPE_400 } from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import { BAD_REQUEST } from '../utils/textConsts.js';
import {
    valideazaTitluAnuntAnimal,
    valideazaCategorieAnimal,
    valideazaJudetAnimal,
    valideazaRasaAnimal,
    valideazaVarstaAnimal,
    valideazaGenAnimal,
    valideazaCuloareAnimal,
    valideazaImagineAnimal,
    valideazaDescriereAnimal,
} from '../utils/utilFunctions.js';

const valideazaAnimal = async (req, res, next) => {
    try {
        const {
            titluAnunt,
            categorie,
            judet,
            rasa,
            varsta,
            gen,
            culoare,
            imagini,
            descriere,
            imagine
        } = req.body;

        valideazaTitluAnuntAnimal(titluAnunt);
        valideazaCategorieAnimal(categorie);
        valideazaJudetAnimal(judet);
        valideazaRasaAnimal(rasa);
        valideazaVarstaAnimal(varsta);
        valideazaGenAnimal(gen);
        valideazaDescriereAnimal(descriere);
        next();
    } catch (error) {
        console.error(error);
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export { valideazaAnimal };
