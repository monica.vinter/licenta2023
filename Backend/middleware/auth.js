import jwt from 'jsonwebtoken';
import { INVALID_TOKEN, TOKEN_REQUIRED } from '../utils/textConsts.js';
import { User } from '../models/userModel.js';
const config = process.env;

const auth = async (req, res, next) => {
    let token;

    if (
        req.headers.authorization.startsWith('Bearer') &&
        req.headers.authorization
    ) {
        try {
            token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.verify(token, process.env.JWT_SECRET);
            req.user = await User.findById(decoded.user.id);
            res.locals.user = decoded.user;
            next();
        } catch (error) {
            console.error(error.message);
            return res.status(400).send({ errors: [{ msg: INVALID_TOKEN }] });
        }
    }
    if (!token) {
        return res.status(403).json({ errors: [{ msg: TOKEN_REQUIRED }] });
    }
};

const authorizeRoles = (...roles) => {
    return (req, res, next) => {
        console.log(req.user);
        if (!roles.includes(req.user.role)) {
            return res.status(403).send({ errors: [{ msg: `Role (${req.user.role}) is not allowed to acccess this resource` }] });
        }
        next();
    }
}

export { auth, authorizeRoles };

