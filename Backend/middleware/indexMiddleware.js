import { ERROR_TYPE_400 } from '../utils/constsFile.js';
import CustomStatusCodeError from '../utils/customError.js';
import { BAD_REQUEST } from '../utils/textConsts.js';
import {
    validateCourseName,
    validateTee,
    validateHandicap,
} from '../utils/utilFunctions.js';

const validateIndex = async (req, res, next) => {
    try {
        const { course, tee, handicap } = req.body;
        validateCourseName(course);
        validateTee(tee);
        validateHandicap(handicap);
        next();
    } catch (error) {
        if (error instanceof CustomStatusCodeError) {
            return res.status(error.statusCode).json({ msg: error.message });
        }
        console.error(error);
        return res.status(ERROR_TYPE_400).json({ msg: BAD_REQUEST });
    }
};

export { validateIndex };
